using Fuse.Scripting;
using Fuse;
using Android;
using Android.ActivityUtils;
using Uno.Compiler.ExportTargetInterop;
using Uno.UX;

[Require("Gradle.Repository","maven { url 'https://bitbucket.org/smartpesa/maven/raw/master' }")]
[Require("Gradle.Repository","maven { url 'https://bitbucket.org/smartpesa/smartpesa-maven/raw/master' }")]
[Require("Gradle.Dependency","compile('com.smartpesa:spconnect:2.2.1')")]
//[Require("AndroidManifest.ApplicationElement", "tools:replace="android:allowBackup"")]

[extern(Android) ForeignInclude(Language.Java,
  "com.fuse.Activity",
  "android.content.Intent",
  "android.net.Uri",
  "android.os.Bundle",
  "android.provider.MediaStore",
	"android.database.Cursor",
	"android.content.Context",
	"android.util.Log",
	"com.smartpesa.intent.SpConnect",
	"com.smartpesa.intent.TransactionArgument",
	"com.smartpesa.intent.TransactionType",
	"com.smartpesa.intent.result.TransactionError",
	"com.smartpesa.intent.result.TransactionResult",
	"java.math.BigDecimal",
	"android.util.Log",
	"java.io.File")]

[UXGlobalModule]
public class Checkout : NativeEventEmitterModule
{
	static readonly Checkout _instance;
  int mposResultCode;
  string mposResult;
  string cardHoldersName;

  

  public Checkout()
  : base(true, "messageReceived")
  {
  	// Make sure we're only initializing the module once
		if (_instance != null) return;

		_instance = this;
		Resource.SetGlobalKey(_instance, "Checkout");
    AddMember( new NativeFunction("mpos", (NativeCallback) mpos));
    AddMember( new NativeFunction("getMposResultCode", (NativeCallback) getMposResultCode));
    AddMember( new NativeFunction("getMposResult", (NativeCallback) getMposResult));
    AddMember( new NativeFunction("getCardHoldersName", (NativeCallback) getCardHoldersName));
    
    
  }

  object mpos(Context c, object[] args)
	{

		var arg = args.Length > 0 ? args[0] as string : null;
		debug_log "messageReceived" + arg;
		//amount = arg;
		//int val = Marshal.ToInt(args);

		//if (arg == null)
		//	debug_log "messageReceived" +  "----";
		//else
		//	debug_log "messageReceived" + val;

	  mposResultCode = 0;
	  mposResult = "nothing";
    var sintent = makeMyIntent((string)args[0]);
		if (sintent!=null)
  	{
			ActivityUtils.StartActivity(sintent, OnResult);
			
		} else {
			debug_log "Didnt make intent. boooo";
		}
		debug_log "ok now";
		return null;
	}

	object getMposResult(Context c, object[] args)
	{
		return mposResult;
	}

	void setMposResult(string re)
	{
		debug_log "This is the result: " + re;
		//debug_log pathFile;
		mposResult = re;
	}

	object getMposResultCode(Context c, object[] args)
	{
		return mposResultCode;
	}

	void setMposResultCode(int r)
	{
		debug_log "This is the result: " + r;
		//debug_log pathFile;
		mposResultCode = r;
	}

  object getCardHoldersName(Context c, object[] args)
  {
    return cardHoldersName;
  }

  void setCardHoldersName(string name)
  {
    debug_log "Card Holders Name: " + name;
    //debug_log pathFile;
    cardHoldersName = name;
  }

  void send(int resultCode)
  {
    debug_log "sending resultcode bacl to javascipt" + resultCode;
    Emit("messageReceived", resultCode);
  }


	[Foreign(Language.Java)]
	extern(android) void OnResult(int resultCode, Java.Object intent, object info) 
	@{
		Log.d("Success!: ", resultCode + " - " + intent + "\n");
		
		Intent data = Intent.class.cast(intent);
		
		 if (data == null) {
               
                @{Checkout:Of(_this).setMposResult(string):Call("nothing")};

            	@{Checkout:Of(_this).setMposResultCode(int):Call(resultCode)};
                return;
            }

		else if (resultCode == -1) {
              
                TransactionResult result = SpConnect.parseSuccessTransaction(data);
                if (result != null) {
            		String resultString = "Success\n" +
                    result.reference() + "\n" +
                    result.type() + "\n" +
                    result.responseDescription() + "\n" +
                    result.responseCode() + "\n" +
                    result.datetime() + "\n" +
                    result.currency().symbol() + " " + result.amount() + "\n" +
                    result.card().pan() + ": " + result.card().holderName() + "\n" +
                    result.description() + "\n";
            		
            		@{Checkout:Of(_this).setMposResult(string):Call(resultString)};

            		@{Checkout:Of(_this).setMposResultCode(int):Call(resultCode)};
                @{Checkout:Of(_this).setCardHoldersName(string):Call(result.card().holderName())};  
                @{Checkout:Of(_this).send(int):Call(resultCode)};               
                
            	}	
            } 
        else {
                
                TransactionError error = SpConnect.parseErrorTransaction(data);
                String errorString = "Error\n" +
                error.transactionException().getMessage() + "\n" +
                error.transactionException().getReason() + "\n";
                TransactionResult r = error.transactionResult();
        		if (error.transactionResult() != null) {
            		
            		errorString += r.reference() + "\n" +
                    r.reference() + "\n" +
                    r.type() + "\n" +
                    r.responseDescription() + "\n" +
                    r.responseCode() + "\n" +
                    r.datetime() + "\n" +
                    r.currency().symbol() + " " + r.amount() + "\n" +
                    r.card().pan() + ": " + r.card().holderName() + "\n" +
                    r.description() + "\n";
        		}
        		
        		@{Checkout:Of(_this).setMposResult(string):Call(errorString)};

            	@{Checkout:Of(_this).setMposResultCode(int):Call(resultCode)};
              @{Checkout:Of(_this).setCardHoldersName(string):Call("Error Occured " + r.card().holderName())};
              @{Checkout:Of(_this).send(int):Call(resultCode)}; 
            }

        
	@}

	[Foreign(Language.Java)]
  static extern(android) Java.Object makeMyIntent(string amount)
  @{

  			int amountParse = Integer.parseInt(amount);		
  		
            TransactionArgument argument = TransactionArgument.builder()
                    .transactionType(TransactionType.SALES)
                    .amount(new BigDecimal(amountParse))
                    .tip(new BigDecimal(0))
                    .tax1Amount(new BigDecimal(0))
                    .tax1Type("")
                    .tax2Amount(new BigDecimal(0))
                    .tax2Type("")
                    .printReceipt(true)
                    .externalReference("OK")
                    .build();

            
            Intent intent2 = SpConnect.createTransactionIntent(argument, false);
            return intent2;
		
    

  @}

	extern(!android) void OnResult(int resultCode, object intent, object info)
  {
  }

	static extern(!android) object makeMyIntent(string amount)
  {
    return null;
  }
}