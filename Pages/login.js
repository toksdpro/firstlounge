var Observable = require("FuseJS/Observable");

var FileSystem = require("FuseJS/FileSystem");

var StaffID = Observable();

var Password = Observable();

// var apitoken = Observable();

var tokenpath = FileSystem.dataDirectory + "/" + "token.txt";

var staffIDpath = FileSystem.dataDirectory + "/" + "staffID.txt";

var categorypath = FileSystem.dataDirectory + "/" + "category.txt";



function saveToken(apitoken) {
	return FileSystem.writeTextToFile(tokenpath, apitoken);
}

function saveStaffID(StaffID) {
	return FileSystem.writeTextToFile(staffIDpath, StaffID);
	// .then(function() {
	//         // return FileSystem.readTextFromFile(staffIDpath);
	//         console.log('staffID was saved');
	//     })
	//     .catch(function(error) {
	//         console.log("Unable to save staffID due to error:" + error);
	//     });
}

function getStaffID() {
	return FileSystem.readTextFromFile(staffIDpath);
	    // .then(function(contents) {
	    //     console.log('staffID: '+contents);
	    //     return contents;
	    // }, function(error) {
	    //     console.log(error);
	    // });
	// return StaffID.value;
}

function getPassword() {
	return Password.value;
}

function getToken() {
	return FileSystem.readTextFromFile(tokenpath);
	    
	// return Token.value;
}

function logout() {
	saveToken("")
	.then(function() {
	        // return FileSystem.readTextFromFile(staffIDpath);
	        console.log('you have logged out successfully');
	    })
	    .catch(function(error) {
	        console.log("Unable to logout due to error:" + error);
	    });

	saveStaffID("")
	.then(function() {
	        // return FileSystem.readTextFromFile(staffIDpath);
	        console.log('stffId deleted successfully');
	    })
	    .catch(function(error) {
	        console.log("Unable to delete staffID due to error:" + error);
	    });
}

module.exports= {
	StaffID: StaffID,
	Password: Password,
	getStaffID: getStaffID,
	getPassword: getPassword,
	getToken: getToken,
	saveToken: saveToken,
	saveStaffID: saveStaffID,
	logout: logout
};