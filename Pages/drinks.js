var Observable = require("FuseJS/Observable");
// let FileSystem = require("FuseJS/FileSystem");
// var Backend = require("Backend.js");
// var lib = require("Modules/functions.js");
var drinksList = Observable();
var qty = Observable();

// get list of products from file
// var products = Backend.getProducts();
// get drink category
// var drink = "drinks";
// var drinksLists = lib.getProductListByCategory(drink, products);
// drinksList.replaceAll(drinksLists);

// cart = [];
// priceTotal = 100;
// quantityTotal = 0;

// var newdrinksList = lib.handleProductAction(drinksList);

// function checkout() {

// 	var cartString = JSON.stringify(cart);
// 	cartString = cartString + "&" + priceTotal.toString() + "&" + quantityTotal.toString();

// 	var param = {data: cartString};	

// 	console.log(cartString);

// 	router.push("checkout", param);
	
// 	//console.log("cart length "+cart.length);
// 	cart = [];
// 	priceTotal = 100;
// 	quantityTotal = 0;
// }

var fdb = new ForerunnerDB();
var db = fdb.db("flc_db");
var itemCollection = db.collection("inventory");

function Item(productName, category, price, quantity){
	var self = this;
	this._id = "";
	this.productName = productName;	
	this.category = category;
	this.price = price;
	this.quantity = quantity;
}


itemCollection.load(function (err, tableStats, metaStats) {
	if (!err) {
		// Load was successful
		result = itemCollection.find({
			category:{
				$eq: "drinks"
			}
		});
		if (tableStats.foundData && tableStats.rowCount > 0) {
			for (var item in result) {
				if (result[item].productName) {
					product = new Item(result[item].productName, result[item].category, result[item].price, result[item].quantity)
					// _product = result[task]._id	
					drinksList.add(product);
				}
			}
		} else {
			itemCollection.save();
		};
		// console.log("Load was successful");
		// console.log(JSON.stringify(result));
	}
});

function InventoryView(args){
	// console.log(JSON.stringify(args.data));
	var param = JSON.stringify(args.data);
	router.push("inventoryview", {data: param});
}

module.exports={
	drinksList: drinksList,
	InventoryView: InventoryView
};