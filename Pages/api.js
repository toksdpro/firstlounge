var Observable = require("FuseJS/Observable");

var data = Observable();

//var ROOT_URL = "http://restaurants.saddleng.com/api/v1/";
var ROOT_URL = "http://109.74.196.234/api/v1/";
//var ROOT_URL = "http://localhost:8000/api/v1/";

function isEmpty(str) {
	if (str != null) {
    	str = str.trim();
    	return (!str || 0 === str.length);
    }else{
    	return false;
    }
}

function apiFetch(path, options) {
	var url = encodeURI(ROOT_URL + path);
	console.log(url);
	if(options === undefined) {
		options = {};
	}
	
	// If a body is provided, serialize it as JSON and set the Content-Type header
	if(options.body !== undefined) {
		options = Object.assign({}, options, {
			body: JSON.stringify(options.body),
			headers: Object.assign({}, options.headers, {
				"Content-Type": "application/json"
			})
		});
	}
	
	// Fetch the resource and parse the response as JSON
	return fetch(url, options)
		.then(function(response) { 
			return response; // This returns a promise
			// console.log("ok: "+JSON.stringify(response.ok));
		});
}

function ApiLogin(StaffID, Password) {
	return apiFetch("staff/login", {
		method: "POST",
		body: {
			staff_id: StaffID,
			password: Password
		}
	});
}

function ApiStaffDetails(token) {
	return apiFetch("account", {
		headers: {
			Authorization: "Bearer " + token
		}
	});
}

function ApiCategory(token) {
	return apiFetch("categories", {
		headers: {
			Authorization: "Bearer " + token
		}
	});
}

function ApiCategoryCreate(token, category_name) {
	return apiFetch("category/create", {
		method: "POST",
		headers: {
			Authorization: "Bearer " + token
		},
		body: {
			category_name: category_name,
			category_image_file: "None",
			category_parent: "None"
			
		}
	});
}


function ApiCategoryProducts(token, category_id) {
	return apiFetch("products?category_id="+category_id, {
		headers: {
			Authorization: "Bearer " + token
		}
	});
}

function ApiProducts(token) {
	return apiFetch("products", {
		headers: {
			Authorization: "Bearer " + token
		}
	});
}

function ApiProductCreate(token, product_name, product_price, product_category_id) {
	//console.log("ID" + product_category_id);
	//console.log("AfterID" + parseInt(product_category_id, 10))
	return apiFetch("product/create", {
		method: "POST",
		headers: {
			Authorization: "Bearer " + token
		},
		body: {
			product_name: product_name,
			product_price: parseInt(product_price, 10),
			product_quantity: 0,
			product_category: parseInt(product_category_id, 10),
			product_image: "0"
			
		}
	});
}

function ApiProductDelete(token, productId, category_id) {
	console.log('productID: '+ productId);
	return apiFetch("product/delete", {
		method: "POST",
		headers: {
			Authorization: "Bearer " + token
		},
		body: {
			product_id: productId,
			category_id: category_id			
		}
	});
}


function ApiOrderCreate(token, StaffID, customer_name, payment_method, total_amount, items, discount_amount) {
	return apiFetch("order/create", {
		method: "POST",
		headers: {
			Authorization: "Bearer " + token
		},
		body: {
			staff_id: StaffID,
			customer_name: customer_name,
			payment_method: payment_method,
			total_amount: total_amount,
			items_ordered: items,
			discount_amount: discount_amount
		}
	});
}

function ApiOrderUpdate(token, orderId) {
	console.log('OrderID: '+orderId);
	return apiFetch("order/update", {
		method: "POST",
		headers: {
			Authorization: "Bearer " + token
		},
		body: {
			order_id: orderId,
			order_status: "Completed",				
		}
	});
}

function ApiOrders(token) {
	return apiFetch("orders", {
		headers: {
			Authorization: "Bearer " + token
		}
	});
}


module.exports = {
	ApiLogin: ApiLogin,
	isEmpty: isEmpty,
	ApiCategory: ApiCategory,
	ApiCategoryProducts: ApiCategoryProducts,
	ApiProducts: ApiProducts,
	ApiStaffDetails: ApiStaffDetails,
	ApiOrderCreate: ApiOrderCreate,
	ApiOrderUpdate: ApiOrderUpdate,
	ApiCategoryCreate: ApiCategoryCreate,
	ApiProductCreate: ApiProductCreate,
	ApiProductDelete: ApiProductDelete,
	ApiOrders: ApiOrders
};