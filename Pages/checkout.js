
var Observable = require("FuseJS/Observable");

var Backend = require("Modules/Backend.js");

var login = require('Pages/login.js');

var api = require('Pages/api.js');

var checkoutCartList = Observable();
var total = Observable("--");
var totalQuantity = Observable("--");
var totalNet = Observable("--");
var customerName = Observable();

var discountAmount = Observable(0);

var isCustomertDialogMessageShowing = Observable(false);

var isDiscountDialogMessageShowing = Observable(false);

var statusMsg = Observable(false);

var isPanelVisible = Observable("Visible");
// var isDialogShowing = Observable(false);

// var dialogMsg = Observable();

// var visible = Observable(true);

var resultString;
var cardHoldersName = "NetPlusPay";
var stres;
var checkoutCart;

var mposPay = require("Checkout");

    function callMpos() {
    	console.log("total: " + totalNet.value)
      	mposPay.mpos(""+totalNet.value);
      	
    }


	mposPay.on("messageReceived", function(message) {
    console.log("Message received " + message);
    var resultCode = mposPay.getMposResultCode();
    resultString = mposPay.getMposResult();
    cardHoldersName = mposPay.getCardHoldersName();
    console.log("Result Code " + resultCode);
    console.log("Result String " +resultString);
    console.log("Card Holders Name " +cardHoldersName);
    if (message == -1){
      		moveToHistory2();
      	}
});

var stresObj = this.Parameter.map(function(x) { return x.data; });

stresObj.onValueChanged(module, function(param) {
	console.log("Param: "+param);

	checkoutCart = JSON.parse(param);
	console.log("checkout: "+JSON.stringify(checkoutCart));
	checkoutCartList.replaceAll(checkoutCart);

	// get the total quantity
	var qty = 0;
	var price = 0;
	for (var i = 0; i < checkoutCart.length; i++) {
		qty += checkoutCart[i].quantityBought;
		price += checkoutCart[i].priceBought;
	}

	totalQuantity.value = qty;
	total.value = price;
	totalNet.value = total.value - discountAmount.value;
	Panel1Visibility();
	
});

function closeCustomerMessageDialog() {
	isCustomertDialogMessageShowing.value = false;
}

function closeDiscountMessageDialog() {
	isDiscountDialogMessageShowing.value = false;
}

function closeDialog() {
	// console.log('jhashja');
	isDialogShowing.value = false;
}

function retry() {
	console.log("kjsd");
	// LoadProductsByCategory(category.value.id);
}

function addDiscountDone() {
	if ((total.value - discountAmount.value) >= 0){
		isDiscountDialogMessageShowing.value = false;
		totalNet.value = total.value - discountAmount.value;
	}
	
}

function getStoreDate(){
	let now = new Date();
	var month = parseInt(now.getMonth()) + 1;
	var date = now.getDate() + "-" + month + "-" + now.getFullYear();
	return date;
}

function getStoreTime(){
	let now = new Date();
	var time = now.getHours() + ":" + now.getMinutes() + ":" + now.getSeconds();
	return time;
}

function countQty(arr) {
	var qty = 0;
	for (var i = 0; i < arr.length; i++) {
		qty += arr[i].quantityBought;
	}
	return qty;
}

function removeFromCart(args) {
	// console.log("it works here");
	
	//console.log(JSON.stringify(args.data._id));
	console.log(args.data.name);
	checkoutCartList.remove(args.data);
	for (var i = 0; i < Backend.cart.length; i++) {
		if (Backend.cart[i].name == args.data.name) {
			Backend.cart.splice(i, 1);
	}
}
	console.log(checkoutCartList.length);
	console.log(Backend.cart.length);
	//checkoutCartList.removeWhere(function(item){
	  //  return item._id == args.data._id;
	    
	//});
	for (var i = 0; i < checkoutCart.length; i++) {
		if (checkoutCart[i]._id == args.data._id) {
			checkoutCart.splice(i, 1);
		}
	}
	
	var qty = 0;
	var price = 0;
	
	checkoutCartList.forEach(function(i) {
		console.log(i); 
		qty += i.quantityBought;
		price += i.priceBought;
	});

	totalQuantity.value = qty;
	total.value = price;
	totalNet.value = total.value - discountAmount.value;
	Backend.quantityTotal.value = countQty(Backend.cart);
	if (Backend.quantityTotal.value == 0){
		totalQuantity.value = "--";
		total.value = "--";
		totalNet.value = "--";
		discountAmount.value = 0;
	}
	Panel1Visibility();
}

function Panel1Visibility(){
	if (checkoutCartList.length > 0)
		isPanelVisible.value = "Visible";
	else{
		isPanelVisible.value =  "Collapsed";
		
	}
	
}

function addCustomer() {
	isCustomertDialogMessageShowing.value = true;
}

function addDiscount() {
	isDiscountDialogMessageShowing.value = true;
}

function isEmpty(str) {
	if (str != null) {
    	str = str.trim();
    	return (!str || 0 === str.length);
    }else{
    	return false;
    }
}

function moveToHistory(){
	
	// var date = getStoreDate();
	// var time = getStoreTime();
	// var status = 1;

	console.log(JSON.stringify(customerName));

	if (checkoutCart != undefined) {
		var arr = [];
		var payment_method = 'cash';
		// function CreateOrder() {
			// reset error status
			// apistatus.clear();

			// format the orders
			for (var i = 0; i < checkoutCart.length; i++) {
				var prdt = {};
				prdt.product_id = checkoutCart[i].id;
				prdt.name = checkoutCart[i].name;
				prdt.price = checkoutCart[i].price;
				prdt.ordered_quantity = checkoutCart[i].quantityBought;

				arr.push(prdt);
				
				// console.log(JSON.stringify(prdt));
			}

			login.getToken()
			.then(function(contents) {
			    console.log('token: '+contents);
				if (contents != null) {
					// token.value = contents;
					// get staffID
					login.getStaffID()
					.then(function(staff_id) {

						if (staff_id != null) {
							if(!isEmpty(customerName.value)){
								CreateOrder(contents, staff_id, customerName.value, payment_method, total.value, JSON.stringify(arr), discountAmount.value);
							}
						}else{
							router.push("checkoutsuccess", {data: statusMsg.value});
							console.log('staff id error');
						}

					}, function(error) {
						router.push("checkoutsuccess", {data: statusMsg.value});
						console.log(error);
					});
				}else{
					console.log("Error not logged in");
				}
			}, function(error) {
				console.log(error);
			});

			function CreateOrder(token, StaffID, customerName, payment_method, total_amount, items, discount_amount) {
				api.ApiOrderCreate(token, StaffID, customerName, payment_method, total_amount, items, discount_amount)
				.then(function(response) {
					// console.log('tokenD: '+token);
					var ok = response.ok;
					var status = response.status;
					console.log('status: '+status);
					var response_msg = JSON.parse(response._bodyText);

					console.log("token: "+token);
					console.log("StaffID: "+StaffID);
					console.log("customerName: "+customerName);
					console.log("payment_method: "+payment_method);
					console.log("total_amount: "+total_amount);
					console.log("items: "+items);
					console.log("discount_amount: "+discount_amount);

					console.log("status: "+status);




					// hide loader
					// visible.value = false;

					if (ok) {
						// console.log("error");
						// isDialogShowing.value = false;
						console.log("it workds");
						statusMsg.value = true;

						// reset cart
						checkoutCart = [];
						checkoutCartList.replaceAll(checkoutCart);
						Backend.quantityTotal.value = 0
						Backend.setCartEmpty();
						router.push("checkoutsuccess", {data: statusMsg.value});
						// console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).orders));
						// var arr = [];
						// arr.push(JSON.parse(response._bodyText).categories);
						// categoryList.replaceAll(JSON.parse(response._bodyText).orders);
						// apistatus.value = status;
					}else{

						// isDialogShowing.value = true;
						// console.log("isDialogShowing.value: "+isDialogShowing.value);
						router.push("checkoutsuccess", {data: statusMsg.value});
						if (status == 502) {
							console.log("Network Error");
							// error.value = "Network error";
							
							// dialogMsg.value = "Network Error";

						}else {
							console.log(status);	
							// error.value = "There was an error";
							// dialogMsg.value = "There was an error";
						}
						
					}
					// apistatus.value = status;

				}).catch(function(err) {
					// console.log('here');
					// apistatus.value = 401;

					router.push("checkoutsuccess", {data: statusMsg.value});

					// visible.value = false;

					// isDialogShowing.value = true;
					// dialogMsg.value = "Network Error";

					// dialogMsg.value = "There was an error";

					// console.log('here: '+error.value);

					console.log("There was an error");
				});
			}

			
			// return categoryList;
		// }

		// for (var item in checkoutCart) {
		// 	if (checkoutCart[item].productName) {
		// 		arr = new ItemHistory(checkoutCart[item].productName, checkoutCart[item].category, checkoutCart[item].priceBought, checkoutCart[item].quantityBought, date, time, status);
		// 		// arr._id = checkoutCart[item]._id;
		// 		// remove the item qty from inventory, homepage and database
		// 		// var item_id = checkoutCart[item]._id;
		// 		Backend.UpdateStockQty(checkoutCart[item]._id, checkoutCart[item].quantityBought);
		// 		new_arr.push(arr);
		// 	}
		// }

		// Backend.MoveToHistory(new_arr);


		


		// router.push("checkoutsuccess");

	}else{
		console.log("empty");
	}
}

function moveToHistory2(){


	if (checkoutCart != undefined) {
		var arr = [];
		var payment_method = 'card';
		
			for (var i = 0; i < checkoutCart.length; i++) {
				var prdt = {};
				prdt.product_id = checkoutCart[i].id;
				prdt.name = checkoutCart[i].name;
				prdt.price = checkoutCart[i].price;
				prdt.ordered_quantity = checkoutCart[i].quantityBought;

				arr.push(prdt);
				
				// console.log(JSON.stringify(prdt));
			}

			login.getToken()
			.then(function(contents) {
			    console.log('token: '+contents);
				if (contents != null) {
					// token.value = contents;
					// get staffID
					login.getStaffID()
					.then(function(staff_id) {

						if (staff_id != null) {
							//if(!isEmpty(resultString)){
								CreateOrder(contents, staff_id, cardHoldersName, payment_method, total.value, JSON.stringify(arr), discountAmount.value);
							//}
						}else{
							router.push("checkoutsuccess", {data: statusMsg.value});
							console.log('staff id error');
						}

					}, function(error) {
						router.push("checkoutsuccess", {data: statusMsg.value});
						console.log(error);
					});
				}else{
					console.log("Error not logged in");
				}
			}, function(error) {
				console.log(error);
			});

			function CreateOrder(token, StaffID, customerName, payment_method, total_amount, items, discount_amount) {
				api.ApiOrderCreate(token, StaffID, customerName, payment_method, total_amount, items, discount_amount)
				.then(function(response) {
					// console.log('tokenD: '+token);
					var ok = response.ok;
					var status = response.status;
					console.log('status: '+status);
					var response_msg = JSON.parse(response._bodyText);

					console.log("token: "+token);
					console.log("StaffID: "+StaffID);
					console.log("customerName: "+customerName);
					console.log("payment_method: "+payment_method);
					console.log("total_amount: "+total_amount);
					console.log("items: "+items);
					console.log("discount_amount: "+discount_amount);

					console.log("status: "+status);




					// hide loader
					// visible.value = false;

					if (ok) {
						// console.log("error");
						// isDialogShowing.value = false;
						console.log("it workds");
						statusMsg.value = true;

						// reset cart
						checkoutCart = [];
						checkoutCartList.replaceAll(checkoutCart);
						Backend.quantityTotal.value = 0
						Backend.setCartEmpty();
						router.push("checkoutsuccess", {data: statusMsg.value});
						// console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).orders));
						// var arr = [];
						// arr.push(JSON.parse(response._bodyText).categories);
						// categoryList.replaceAll(JSON.parse(response._bodyText).orders);
						// apistatus.value = status;
					}else{

						// isDialogShowing.value = true;
						// console.log("isDialogShowing.value: "+isDialogShowing.value);
						router.push("checkoutsuccess", {data: statusMsg.value});
						if (status == 502) {
							console.log("Network Error");
							// error.value = "Network error";
							
							// dialogMsg.value = "Network Error";

						}else {
							console.log(status);	
							// error.value = "There was an error";
							// dialogMsg.value = "There was an error";
						}
						
					}
					// apistatus.value = status;

				}).catch(function(err) {
					// console.log('here');
					// apistatus.value = 401;

					router.push("checkoutsuccess", {data: statusMsg.value});

					// visible.value = false;

					// isDialogShowing.value = true;
					// dialogMsg.value = "Network Error";

					// dialogMsg.value = "There was an error";

					// console.log('here: '+error.value);

					console.log("There was an error");
				});
			}

			
			// return categoryList;
		// }

		// for (var item in checkoutCart) {
		// 	if (checkoutCart[item].productName) {
		// 		arr = new ItemHistory(checkoutCart[item].productName, checkoutCart[item].category, checkoutCart[item].priceBought, checkoutCart[item].quantityBought, date, time, status);
		// 		// arr._id = checkoutCart[item]._id;
		// 		// remove the item qty from inventory, homepage and database
		// 		// var item_id = checkoutCart[item]._id;
		// 		Backend.UpdateStockQty(checkoutCart[item]._id, checkoutCart[item].quantityBought);
		// 		new_arr.push(arr);
		// 	}
		// }

		// Backend.MoveToHistory(new_arr);


		


		// router.push("checkoutsuccess");

	}else{
		console.log("empty");
	}
}
module.exports = {
    checkoutCart: checkoutCartList,
    totalPrice: total,
    totalQuantity: totalQuantity,
    moveToHistory: moveToHistory,
    removeFromCart: removeFromCart,   
    Panel1Visibility : isPanelVisible,
    isCustomertDialogMessageShowing: isCustomertDialogMessageShowing,
    addCustomer: addCustomer,
    closeCustomerMessageDialog: closeCustomerMessageDialog,
    totalNet: totalNet,
    customerName: customerName,

    discountAmount: discountAmount,

    closeDiscountMessageDialog: closeDiscountMessageDialog,
    isDiscountDialogMessageShowing: isDiscountDialogMessageShowing,
    addDiscount: addDiscount,
    addDiscountDone: addDiscountDone,
    callMpos: callMpos,

    // isDialogShowing: isDialogShowing,
    // dialogMsg: dialogMsg,
    // visible: visible
    // stres: stres
};