var Observable = require("FuseJS/Observable");
// var productDetails = Observable();
var Backend = require("Modules/Backend.js");
var productDetails = Observable();


var qty = Observable(0);

var isCheckOutDialogMessageShowing = Observable(false);
var dialogMsg = Observable("Added to Cart!!!");

var stresObj = this.Parameter.map(function(x) { return x.data; });
	stresObj.onValueChanged(module, function(param) {
	var data = [];
	data[0] = JSON.parse(param);
				
	productDetails.replaceAll(data);
						
	});


function isEmpty(str) {
	if(!str || str.length === 0){
		return true;
	}else{
		return false;
	}
}

function findProductById(productArray, productID) { 
    var found = -1;
	for(var i = 0; i < productArray.length; i++) {
	    if (productArray[i].id == productID) {
	        found = i;
	        break;
	    }
	}
	return found;
}


function closeCheckOutMessageDialog() {
	isCheckOutDialogMessageShowing.value = false;
}


var temp_product = Observable();
quantity = 0;
test = {};

function addItem(args){
	var product = {};
	qty.value += 1;
	product = args.data;
	product.quantityBought = qty.value;
	product.priceBought = product.price * product.quantityBought;

	var data = [];
	data[0] = product;

	qty.value = product.quantityBought;

	temp_product.replaceAll(data);

	// test = product;
	// console.log(JSON.stringify(test));

	// console.log(JSON.stringify(temp_product.value));
}

function resetOrder(){
	qty.value = 0;
	router.goBack();
}

function removeItem(args){
	var product = {};
	if (qty.value != 0) {
		qty.value -= 1;
		product = args.data;
		product.quantityBought = qty.value;
		product.priceBought = product.price * product.quantityBought;

		var data = [];
		data[0] = product;

		qty.value = product.quantityBought;

		
		// test = product;
		// console.log(JSON.stringify(test));
		temp_product.replaceAll(data);
	}

	// console.log(JSON.stringify(temp_product.value));
}

function countQty(arr) {
	var qty = 0;
	for (var i = 0; i < arr.length; i++) {
		qty += arr[i].quantityBought;
	}
	return qty;
}

function addToCart(){
	// var product = {};
	console.log("qty: "+qty.value);
	if (qty.value != 0) {
		qty.value = 0;
		// is cart empty
		// Backend.quantityTotal.clear();
		if (Backend.cart.length == 0) {
			// add temp data to cart
			
			console.log("cart: "+JSON.stringify(Backend.cart));
			// console.log("temp data unseason: "+temp_product.value);
			console.log("temp data: "+JSON.stringify(temp_product.value));
			Backend.cart.push(temp_product.value);
			//Backend.quantityTotal.value = countQty(Backend.cart);
			Backend.quantityTotal.value = countQty(Backend.cart);
			console.log("quantityTotal: "+Backend.quantityTotal.value);
		}else{
			// console.log("cart: "+JSON.stringify(cart));

			console.log("temp data: "+JSON.stringify(temp_product.value));
			// check if data exists in the array
			var isInArray = findProductById(Backend.cart, temp_product.value._id)
			// console.log(isInArray);
			// if it exists, change data
			if (isInArray >= 0 ) {
				console.log("new: "+JSON.stringify(Backend.cart));
			}else{
				Backend.cart.push(temp_product.value);
				console.log("first time: "+JSON.stringify(Backend.cart));
				Backend.quantityTotal.value = countQty(Backend.cart);
			}

		}

		isCheckOutDialogMessageShowing.value = true;

	}else{
		console.log("its empty");
	}
}

	

function gocheckout() {
	console.log('got here');
	console.log("quantityTotal: "+Backend.quantityTotal.value);
	if (Backend.quantityTotal.value > 0) {
		var cartString = JSON.stringify(Backend.cart);
		// console.log(cartString);
		// cartString = cartString + "&" + priceTotal.toString() + "&" + quantityTotal.toString();

		var param = {data: cartString};	

		console.log(cartString);

		isCheckOutDialogMessageShowing.value = false;
		
		//console.log("cart length "+cart.length);
		Backend.setCartEmpty();
		
		router.push("checkout", param);
	}
}


module.exports = {
	addItem: addItem,
	removeItem: removeItem,
	qty: qty,
	gocheckout: gocheckout,

	resetOrder: resetOrder,
	addToCart: addToCart,

	productDetails: productDetails,

	dialogMsg: dialogMsg,
	isCheckOutDialogMessageShowing: isCheckOutDialogMessageShowing,
	closeCheckOutMessageDialog: closeCheckOutMessageDialog
			
};