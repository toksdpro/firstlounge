var Observable = require("FuseJS/Observable");

var historyList = Observable();

var login = require('Pages/login.js');

var api = require('Pages/api.js');

var Backend = require('Modules/Backend.js');

var visible = Observable(true);

var isDialogShowing = Observable(false);

var dialogMsg = Observable();

var someDate = Observable(new Date(Date.parse("2007-02-14T00:00:00.000Z")));

someDate.onValueChanged(module, function(date) {
	console.log("someDate changed: " + JSON.stringify(date));
 });



function retry() {
	LoadCategory();
}

function LoadOrders() {
	// reset error status
	// apistatus.clear();

	login.getToken()
	.then(function(contents) {
	    console.log('token: '+contents);
		if (contents != null) {
			// token.value = contents;
			GetOrders(contents);
		}else{
			console.log("Error not logged in");
		}
	}, function(error) {
		console.log(error);
	});

	function GetOrders(token) {
		api.ApiOrders(token)
		.then(function(response) {
			// console.log('tokenD: '+token);
			var ok = response.ok;
			var status = response.status;
			console.log('status: '+status);
			var response_msg = JSON.parse(response._bodyText);

			// hide loader
			visible.value = false;

			if (ok) {
				// console.log("error");
				isDialogShowing.value = false;
				console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).orders));
				// var arr = [];
				// arr.push(JSON.parse(response._bodyText).categories);
				historyList.replaceAll(JSON.parse(response._bodyText).orders);
				// apistatus.value = status;
			}else{

				isDialogShowing.value = true;
				console.log("isDialogShowing.value: "+isDialogShowing.value);

				if (status == 502) {
					console.log("Network Error");
					// error.value = "Network error";
					
					dialogMsg.value = "Network Error";

				}else {
					console.log(status);	
					// error.value = "There was an error";
					dialogMsg.value = "There was an error";
				}
				
			}
			// apistatus.value = status;

		}).catch(function(err) {
			// console.log('here');
			// apistatus.value = 401;

			visible.value = false;

			isDialogShowing.value = true;
			dialogMsg.value = "Network Error";

			// dialogMsg.value = "There was an error";

			// console.log('here: '+error.value);

			console.log("There was an error");
		});
	}

	
	// return historyList;
}


function updateMe(args){
	console.log(args.data.order_id);	
	//saveCategoryTitle(args.data.name);
	
	//var param = JSON.stringify(args.data);
	
	login.getToken()
			.then(function(contents) {
			    console.log('token: '+contents);
				if (contents != null) {
					// token.value = contents;
					// get staffID
					//console.log("See mee here");
					updateOrder(contents, args.data.order_id);
					console.log("Something is happening");
				}else{
					console.log("Error not logged in");
					LoadOrders();
				}
			}, function(error) {
				console.log(error);
			});

}

function updateOrder(token, orderId) {
	
				api.ApiOrderUpdate(token, orderId)
				.then(function(response) {
					// console.log('tokenD: '+token);
					var ok = response.ok;
					var status = response.status;
					console.log('status: '+status);
					var response_msg = JSON.parse(response._bodyText);

					if (ok) {						
						console.log("it workds");
						
						LoadOrders(); 

					}else{

						console.log("Error Ocured");
						LoadOrders();
						if (status == 502) {
							console.log("Network Error");							

						}else {
							console.log(status);	
							
						}
						
					}
					// apistatus.value = status;

				}).catch(function(err) {					

					LoadOrders();
					
					console.log("There was an error");
				});
}

LoadOrders() 

function showDialog() {
	//
	//
	isDialogShowing.value = true;
}

function closeDialog() {
	//
	//
	isDialogShowing.value = false;
}

module.exports = {
	historyList: historyList,
	updateMe: updateMe,	
	showDialog: showDialog,
	closeDialog: closeDialog,
	retry: retry,
	dialogMsg: dialogMsg,
	isDialogShowing: isDialogShowing,
	visible: visible,
	someDate: someDate,

    minDate: new Date(Date.parse("1950-01-01T00:00:00.000Z")),
    maxDate: new Date(Date.parse("2050-01-01T00:00:00.000Z")),

    whoYouGonnaCall: function() {
     someDate.value = new Date(Date.parse("1984-06-08T00:00:00.000Z"));
 }
};