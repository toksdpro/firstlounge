var Observable = require("FuseJS/Observable");

var FileSystem = require("FuseJS/FileSystem");

var login = require('Pages/login.js');

var api = require('Pages/api.js');

var Backend = require('Modules/Backend.js');

var visible = Observable(true);

var isDialogShowing = Observable(false);

var dialogMsg = Observable();

var categoryList = Observable();

var StaffID = Observable();

searchString = Observable("");

// ignore case so just convert both to lowercase before comparison
function stringContainsString(main, filter){
	return main === '' || main.toLowerCase().indexOf(filter.toLowerCase()) !== -1;
}

var filteredItems = searchString.flatMap(function(searchValue) {
    return categoryList.where(function(item) { return stringContainsString(item.name, searchValue); });
});


function categoryProductview(args){
	//console.log(JSON.stringify(args.data));	
	//saveCategoryTitle(args.data.name);
	
	var param = JSON.stringify(args.data);
	// console.log("param: "+param);

	router.push("categoryproduct", {data: param});
}

function closeDialog() {
	isDialogShowing.value = false;
}

function retry() {
	LoadCategory();
}


function LoadCategory() {
	// reset error status
	// apistatus.clear();

	login.getToken()
	.then(function(contents) {
	    console.log('token: '+contents);
		if (contents != null) {
			// token.value = contents;
			GetCategories(contents);
		}else{
			console.log("Error not logged in");
		}
	}, function(error) {
		console.log(error);
	});

	function GetCategories(token) {
		api.ApiCategory(token)
		.then(function(response) {
			// console.log('tokenD: '+token);
			var ok = response.ok;
			var status = response.status;
			console.log('status: '+status);
			var response_msg = JSON.parse(response._bodyText);

			// hide loader
			visible.value = false;

			if (ok) {
				// console.log("error");
				isDialogShowing.value = false;
				console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).categories));
				// var arr = [];
				// arr.push(JSON.parse(response._bodyText).categories);
				categoryList.replaceAll(JSON.parse(response._bodyText).categories);
				// apistatus.value = status;
			}else{

				isDialogShowing.value = true;
				console.log("isDialogShowing.value: "+isDialogShowing.value);

				if (status == 502) {
					console.log("Network Error");
					// error.value = "Network error";
					
					dialogMsg.value = "Network Error";

				}else {
					console.log(status);	
					// error.value = "There was an error";
					dialogMsg.value = "There was an error";
				}
				
			}
			// apistatus.value = status;

		}).catch(function(err) {
			// console.log('here');
			// apistatus.value = 401;

			visible.value = false;

			isDialogShowing.value = true;
			dialogMsg.value = "Network Error";

			// dialogMsg.value = "There was an error";

			// console.log('here: '+error.value);

			console.log("There was an error");
		});
	}

	
	// return categoryList;
}

// console.log("token: "+ login.StaffID.value);

LoadCategory();
// setInterval(function(){ LoadCategory(); }, 20000);


module.exports = {
	homepageList: categoryList,

	StaffID: Backend.getStaffDetails(),

	categoryProductview: categoryProductview,

	dialogMsg: dialogMsg,

	isDialogShowing: isDialogShowing,

	visible: visible,

	closeDialog: closeDialog,

	retry: retry,

	filteredItems: filteredItems, 

	searchString: searchString 
};