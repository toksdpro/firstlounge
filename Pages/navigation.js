var Observable = require("FuseJS/Observable");

var Backend = require("Modules/Backend.js");

var login = require('Pages/login.js');


function home() {
	router.push("home");	
}

function inventory() {
	router.push("inventory");	
}

function history() {
	router.push("history");	
}

function drinks() {

	router.push("drinks");	
}

function food() {

	router.push("food");	
}

function SearchPage(){
	router.push("search");
}

function Settings(){
	router.push("settings");
}

function checkout() {
	if (Backend.quantityTotal.value > 0) {
		var cartString = JSON.stringify(Backend.cart);
		var param = {data: cartString};	

		console.log(cartString);

		Backend.setCartEmpty();

		router.push("checkout", param);
	}
}


function goBack() {
	router.goBack();
}

function logout() {
	login.logout();
	router.push("login");
}

// console.log(login.Token.value);

module.exports = {
	home: home,
	inventory: inventory,
	history: history,
	closeEdge: function() {
	  	 //router.push("detail");
	  	 EdgeNav.dismiss();
	   },
	goBack: goBack,
	food: food,
	drinks: drinks,
	SearchPage: SearchPage,
	Settings: Settings,
	checkout: checkout,
	logout: logout,
	quantityTotal: Backend.quantityTotal
}