[Uno.Compiler.UxGenerated]
public partial class CheckoutList: Fuse.Controls.DockPanel
{
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb8;
    static CheckoutList()
    {
    }
    [global::Uno.UX.UXConstructor]
    public CheckoutList()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("name");
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("quantityBought");
        var temp2 = new global::Fuse.Controls.Text();
        temp2_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp2, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("priceBought");
        var temp6 = new global::Fuse.Reactive.Data("removeFromCart");
        var temp7 = new global::Fuse.Controls.Panel();
        var temp8 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp9 = new global::Fuse.Controls.Panel();
        var temp10 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp11 = new global::Fuse.Controls.Panel();
        var temp12 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::Fuse.Controls.Panel();
        var temp14 = new global::Fuse.Controls.Image();
        temp_eb8 = new global::Fuse.Reactive.EventBinding(temp6);
        var temp15 = new global::Fuse.Controls.Rectangle();
        var temp16 = new global::Fuse.Drawing.Stroke();
        var temp17 = new global::Fuse.Drawing.StaticSolidColor(float4(0.7333333f, 0.5490196f, 0.007843138f, 1f));
        this.Color = float4(1f, 1f, 1f, 1f);
        this.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        this.Alignment = Fuse.Elements.Alignment.TopCenter;
        this.Padding = float4(10f, 10f, 10f, 10f);
        this.SourceLineNumber = 6;
        this.SourceFileName = "Pages/checkout.ux";
        temp7.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Percent);
        temp7.SourceLineNumber = 7;
        temp7.SourceFileName = "Pages/checkout.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp7, Fuse.Layouts.Dock.Left);
        temp7.Children.Add(temp);
        temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp.FontSize = 12f;
        temp.TextAlignment = Fuse.Controls.TextAlignment.Left;
        temp.TextColor = float4(0f, 0f, 0f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.SourceLineNumber = 8;
        temp.SourceFileName = "Pages/checkout.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp8);
        temp3.SourceLineNumber = 8;
        temp3.SourceFileName = "Pages/checkout.ux";
        temp9.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Percent);
        temp9.SourceLineNumber = 10;
        temp9.SourceFileName = "Pages/checkout.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp9, Fuse.Layouts.Dock.Left);
        temp9.Children.Add(temp1);
        temp1.FontSize = 12f;
        temp1.TextAlignment = Fuse.Controls.TextAlignment.Left;
        temp1.TextColor = float4(0f, 0f, 0f, 1f);
        temp1.SourceLineNumber = 11;
        temp1.SourceFileName = "Pages/checkout.ux";
        temp1.Font = global::LoginPage.Poppins;
        temp1.Bindings.Add(temp10);
        temp4.SourceLineNumber = 11;
        temp4.SourceFileName = "Pages/checkout.ux";
        temp11.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Percent);
        temp11.SourceLineNumber = 13;
        temp11.SourceFileName = "Pages/checkout.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp11, Fuse.Layouts.Dock.Left);
        temp11.Children.Add(temp2);
        temp2.FontSize = 12f;
        temp2.TextAlignment = Fuse.Controls.TextAlignment.Left;
        temp2.TextColor = float4(0f, 0f, 0f, 1f);
        temp2.SourceLineNumber = 14;
        temp2.SourceFileName = "Pages/checkout.ux";
        temp2.Font = global::LoginPage.Poppins;
        temp2.Bindings.Add(temp12);
        temp5.SourceLineNumber = 14;
        temp5.SourceFileName = "Pages/checkout.ux";
        temp13.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        temp13.Width = new Uno.UX.Size(5f, Uno.UX.Unit.Percent);
        temp13.SourceLineNumber = 16;
        temp13.SourceFileName = "Pages/checkout.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Left);
        temp13.Children.Add(temp14);
        temp14.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp14.SourceLineNumber = 17;
        temp14.SourceFileName = "Pages/checkout.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp14, temp_eb8.OnEvent);
        temp14.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/remove.png"));
        temp14.Bindings.Add(temp_eb8);
        temp6.SourceLineNumber = 17;
        temp6.SourceFileName = "Pages/checkout.ux";
        temp15.MaxHeight = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        temp15.Offset = new Uno.UX.Size2(new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified), new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified));
        temp15.Layer = Fuse.Layer.Background;
        temp15.SourceLineNumber = 19;
        temp15.SourceFileName = "Pages/checkout.ux";
        temp15.Strokes.Add(temp16);
        temp16.Color = float4(0f, 1f, 0f, 1f);
        temp16.Width = 1f;
        temp16.Brush = temp17;
        this.Children.Add(temp7);
        this.Children.Add(temp9);
        this.Children.Add(temp11);
        this.Children.Add(temp13);
        this.Children.Add(temp15);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
