[Uno.Compiler.UxGenerated]
public partial class ComboBox: Fuse.Controls.Panel
{
    object _field_Options;
    [global::Uno.UX.UXOriginSetter("SetOptions")]
    public object Options
    {
        get { return _field_Options; }
        set { SetOptions(value, null); }
    }
    public void SetOptions(object value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Options)
        {
            _field_Options = value;
            OnPropertyChanged("Options", origin);
        }
    }
    object _field_Selected;
    [global::Uno.UX.UXOriginSetter("SetSelected")]
    public object Selected
    {
        get { return _field_Selected; }
        set { SetSelected(value, null); }
    }
    public void SetSelected(object value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Selected)
        {
            _field_Selected = value;
            OnPropertyChanged("Selected", origin);
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ComboBox __parent;
        [Uno.WeakReference] internal readonly ComboBox __parentInstance;
        public Template(ComboBox parent, ComboBox parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> txt_Value_inst;
        global::Uno.UX.Property<float4> __self_Color_inst;
        global::Uno.UX.Property<float4> txt_Color_inst;
        global::Uno.UX.Property<bool> temp_Value_inst;
        internal global::Fuse.Controls.Text txt;
        internal global::Fuse.Reactive.EventBinding temp_eb19;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            txt = new global::Fuse.Controls.Text();
            txt_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(txt, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("title");
            __self_Color_inst = new FirstLoungeNewApp_FuseControlsPanel_Color_Property(__self, __selector1);
            txt_Color_inst = new FirstLoungeNewApp_FuseControlsTextControl_Color_Property(txt, __selector1);
            var temp = new global::Fuse.Triggers.WhileTrue();
            temp_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp, __selector0);
            var temp2 = new global::Fuse.Reactive.Data("isSelected");
            var temp3 = new global::Fuse.Reactive.Data("clicked");
            var temp4 = new global::Fuse.Reactive.DataBinding(txt_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
            var temp5 = new global::Fuse.Gestures.WhilePressed();
            var temp6 = new global::Fuse.Animations.Change<float4>(__self_Color_inst);
            var temp7 = new global::Fuse.Animations.Change<float4>(__self_Color_inst);
            var temp8 = new global::Fuse.Animations.Change<float4>(txt_Color_inst);
            var temp9 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
            temp_eb19 = new global::Fuse.Reactive.EventBinding(temp3);
            __self.Color = float4(0.945098f, 0.945098f, 0.945098f, 1f);
            __self.Name = __selector2;
            __self.SourceLineNumber = 52;
            __self.SourceFileName = "Pages/ComboBox.ux";
            global::Fuse.Gestures.Clicked.AddHandler(__self, temp_eb19.OnEvent);
            txt.Margin = float4(20f, 10f, 20f, 10f);
            txt.Name = __selector3;
            txt.SourceLineNumber = 53;
            txt.SourceFileName = "Pages/ComboBox.ux";
            txt.Bindings.Add(temp4);
            temp1.SourceLineNumber = 53;
            temp1.SourceFileName = "Pages/ComboBox.ux";
            temp5.SourceLineNumber = 54;
            temp5.SourceFileName = "Pages/ComboBox.ux";
            temp5.Animators.Add(temp6);
            temp6.Value = float4(0.8666667f, 0.8666667f, 0.8666667f, 1f);
            temp6.Duration = 0.05;
            temp6.DurationBack = 0.1;
            temp.SourceLineNumber = 57;
            temp.SourceFileName = "Pages/ComboBox.ux";
            temp.Animators.Add(temp7);
            temp.Animators.Add(temp8);
            temp.Bindings.Add(temp9);
            temp7.Value = float4(0.7333333f, 0.5490196f, 0.007843138f, 1f);
            temp8.Value = float4(1f, 1f, 1f, 1f);
            temp2.SourceLineNumber = 57;
            temp2.SourceFileName = "Pages/ComboBox.ux";
            temp3.SourceLineNumber = 52;
            temp3.SourceFileName = "Pages/ComboBox.ux";
            __self.Children.Add(txt);
            __self.Children.Add(temp5);
            __self.Children.Add(temp);
            __self.Bindings.Add(temp_eb19);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "Color";
        static global::Uno.UX.Selector __selector2 = "item";
        static global::Uno.UX.Selector __selector3 = "txt";
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<float4> header_Color_inst;
    global::Uno.UX.Property<object> temp1_Items_inst;
    global::Uno.UX.Property<float> dropdown_Opacity_inst;
    global::Uno.UX.Property<Fuse.Elements.Visibility> dropdown_Visibility_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<object> this_Options_inst;
    global::Uno.UX.Property<object> this_Selected_inst;
    internal global::Fuse.Controls.Panel header;
    internal global::Fuse.Reactive.EventBinding temp_eb18;
    internal global::Fuse.Controls.Panel dropdown;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "header",
        "temp_eb18",
        "dropdown"
    };
    static ComboBox()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ComboBox()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        this_Options_inst = new FirstLoungeNewApp_ComboBox_Options_Property(this, __selector0);
        this_Selected_inst = new FirstLoungeNewApp_ComboBox_Selected_Property(this, __selector1);
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector2);
        var temp3 = new global::Fuse.Reactive.Data("selected");
        header = new global::Fuse.Controls.Panel();
        header_Color_inst = new FirstLoungeNewApp_FuseControlsPanel_Color_Property(header, __selector3);
        var temp4 = new global::Fuse.Reactive.Data("toggleOpen");
        var temp1 = new global::Fuse.Reactive.Each();
        temp1_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp1, __selector4);
        var temp5 = new global::Fuse.Reactive.Data("options");
        dropdown = new global::Fuse.Controls.Panel();
        dropdown_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(dropdown, __selector5);
        dropdown_Visibility_inst = new FirstLoungeNewApp_FuseElementsElement_Visibility_Property(dropdown, __selector6);
        var temp2 = new global::Fuse.Triggers.WhileFalse();
        temp2_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp2, __selector2);
        var temp6 = new global::Fuse.Reactive.Data("isOpen");
        var temp7 = new global::Fuse.Controls.Rectangle();
        var temp8 = new global::Fuse.Drawing.Stroke();
        var temp9 = new global::Fuse.Drawing.StaticSolidColor(float4(0.4666667f, 0.4666667f, 0.4666667f, 1f));
        var temp10 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp11 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::Fuse.Controls.Text();
        var temp13 = new global::Fuse.Gestures.WhilePressed();
        var temp14 = new global::Fuse.Animations.Change<float4>(header_Color_inst);
        temp_eb18 = new global::Fuse.Reactive.EventBinding(temp4);
        var temp15 = new global::Fuse.Translation();
        var temp16 = new global::Fuse.Controls.ScrollView();
        var temp17 = new global::Fuse.Controls.StackPanel();
        var item = new Template(this, this);
        var temp18 = new global::Fuse.Reactive.DataBinding(temp1_Items_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp19 = new global::Fuse.Animations.Change<float>(dropdown_Opacity_inst);
        var temp20 = new global::Fuse.Animations.Change<Fuse.Elements.Visibility>(dropdown_Visibility_inst);
        var temp21 = new global::Fuse.Animations.Move();
        var temp22 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        this.Color = float4(0.945098f, 0.945098f, 0.945098f, 1f);
        this.Alignment = Fuse.Elements.Alignment.Top;
        this.Margin = float4(0f, 5f, 0f, 5f);
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/ComboBox.ux";
        temp7.Layer = Fuse.Layer.Background;
        temp7.SourceLineNumber = 4;
        temp7.SourceFileName = "Pages/ComboBox.ux";
        temp7.Strokes.Add(temp8);
        temp8.Color = float4(0f, 1f, 0f, 1f);
        temp8.Width = 1.4f;
        temp8.Brush = temp9;
        temp10.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\n\t\tvar self = this;\n\n\t\tvar options = self.Options.inner();\n\t\tvar selected = self.Selected.inner();\n\t\tvar isOpen = Observable(false);\n\t\tmodule.exports = {\n\t\t\tisOpen: isOpen,\n\t\t\ttoggleOpen: function() {\n\t\t\t\tisOpen.value = !isOpen.value;\n\t\t\t},\n\t\t\toptions: options.map(function(option){\n\t\t\t\treturn{\n\t\t\t\t\ttitle: option,\n\t\t\t\t\tisSelected: Observable(function() {\n\t\t\t\t\t\treturn selected.value === option;\n\t\t\t\t\t}),\n\t\t\t\t\tclicked: function() {\n\t\t\t\t\t\tselected.value = option;\n\t\t\t\t\t\tif (self.Selected.value instanceof Observable) {\n\t\t\t\t\t\t\tself.Selected.value.value = option;\n\t\t\t\t\t\t}\n\t\t\t\t\t\tisOpen.value = !isOpen.value;\n\t\t\t\t\t}\n\t\t\t\t}\n\t\t\t}),\n\t\t\tselected: selected\n\t\t}\n\t";
        temp10.LineNumber = 7;
        temp10.FileName = "Pages/ComboBox.ux";
        temp10.SourceLineNumber = 7;
        temp10.SourceFileName = "Pages/ComboBox.ux";
        header.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        header.Name = __selector7;
        header.SourceLineNumber = 39;
        header.SourceFileName = "Pages/ComboBox.ux";
        global::Fuse.Gestures.Clicked.AddHandler(header, temp_eb18.OnEvent);
        header.Children.Add(temp);
        header.Children.Add(temp12);
        header.Children.Add(temp13);
        header.Bindings.Add(temp_eb18);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.Margin = float4(20f, 5f, 20f, 5f);
        temp.SourceLineNumber = 40;
        temp.SourceFileName = "Pages/ComboBox.ux";
        temp.Bindings.Add(temp11);
        temp3.SourceLineNumber = 40;
        temp3.SourceFileName = "Pages/ComboBox.ux";
        temp12.Value = "\uF0D7";
        temp12.FontSize = 26f;
        temp12.Alignment = Fuse.Elements.Alignment.CenterRight;
        temp12.Margin = float4(20f, 5f, 20f, 5f);
        temp12.SourceLineNumber = 41;
        temp12.SourceFileName = "Pages/ComboBox.ux";
        temp12.Font = global::LoginPage.FontAwesome;
        temp13.SourceLineNumber = 42;
        temp13.SourceFileName = "Pages/ComboBox.ux";
        temp13.Animators.Add(temp14);
        temp14.Value = float4(0.7333333f, 0.7333333f, 0.7333333f, 1f);
        temp14.Duration = 0.05;
        temp14.DurationBack = 0.1;
        temp4.SourceLineNumber = 39;
        temp4.SourceFileName = "Pages/ComboBox.ux";
        dropdown.Color = float4(0.945098f, 0.945098f, 0.945098f, 1f);
        dropdown.MaxHeight = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        dropdown.Alignment = Fuse.Elements.Alignment.Top;
        dropdown.LayoutRole = Fuse.LayoutRole.Inert;
        dropdown.Name = __selector8;
        dropdown.SourceLineNumber = 47;
        dropdown.SourceFileName = "Pages/ComboBox.ux";
        dropdown.Children.Add(temp15);
        dropdown.Children.Add(temp16);
        dropdown.Children.Add(temp2);
        temp15.Y = 1f;
        temp15.SourceLineNumber = 48;
        temp15.SourceFileName = "Pages/ComboBox.ux";
        temp15.RelativeNode = this;
        temp15.RelativeTo = Fuse.TranslationModes.Size;
        temp16.SourceLineNumber = 49;
        temp16.SourceFileName = "Pages/ComboBox.ux";
        temp16.Children.Add(temp17);
        temp17.SourceLineNumber = 50;
        temp17.SourceFileName = "Pages/ComboBox.ux";
        temp17.Children.Add(temp1);
        temp1.SourceLineNumber = 51;
        temp1.SourceFileName = "Pages/ComboBox.ux";
        temp1.Templates.Add(item);
        temp1.Bindings.Add(temp18);
        temp5.SourceLineNumber = 51;
        temp5.SourceFileName = "Pages/ComboBox.ux";
        temp2.SourceLineNumber = 65;
        temp2.SourceFileName = "Pages/ComboBox.ux";
        temp2.Animators.Add(temp19);
        temp2.Animators.Add(temp20);
        temp2.Animators.Add(temp21);
        temp2.Bindings.Add(temp22);
        temp19.Value = 0f;
        temp19.Duration = 0.2;
        temp19.Easing = Fuse.Animations.Easing.CubicOut;
        temp20.Value = Fuse.Elements.Visibility.Hidden;
        temp20.Delay = 0.2;
        temp21.Y = 300f;
        temp21.Duration = 0.2;
        temp21.Easing = Fuse.Animations.Easing.CubicIn;
        temp6.SourceLineNumber = 65;
        temp6.SourceFileName = "Pages/ComboBox.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(header);
        __g_nametable.Objects.Add(temp_eb18);
        __g_nametable.Objects.Add(dropdown);
        __g_nametable.Properties.Add(this_Options_inst);
        __g_nametable.Properties.Add(this_Selected_inst);
        this.Children.Add(temp7);
        this.Children.Add(temp10);
        this.Children.Add(header);
        this.Children.Add(dropdown);
    }
    static global::Uno.UX.Selector __selector0 = "Options";
    static global::Uno.UX.Selector __selector1 = "Selected";
    static global::Uno.UX.Selector __selector2 = "Value";
    static global::Uno.UX.Selector __selector3 = "Color";
    static global::Uno.UX.Selector __selector4 = "Items";
    static global::Uno.UX.Selector __selector5 = "Opacity";
    static global::Uno.UX.Selector __selector6 = "Visibility";
    static global::Uno.UX.Selector __selector7 = "header";
    static global::Uno.UX.Selector __selector8 = "dropdown";
}
