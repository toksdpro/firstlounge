[Uno.Compiler.UxGenerated]
public partial class CategoryProducts: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly CategoryProducts __parent;
        [Uno.WeakReference] internal readonly CategoryProducts __parentInstance;
        public Template(CategoryProducts parent, CategoryProducts parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::SortedItem();
            __self.SourceLineNumber = 233;
            __self.SourceFileName = "Pages/categoryProducts.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<object> temp1_Items_inst;
    global::Uno.UX.Property<float> categorypage_Opacity_inst;
    global::Uno.UX.Property<float> cloading_Opacity_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<bool> temp3_Value_inst;
    global::Uno.UX.Property<float> cdialog_Opacity_inst;
    global::Uno.UX.Property<float> cmainPanelBlur_Radius_inst;
    global::Uno.UX.Property<bool> temp4_Value_inst;
    global::Uno.UX.Property<string> cdialogMsg_Value_inst;
    global::Uno.UX.Property<bool> temp5_Value_inst;
    internal global::Fuse.Controls.StackPanel categorypage;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb5;
    internal global::Fuse.Effects.Blur cmainPanelBlur;
    internal global::Fuse.Controls.Panel cloading;
    internal global::Fuse.Controls.Panel cdialog;
    internal global::Fuse.Controls.Text cdialogMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb6;
    internal global::Fuse.Reactive.EventBinding temp_eb7;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "categorypage",
        "ScrollbarBox",
        "temp_eb5",
        "cmainPanelBlur",
        "cloading",
        "cdialog",
        "cdialogMsg",
        "temp_eb6",
        "temp_eb7"
    };
    static CategoryProducts()
    {
    }
    [global::Uno.UX.UXConstructor]
    public CategoryProducts(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("title");
        var temp1 = new global::Fuse.Reactive.Each();
        temp1_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp1, __selector1);
        var temp7 = new global::Fuse.Reactive.Data("productsByCategoryList");
        var temp8 = new global::Fuse.Reactive.Data("goBack");
        categorypage = new global::Fuse.Controls.StackPanel();
        categorypage_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(categorypage, __selector2);
        cloading = new global::Fuse.Controls.Panel();
        cloading_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(cloading, __selector2);
        var temp2 = new global::Fuse.Triggers.WhileTrue();
        temp2_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp2, __selector0);
        var temp9 = new global::Fuse.Reactive.Data("visible");
        var temp3 = new global::Fuse.Triggers.WhileFalse();
        temp3_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp3, __selector0);
        var temp10 = new global::Fuse.Reactive.Data("visible");
        cdialog = new global::Fuse.Controls.Panel();
        cdialog_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(cdialog, __selector2);
        cmainPanelBlur = new global::Fuse.Effects.Blur();
        cmainPanelBlur_Radius_inst = new FirstLoungeNewApp_FuseEffectsBlur_Radius_Property(cmainPanelBlur, __selector3);
        var temp4 = new global::Fuse.Triggers.WhileTrue();
        temp4_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp4, __selector0);
        var temp11 = new global::Fuse.Reactive.Data("isDialogShowing");
        cdialogMsg = new global::Fuse.Controls.Text();
        cdialogMsg_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(cdialogMsg, __selector0);
        var temp12 = new global::Fuse.Reactive.Data("dialogMsg");
        var temp13 = new global::Fuse.Reactive.Data("retry");
        var temp14 = new global::Fuse.Reactive.Data("closeDialog");
        var temp5 = new global::Fuse.Triggers.WhileFalse();
        temp5_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp5, __selector0);
        var temp15 = new global::Fuse.Reactive.Data("isDialogShowing");
        var temp16 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp17 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp18 = new global::Fuse.Controls.DockPanel();
        var temp19 = new global::Header();
        var temp20 = new global::Fuse.Controls.StackPanel();
        var temp21 = new global::OnlySearch();
        var temp22 = new global::Fuse.Controls.DockPanel();
        var temp23 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp24 = new global::Fuse.Controls.StackPanel();
        var temp25 = new Template(this, this);
        var temp26 = new global::Fuse.Reactive.DataBinding(temp1_Items_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp27 = new global::Fuse.Controls.Panel();
        var temp28 = new global::SubmitButton1();
        temp_eb5 = new global::Fuse.Reactive.EventBinding(temp8);
        var temp29 = new global::Loader();
        var temp30 = new global::Fuse.Animations.Change<float>(categorypage_Opacity_inst);
        var temp31 = new global::Fuse.Animations.Change<float>(cloading_Opacity_inst);
        var temp32 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp33 = new global::Fuse.Animations.Change<float>(categorypage_Opacity_inst);
        var temp34 = new global::Fuse.Animations.Change<float>(cloading_Opacity_inst);
        var temp35 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp10, Fuse.Reactive.BindingMode.Default);
        var temp36 = new global::Fuse.Animations.Change<float>(cdialog_Opacity_inst);
        var temp37 = new global::Fuse.Animations.Change<float>(cmainPanelBlur_Radius_inst);
        var temp38 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp11, Fuse.Reactive.BindingMode.Default);
        var temp39 = new global::Fuse.Controls.Rectangle();
        var temp40 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp41 = new global::Fuse.Controls.StackPanel();
        var temp42 = new global::Fuse.Reactive.DataBinding(cdialogMsg_Value_inst, temp12, Fuse.Reactive.BindingMode.Default);
        var temp43 = new global::SubmitButton1();
        temp_eb6 = new global::Fuse.Reactive.EventBinding(temp13);
        var temp44 = new global::SubmitButton1();
        temp_eb7 = new global::Fuse.Reactive.EventBinding(temp14);
        var temp45 = new global::Fuse.Animations.Move();
        var temp46 = new global::Fuse.Animations.Scale();
        var temp47 = new global::Fuse.Reactive.DataBinding(temp5_Value_inst, temp15, Fuse.Reactive.BindingMode.Default);
        var temp48 = new global::Fuse.Effects.DropShadow();
        var temp49 = new global::FooterNav();
        var temp50 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/categoryProducts.ux";
        temp16.LineNumber = 4;
        temp16.FileName = "Pages/categoryProducts.ux";
        temp16.SourceLineNumber = 4;
        temp16.SourceFileName = "Pages/categoryProducts.ux";
        temp16.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp17.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar login = require('Pages/login.js');\t\t\n\n\t\tvar api = require('Pages/api.js');\n\n\t\t// var Backend = require(\"../Modules/Backend.js\");\n\n\t\tvar category = Observable();\n\n\t\tvar visible = Observable(true);\n\n\t\tvar isDialogShowing = Observable(false);\n\n\t\tvar dialogMsg = Observable();\n\n\t\tvar productsByCategoryList = Observable();\n\t\t\n\n\t\tvar StaffID = Observable();\n\t\t// var Image = Observable(\"\");\n\t\t// var productsByCategoryList = Observable();\n\n\t\n\n\t\tfunction closeDialog() {\n\t\t\tconsole.log('jhashja');\n\t\t\tisDialogShowing.value = false;\n\t\t}\n\n\t\tfunction retry() {\n\t\t\tconsole.log(\"kjsd\");\n\t\t\tLoadProductsByCategory(category.value.id);\n\t\t}\n\n\n\t\tfunction LoadProductsByCategory(category_id) {\n\n\t\t\t// var arr = [];\n\t\t\t// productsByCategoryList.refreshAll(arr);\n\n\t\t\tlogin.getToken()\n\t\t\t.then(function(contents) {\n\t\t\t    console.log('token: '+contents);\n\t\t\t\tif (contents != null) {\n\t\t\t\t\tGetCategoryProducts(contents);\n\t\t\t\t}else{\n\t\t\t\t\tconsole.log(\"Error not logged in\");\n\t\t\t\t}\n\t\t\t}, function(error) {\n\t\t\t\tconsole.log(error);\n\t\t\t});\n\n\t\t\tfunction GetCategoryProducts(token) {\n\t\t\t\tapi.ApiCategoryProducts(token, category_id)\n\t\t\t\t.then(function(response) {\n\t\t\t\t\tvar ok = response.ok;\n\t\t\t\t\tvar status = response.status;\n\t\t\t\t\tvar response_msg = JSON.parse(response._bodyText);\n\t\t\t\t\t// hide loader\n\t\t\t\t\tvisible.value = false;\n\t\t\t\t\tif (ok) {\n\t\t\t\t\t\tisDialogShowing.value = false;\n\t\t\t\t\t\tconsole.log(\"api: \"+JSON.stringify(JSON.parse(response._bodyText).products));\n\t\t\t\t\t\tproductsByCategoryList.replaceAll(JSON.parse(response._bodyText).products);\n\t\t\t\t\t}else{\n\t\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\t\tif (status == 502) {\n\t\t\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\t\t\tconsole.log(\"Network Error\");\n\t\t\t\t\t\t}else {\n\t\t\t\t\t\t\tdialogMsg.value = \"There was an error\";\n\t\t\t\t\t\t\tconsole.log(status);\t\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t}).catch(function(err) {\n\t\t\t\t\tvisible.value = false;\n\n\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t});\n\t\t\t}\n\t\t\t\n\t\t}\n\n\t\tvar stresObj = this.Parameter.map(function(x) { return x.data; });\t\t\t\n\t\t\t//var data = JSON.parse(stresObj.value);\n\t\t\t//console.log(\"Name\" + data.name);\n\t\t\tstresObj.onValueChanged(module, function(param) {\n\t\t\t// console.log(\"new \"+param);\n\t\t\tvar data = [];\n\t\t\tdata[0] = JSON.parse(param);\n\t\t\t\t// console.log('data: '+JSON.stringify(data));\n\n\t\t\tcategory.replaceAll(data);\n\t\t\t// console.log(param.toLowerCase());\n\t\t\t// if (data[0].toLowerCase() == \"ticket\") {\n\t\t\t// \tImage.value = \"Assets/packages.png\";\n\t\t\t// }else{\n\t\t\t// \tImage.value = \"Assets/\"+data[0].toLowerCase()+\".png\";\n\t\t\t// }\n\n\t\t\tconsole.log(\"categoryID: \"+JSON.stringify(category.value.id));\n\n\t\t\tLoadProductsByCategory(category.value.id);\n\n\t\t\t// console.log(JSON.stringify(Backend.lowerlize(category.value.name)));\n\t\t\t// console.log(JSON.stringify(category));\n\t\t\t// productsByCategoryList = Backend.LoadProductsByCategory(\"ticket\");\n\t\t\t\t\t\t\n\t\t});\n\n\t\t// Backend.productsByCategoryList.onValueChanged(module, function(x) {\n\t\t// \tconsole.log(\"We got a new value: \" + JSON.stringify(x));\n\t\t// })\n\n\t\tfunction InventoryView(args){\n\t\t\t// console.log(JSON.stringify(args.data));\n\t\t\tvar param = JSON.stringify(args.data);\n\t\t\trouter.push(\"inventoryview\", {data: param});\n\t\t}\n\n\t\tmodule.exports = {\n\t\t\tproductsByCategoryList: productsByCategoryList,\n\t\t\ttitle: category.value.name,\n\t\t\t//title: getCategoryTitle(),\n\t\t\tInventoryView: InventoryView,\n\t\t\tdialogMsg: dialogMsg,\n\t\t\tisDialogShowing: isDialogShowing,\n\t\t\tvisible: visible,\n\t\t\tcloseDialog: closeDialog,\n\t\t\tretry: retry\n\t\t}\n\t";
        temp17.LineNumber = 6;
        temp17.FileName = "Pages/categoryProducts.ux";
        temp17.SourceLineNumber = 6;
        temp17.SourceFileName = "Pages/categoryProducts.ux";
        temp18.SourceLineNumber = 213;
        temp18.SourceFileName = "Pages/categoryProducts.ux";
        temp18.Children.Add(temp19);
        temp18.Children.Add(temp20);
        temp18.Children.Add(categorypage);
        temp18.Children.Add(cloading);
        temp18.Children.Add(temp4);
        temp18.Children.Add(cdialog);
        temp18.Children.Add(temp49);
        temp19.SourceLineNumber = 214;
        temp19.SourceFileName = "Pages/categoryProducts.ux";
        temp20.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp20.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp20.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp20.SourceLineNumber = 217;
        temp20.SourceFileName = "Pages/categoryProducts.ux";
        temp20.Children.Add(temp21);
        temp20.Children.Add(temp22);
        temp21.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp21.SourceLineNumber = 219;
        temp21.SourceFileName = "Pages/categoryProducts.ux";
        temp22.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp22.Margin = float4(0f, 0f, 0f, 10f);
        temp22.SourceLineNumber = 221;
        temp22.SourceFileName = "Pages/categoryProducts.ux";
        temp22.Children.Add(temp);
        temp.SourceLineNumber = 222;
        temp.SourceFileName = "Pages/categoryProducts.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp23);
        temp6.SourceLineNumber = 222;
        temp6.SourceFileName = "Pages/categoryProducts.ux";
        categorypage.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        categorypage.Alignment = Fuse.Elements.Alignment.Center;
        categorypage.Name = __selector4;
        categorypage.SourceLineNumber = 227;
        categorypage.SourceFileName = "Pages/categoryProducts.ux";
        categorypage.Children.Add(ScrollbarBox);
        categorypage.Children.Add(temp27);
        categorypage.Children.Add(cmainPanelBlur);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(450f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector5;
        ScrollbarBox.SourceLineNumber = 228;
        ScrollbarBox.SourceFileName = "Pages/categoryProducts.ux";
        ScrollbarBox.Children.Add(temp24);
        temp24.Margin = float4(0f, 60f, 0f, 0f);
        temp24.SourceLineNumber = 230;
        temp24.SourceFileName = "Pages/categoryProducts.ux";
        temp24.Children.Add(temp1);
        temp1.SourceLineNumber = 232;
        temp1.SourceFileName = "Pages/categoryProducts.ux";
        temp1.Templates.Add(temp25);
        temp1.Bindings.Add(temp26);
        temp7.SourceLineNumber = 232;
        temp7.SourceFileName = "Pages/categoryProducts.ux";
        temp27.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp27.Margin = float4(0f, 10f, 0f, 0f);
        temp27.Padding = float4(5f, 5f, 5f, 5f);
        temp27.SourceLineNumber = 240;
        temp27.SourceFileName = "Pages/categoryProducts.ux";
        temp27.Children.Add(temp28);
        temp28.Text = "Back";
        temp28.Alignment = Fuse.Elements.Alignment.Center;
        temp28.SourceLineNumber = 241;
        temp28.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp28, temp_eb5.OnEvent);
        temp28.Bindings.Add(temp_eb5);
        temp8.SourceLineNumber = 241;
        temp8.SourceFileName = "Pages/categoryProducts.ux";
        cmainPanelBlur.Radius = 0f;
        cmainPanelBlur.Name = __selector6;
        cmainPanelBlur.SourceLineNumber = 244;
        cmainPanelBlur.SourceFileName = "Pages/categoryProducts.ux";
        cloading.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        cloading.Height = new Uno.UX.Size(65f, Uno.UX.Unit.Percent);
        cloading.Opacity = 1f;
        cloading.Name = __selector7;
        cloading.SourceLineNumber = 248;
        cloading.SourceFileName = "Pages/categoryProducts.ux";
        cloading.Children.Add(temp29);
        cloading.Children.Add(temp2);
        cloading.Children.Add(temp3);
        temp29.Lcolor = float4(0f, 0f, 0f, 1f);
        temp29.SourceLineNumber = 249;
        temp29.SourceFileName = "Pages/categoryProducts.ux";
        temp2.SourceLineNumber = 251;
        temp2.SourceFileName = "Pages/categoryProducts.ux";
        temp2.Animators.Add(temp30);
        temp2.Animators.Add(temp31);
        temp2.Bindings.Add(temp32);
        temp30.Value = 0f;
        temp31.Value = 1f;
        temp9.SourceLineNumber = 251;
        temp9.SourceFileName = "Pages/categoryProducts.ux";
        temp3.SourceLineNumber = 256;
        temp3.SourceFileName = "Pages/categoryProducts.ux";
        temp3.Animators.Add(temp33);
        temp3.Animators.Add(temp34);
        temp3.Bindings.Add(temp35);
        temp33.Value = 1f;
        temp34.Value = 0f;
        temp10.SourceLineNumber = 256;
        temp10.SourceFileName = "Pages/categoryProducts.ux";
        temp4.SourceLineNumber = 262;
        temp4.SourceFileName = "Pages/categoryProducts.ux";
        temp4.Animators.Add(temp36);
        temp4.Animators.Add(temp37);
        temp4.Bindings.Add(temp38);
        temp36.Value = 1f;
        temp36.Duration = 0.3;
        temp36.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp37.Value = 10f;
        temp37.Duration = 0.3;
        temp37.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp11.SourceLineNumber = 262;
        temp11.SourceFileName = "Pages/categoryProducts.ux";
        cdialog.Width = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        cdialog.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        cdialog.Padding = float4(20f, 40f, 20f, 0f);
        cdialog.Opacity = 0f;
        cdialog.ZOffset = 1f;
        cdialog.Name = __selector8;
        cdialog.SourceLineNumber = 267;
        cdialog.SourceFileName = "Pages/categoryProducts.ux";
        cdialog.Children.Add(temp39);
        cdialog.Children.Add(temp41);
        cdialog.Children.Add(temp5);
        cdialog.Children.Add(temp48);
        temp39.CornerRadius = float4(2f, 2f, 2f, 2f);
        temp39.Opacity = 0.8f;
        temp39.Layer = Fuse.Layer.Background;
        temp39.SourceLineNumber = 268;
        temp39.SourceFileName = "Pages/categoryProducts.ux";
        temp39.Fill = temp40;
        temp41.SourceLineNumber = 269;
        temp41.SourceFileName = "Pages/categoryProducts.ux";
        temp41.Children.Add(cdialogMsg);
        temp41.Children.Add(temp43);
        temp41.Children.Add(temp44);
        cdialogMsg.TextAlignment = Fuse.Controls.TextAlignment.Center;
        cdialogMsg.Alignment = Fuse.Elements.Alignment.Center;
        cdialogMsg.Name = __selector9;
        cdialogMsg.SourceLineNumber = 271;
        cdialogMsg.SourceFileName = "Pages/categoryProducts.ux";
        cdialogMsg.Font = global::LoginPage.Poppins;
        cdialogMsg.Bindings.Add(temp42);
        temp12.SourceLineNumber = 271;
        temp12.SourceFileName = "Pages/categoryProducts.ux";
        temp43.Text = "Retry";
        temp43.Alignment = Fuse.Elements.Alignment.Center;
        temp43.Margin = float4(0f, 10f, 0f, 0f);
        temp43.SourceLineNumber = 272;
        temp43.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp43, temp_eb6.OnEvent);
        temp43.Bindings.Add(temp_eb6);
        temp13.SourceLineNumber = 272;
        temp13.SourceFileName = "Pages/categoryProducts.ux";
        temp44.Text = "close";
        temp44.Alignment = Fuse.Elements.Alignment.Center;
        temp44.Margin = float4(0f, 10f, 0f, 30f);
        temp44.SourceLineNumber = 273;
        temp44.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp44, temp_eb7.OnEvent);
        temp44.Bindings.Add(temp_eb7);
        temp14.SourceLineNumber = 273;
        temp14.SourceFileName = "Pages/categoryProducts.ux";
        temp5.SourceLineNumber = 276;
        temp5.SourceFileName = "Pages/categoryProducts.ux";
        temp5.Animators.Add(temp45);
        temp5.Animators.Add(temp46);
        temp5.Bindings.Add(temp47);
        temp45.X = 3f;
        temp45.Duration = 0.6;
        temp45.RelativeTo = Fuse.TranslationModes.Size;
        temp45.Easing = Fuse.Animations.Easing.BackIn;
        temp46.Factor = 0.8f;
        temp46.Duration = 0.6;
        temp46.Easing = Fuse.Animations.Easing.BackIn;
        temp15.SourceLineNumber = 276;
        temp15.SourceFileName = "Pages/categoryProducts.ux";
        temp48.SourceLineNumber = 281;
        temp48.SourceFileName = "Pages/categoryProducts.ux";
        temp49.SourceLineNumber = 286;
        temp49.SourceFileName = "Pages/categoryProducts.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(categorypage);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb5);
        __g_nametable.Objects.Add(cmainPanelBlur);
        __g_nametable.Objects.Add(cloading);
        __g_nametable.Objects.Add(cdialog);
        __g_nametable.Objects.Add(cdialogMsg);
        __g_nametable.Objects.Add(temp_eb6);
        __g_nametable.Objects.Add(temp_eb7);
        this.Background = temp50;
        this.Children.Add(temp16);
        this.Children.Add(temp17);
        this.Children.Add(temp18);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "Opacity";
    static global::Uno.UX.Selector __selector3 = "Radius";
    static global::Uno.UX.Selector __selector4 = "categorypage";
    static global::Uno.UX.Selector __selector5 = "ScrollbarBox";
    static global::Uno.UX.Selector __selector6 = "cmainPanelBlur";
    static global::Uno.UX.Selector __selector7 = "cloading";
    static global::Uno.UX.Selector __selector8 = "cdialog";
    static global::Uno.UX.Selector __selector9 = "cdialogMsg";
}
