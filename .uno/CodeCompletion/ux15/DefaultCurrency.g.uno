[Uno.Compiler.UxGenerated]
public partial class DefaultCurrency: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb20;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "ScrollbarBox",
        "temp_eb20"
    };
    static DefaultCurrency()
    {
    }
    [global::Uno.UX.UXConstructor]
    public DefaultCurrency(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Data("goBack");
        var temp1 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp2 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Controls.DockPanel();
        var temp5 = new global::Header();
        var temp6 = new global::Fuse.Controls.StackPanel();
        var temp7 = new global::OnlySearch();
        var temp8 = new global::Fuse.Controls.DockPanel();
        var temp9 = new global::Fuse.Controls.Text();
        var temp10 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp11 = new global::Fuse.Controls.StackPanel();
        var temp12 = new global::Fuse.Controls.Panel();
        var temp13 = new global::SubmitButton1();
        temp_eb20 = new global::Fuse.Reactive.EventBinding(temp);
        var temp14 = new global::FooterNav();
        var temp15 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/defaultcurrency.ux";
        temp1.LineNumber = 2;
        temp1.FileName = "Pages/defaultcurrency.ux";
        temp1.SourceLineNumber = 2;
        temp1.SourceFileName = "Pages/defaultcurrency.ux";
        temp1.File = new global::Uno.UX.BundleFileSource(import("../../../fdb-all.js"));
        temp2.LineNumber = 3;
        temp2.FileName = "Pages/defaultcurrency.ux";
        temp2.SourceLineNumber = 3;
        temp2.SourceFileName = "Pages/defaultcurrency.ux";
        temp2.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp3.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\t\n\t\tmodule.exports = {\n\t\t\t\n\t\t};\n\t";
        temp3.LineNumber = 4;
        temp3.FileName = "Pages/defaultcurrency.ux";
        temp3.SourceLineNumber = 4;
        temp3.SourceFileName = "Pages/defaultcurrency.ux";
        temp4.SourceLineNumber = 14;
        temp4.SourceFileName = "Pages/defaultcurrency.ux";
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp6);
        temp4.Children.Add(temp10);
        temp4.Children.Add(temp14);
        temp5.SourceLineNumber = 15;
        temp5.SourceFileName = "Pages/defaultcurrency.ux";
        temp6.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp6.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp6.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp6.SourceLineNumber = 18;
        temp6.SourceFileName = "Pages/defaultcurrency.ux";
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp7.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp7.SourceLineNumber = 20;
        temp7.SourceFileName = "Pages/defaultcurrency.ux";
        temp8.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp8.Margin = float4(0f, 0f, 0f, 10f);
        temp8.SourceLineNumber = 22;
        temp8.SourceFileName = "Pages/defaultcurrency.ux";
        temp8.Children.Add(temp9);
        temp9.Value = "Currency";
        temp9.SourceLineNumber = 23;
        temp9.SourceFileName = "Pages/defaultcurrency.ux";
        temp9.Font = global::LoginPage.Poppins;
        temp10.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp10.Alignment = Fuse.Elements.Alignment.Center;
        temp10.SourceLineNumber = 28;
        temp10.SourceFileName = "Pages/defaultcurrency.ux";
        temp10.Children.Add(ScrollbarBox);
        temp10.Children.Add(temp12);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(350f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector0;
        ScrollbarBox.SourceLineNumber = 29;
        ScrollbarBox.SourceFileName = "Pages/defaultcurrency.ux";
        ScrollbarBox.Children.Add(temp11);
        temp11.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp11.Margin = float4(0f, 70f, 0f, 0f);
        temp11.SourceLineNumber = 31;
        temp11.SourceFileName = "Pages/defaultcurrency.ux";
        temp12.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp12.Margin = float4(0f, 10f, 0f, 0f);
        temp12.Padding = float4(5f, 5f, 5f, 5f);
        temp12.SourceLineNumber = 39;
        temp12.SourceFileName = "Pages/defaultcurrency.ux";
        temp12.Children.Add(temp13);
        temp13.Text = "Back";
        temp13.Alignment = Fuse.Elements.Alignment.Center;
        temp13.SourceLineNumber = 40;
        temp13.SourceFileName = "Pages/defaultcurrency.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp13, temp_eb20.OnEvent);
        temp13.Bindings.Add(temp_eb20);
        temp.SourceLineNumber = 40;
        temp.SourceFileName = "Pages/defaultcurrency.ux";
        temp14.SourceLineNumber = 47;
        temp14.SourceFileName = "Pages/defaultcurrency.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb20);
        this.Background = temp15;
        this.Children.Add(temp1);
        this.Children.Add(temp2);
        this.Children.Add(temp3);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "ScrollbarBox";
}
