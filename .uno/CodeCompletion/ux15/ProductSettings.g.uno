[Uno.Compiler.UxGenerated]
public partial class ProductSettings: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ProductSettings __parent;
        [Uno.WeakReference] internal readonly ProductSettings __parentInstance;
        public Template(ProductSettings parent, ProductSettings parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Image_inst;
        global::Uno.UX.Property<string> temp_name_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb58;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp1 = "http://restaurants.saddleng.com/storage/";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            var temp3 = new global::Fuse.Reactive.Data("image_url");
            var temp = new global::HomeIcons();
            temp_Image_inst = new FirstLoungeNewApp_HomeIcons_Image_Property(temp, __selector0);
            var temp4 = new global::Fuse.Reactive.Add(temp2, temp3);
            temp_name_inst = new FirstLoungeNewApp_HomeIcons_name_Property(temp, __selector1);
            var temp5 = new global::Fuse.Reactive.Data("name");
            var temp6 = new global::Fuse.Reactive.Data("categoryProductview");
            var temp7 = new global::Fuse.Reactive.DataBinding(temp_Image_inst, temp4, Fuse.Reactive.BindingMode.Default);
            var temp8 = new global::Fuse.Reactive.DataBinding(temp_name_inst, temp5, Fuse.Reactive.BindingMode.Default);
            var temp9 = new global::Fuse.Controls.Rectangle();
            var temp10 = new global::Fuse.Drawing.Stroke();
            var temp11 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
            temp_eb58 = new global::Fuse.Reactive.EventBinding(temp6);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
            __self.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            __self.Alignment = Fuse.Elements.Alignment.TopCenter;
            __self.Margin = float4(5f, 5f, 5f, 5f);
            __self.Padding = float4(10f, 10f, 10f, 10f);
            __self.SourceLineNumber = 194;
            __self.SourceFileName = "Pages/productsettings.ux";
            global::Fuse.Gestures.Clicked.AddHandler(__self, temp_eb58.OnEvent);
            temp.SourceLineNumber = 196;
            temp.SourceFileName = "Pages/productsettings.ux";
            temp.Bindings.Add(temp7);
            temp.Bindings.Add(temp8);
            temp4.SourceLineNumber = 196;
            temp4.SourceFileName = "Pages/productsettings.ux";
            temp2.SourceLineNumber = 196;
            temp2.SourceFileName = "Pages/productsettings.ux";
            temp3.SourceLineNumber = 196;
            temp3.SourceFileName = "Pages/productsettings.ux";
            temp5.SourceLineNumber = 196;
            temp5.SourceFileName = "Pages/productsettings.ux";
            temp9.CornerRadius = float4(0f, 0f, 0f, 0f);
            temp9.Color = float4(1f, 1f, 1f, 1f);
            temp9.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp9.Layer = Fuse.Layer.Background;
            temp9.SourceLineNumber = 200;
            temp9.SourceFileName = "Pages/productsettings.ux";
            temp9.Strokes.Add(temp10);
            temp10.Color = float4(0f, 1f, 0f, 1f);
            temp10.Width = 1f;
            temp10.Brush = temp11;
            temp6.SourceLineNumber = 194;
            temp6.SourceFileName = "Pages/productsettings.ux";
            __self.Children.Add(temp);
            __self.Children.Add(temp9);
            __self.Bindings.Add(temp_eb58);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Image";
        static global::Uno.UX.Selector __selector1 = "name";
    }
    global::Uno.UX.Property<string> text_Value_inst;
    global::Uno.UX.Property<object> temp_Items_inst;
    internal global::Fuse.Drawing.ImageFill buttonBg;
    internal global::Fuse.Reactive.EventBinding temp_eb57;
    internal global::Fuse.Controls.TextInput text;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb59;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "buttonBg",
        "temp_eb57",
        "text",
        "ScrollbarBox",
        "temp_eb59"
    };
    static ProductSettings()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ProductSettings(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp1 = new global::Fuse.Reactive.Data("SearchPage");
        text = new global::Fuse.Controls.TextInput();
        text_Value_inst = new FirstLoungeNewApp_FuseControlsTextInputControl_Value_Property(text, __selector0);
        var temp2 = new global::Fuse.Reactive.Data("searchString");
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp, __selector1);
        var temp3 = new global::Fuse.Reactive.Data("filteredItems");
        var temp4 = new global::Fuse.Reactive.Data("goBack");
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp7 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp8 = new global::Fuse.Controls.DockPanel();
        var temp9 = new global::Header();
        var temp10 = new global::Fuse.Controls.StackPanel();
        var temp11 = new global::Fuse.Controls.DockPanel();
        var temp12 = new global::Fuse.Controls.Panel();
        var temp13 = new global::Fuse.Controls.Button();
        buttonBg = new global::Fuse.Drawing.ImageFill();
        var temp14 = new global::Fuse.Gestures.Tapped();
        var temp15 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb57 = new global::Fuse.Reactive.EventBinding(temp1);
        var temp16 = new global::Fuse.Reactive.DataBinding(text_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp17 = new global::Fuse.Controls.Rectangle();
        var temp18 = new global::Fuse.Drawing.Stroke();
        var temp19 = new global::Fuse.Drawing.StaticSolidColor(float4(0.5568628f, 0.5882353f, 0.6470588f, 1f));
        var temp20 = new global::Fuse.Controls.DockPanel();
        var temp21 = new global::Fuse.Controls.Text();
        var temp22 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp23 = new global::Fuse.Controls.StackPanel();
        var temp24 = new global::Fuse.Controls.Grid();
        var temp25 = new Template(this, this);
        var temp26 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp27 = new global::Fuse.Controls.Panel();
        var temp28 = new global::SubmitButton1();
        temp_eb59 = new global::Fuse.Reactive.EventBinding(temp4);
        var temp29 = new global::FooterNav();
        var temp30 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/productsettings.ux";
        temp5.LineNumber = 3;
        temp5.FileName = "Pages/productsettings.ux";
        temp5.SourceLineNumber = 3;
        temp5.SourceFileName = "Pages/productsettings.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../../fdb-all.js"));
        temp6.LineNumber = 4;
        temp6.FileName = "Pages/productsettings.ux";
        temp6.SourceLineNumber = 4;
        temp6.SourceFileName = "Pages/productsettings.ux";
        temp6.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp7.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar Backend = require(\"Modules/Backend.js\");\n\t\tvar FileSystem = require(\"FuseJS/FileSystem\");\n\n\t\tvar login = require('Pages/login.js');\n\n\t\tvar api = require('Pages/api.js');\n\t\tvar Backend = require('Modules/Backend.js');\n\t\tvar categoryName = Observable();\n\t\tvar error = Observable();\n\t\tvar categoryList = Observable();\n\t\tvar visible = Observable(true);\n\n\t\tvar isDialogShowing = Observable(false);\n\n\t\tvar dialogMsg = Observable();\t\n\n\t\tsearchString = Observable(\"\");\n\n// ignore case so just convert both to lowercase before comparison\nfunction stringContainsString(main, filter){\n\treturn main === '' || main.toLowerCase().indexOf(filter.toLowerCase()) !== -1;\n}\n\nvar filteredItems = searchString.flatMap(function(searchValue) {\n    return categoryList.where(function(item) { return stringContainsString(item.name, searchValue); });\n});\n\n\nfunction categoryProductview(args){\n\t//console.log(JSON.stringify(args.data));\t\n\t//saveCategoryTitle(args.data.name);\n\t\n\tvar param = JSON.stringify(args.data);\n\t// console.log(\"param: \"+param);\n\n\trouter.push(\"productcreate\", {data: param});\n}\n\n\t\t\n\t\tfunction LoadCategory() {\n\t// reset error status\n\t// apistatus.clear();\n\tconsole.log(\"I called here\");\n\tlogin.getToken()\n\t.then(function(contents) {\n\t    console.log('token: '+contents);\n\t\tif (contents != null) {\n\t\t\t// token.value = contents;\n\t\t\tGetCategories(contents);\n\t\t}else{\n\t\t\tconsole.log(\"Error not logged in\");\n\t\t}\n\t}, function(error) {\n\t\tconsole.log(error);\n\t});\n\n\tfunction GetCategories(token) {\n\t\tapi.ApiCategory(token)\n\t\t.then(function(response) {\n\t\t\t// console.log('tokenD: '+token);\n\t\t\tvar ok = response.ok;\n\t\t\tvar status = response.status;\n\t\t\tconsole.log('status: '+status);\n\t\t\tvar response_msg = JSON.parse(response._bodyText);\n\n\t\t\t// hide loader\n\t\t\tvisible.value = false;\n\n\t\t\tif (ok) {\n\t\t\t\t// console.log(\"error\");\n\t\t\t\tisDialogShowing.value = false;\n\t\t\t\tconsole.log(\"api: \"+JSON.stringify(JSON.parse(response._bodyText).categories));\n\t\t\t\t// var arr = [];\n\t\t\t\t// arr.push(JSON.parse(response._bodyText).categories);\n\t\t\t\tcategoryList.replaceAll(JSON.parse(response._bodyText).categories);\n\t\t\t\t// apistatus.value = status;\n\t\t\t}else{\n\n\t\t\t\tisDialogShowing.value = true;\n\t\t\t\tconsole.log(\"isDialogShowing.value: \"+isDialogShowing.value);\n\n\t\t\t\tif (status == 502) {\n\t\t\t\t\tconsole.log(\"Network Error\");\n\t\t\t\t\t// error.value = \"Network error\";\n\t\t\t\t\t\n\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\n\t\t\t\t}else {\n\t\t\t\t\tconsole.log(status);\t\n\t\t\t\t\t// error.value = \"There was an error\";\n\t\t\t\t\tdialogMsg.value = \"There was an error\";\n\t\t\t\t}\n\t\t\t\t\n\t\t\t}\n\t\t\t// apistatus.value = status;\n\n\t\t}).catch(function(err) {\n\t\t\t// console.log('here');\n\t\t\t// apistatus.value = 401;\n\n\t\t\tvisible.value = false;\n\n\t\t\tisDialogShowing.value = true;\n\t\t\tdialogMsg.value = \"Network Error\";\n\n\t\t\t// dialogMsg.value = \"There was an error\";\n\n\t\t\t// console.log('here: '+error.value);\n\n\t\t\tconsole.log(\"There was an error\");\n\t\t});\n\t}\n\n\t\n\t// return categoryList;\n}\n\n\n\nLoadCategory();\n\n\t\t\n\t\tmodule.exports = {\n\t\t\t\n\t\t\tcategoryName: categoryName,\n\t\t\thomepageList: categoryList,\n\t\t\tdialogMsg: dialogMsg,\n\t\t\tfilteredItems: filteredItems, \n\t\t\tsearchString: searchString,\n\t\t\tcategoryProductview: categoryProductview,\n\t\t\tisDialogShowing: isDialogShowing,\n\n\t\t\tvisible: visible\n\t\t};\n\t";
        temp7.LineNumber = 5;
        temp7.FileName = "Pages/productsettings.ux";
        temp7.SourceLineNumber = 5;
        temp7.SourceFileName = "Pages/productsettings.ux";
        temp8.SourceLineNumber = 144;
        temp8.SourceFileName = "Pages/productsettings.ux";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp8.Children.Add(temp22);
        temp8.Children.Add(temp29);
        temp9.SourceLineNumber = 145;
        temp9.SourceFileName = "Pages/productsettings.ux";
        temp10.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp10.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp10.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp10.SourceLineNumber = 148;
        temp10.SourceFileName = "Pages/productsettings.ux";
        temp10.Children.Add(temp11);
        temp10.Children.Add(temp20);
        temp11.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp11.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp11.Margin = float4(0f, 5f, 0f, 5f);
        temp11.SourceLineNumber = 150;
        temp11.SourceFileName = "Pages/productsettings.ux";
        temp11.Children.Add(temp12);
        temp12.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp12.Margin = float4(5f, 5f, 5f, 5f);
        temp12.SourceLineNumber = 151;
        temp12.SourceFileName = "Pages/productsettings.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp12, Fuse.Layouts.Dock.Left);
        temp12.Children.Add(temp13);
        temp12.Children.Add(text);
        temp12.Children.Add(temp17);
        temp13.Color = float4(0f, 0f, 0f, 1f);
        temp13.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp13.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp13.Alignment = Fuse.Elements.Alignment.Right;
        temp13.Padding = float4(15f, 15f, 15f, 15f);
        temp13.SourceLineNumber = 152;
        temp13.SourceFileName = "Pages/productsettings.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Right);
        temp13.Background = buttonBg;
        temp13.Children.Add(temp14);
        buttonBg.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/search-icon-2.png"));
        temp14.SourceLineNumber = 154;
        temp14.SourceFileName = "Pages/productsettings.ux";
        temp14.Actions.Add(temp15);
        temp14.Bindings.Add(temp_eb57);
        temp15.SourceLineNumber = 155;
        temp15.SourceFileName = "Pages/productsettings.ux";
        temp15.Handler += temp_eb57.OnEvent;
        temp1.SourceLineNumber = 155;
        temp1.SourceFileName = "Pages/productsettings.ux";
        text.PlaceholderText = "Search by Name";
        text.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        text.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        text.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        text.Alignment = Fuse.Elements.Alignment.Left;
        text.Padding = float4(15f, 15f, 15f, 15f);
        text.Name = __selector2;
        text.SourceLineNumber = 159;
        text.SourceFileName = "Pages/productsettings.ux";
        global::Fuse.Controls.DockPanel.SetDock(text, Fuse.Layouts.Dock.Left);
        text.Bindings.Add(temp16);
        temp2.SourceLineNumber = 159;
        temp2.SourceFileName = "Pages/productsettings.ux";
        temp17.CornerRadius = float4(15f, 15f, 15f, 15f);
        temp17.Color = float4(1f, 1f, 1f, 1f);
        temp17.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp17.Layer = Fuse.Layer.Background;
        temp17.SourceLineNumber = 161;
        temp17.SourceFileName = "Pages/productsettings.ux";
        temp17.Strokes.Add(temp18);
        temp18.Color = float4(0f, 1f, 0f, 1f);
        temp18.Width = 1f;
        temp18.Brush = temp19;
        temp20.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp20.Margin = float4(0f, 0f, 0f, 10f);
        temp20.SourceLineNumber = 176;
        temp20.SourceFileName = "Pages/productsettings.ux";
        temp20.Children.Add(temp21);
        temp21.Value = "Select a Category";
        temp21.SourceLineNumber = 177;
        temp21.SourceFileName = "Pages/productsettings.ux";
        temp21.Font = global::LoginPage.Poppins;
        temp22.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp22.Alignment = Fuse.Elements.Alignment.Center;
        temp22.SourceLineNumber = 182;
        temp22.SourceFileName = "Pages/productsettings.ux";
        temp22.Children.Add(ScrollbarBox);
        temp22.Children.Add(temp27);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(450f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector3;
        ScrollbarBox.SourceLineNumber = 183;
        ScrollbarBox.SourceFileName = "Pages/productsettings.ux";
        ScrollbarBox.Children.Add(temp23);
        temp23.Color = float4(1f, 1f, 1f, 1f);
        temp23.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp23.Margin = float4(0f, 70f, 0f, 0f);
        temp23.SourceLineNumber = 185;
        temp23.SourceFileName = "Pages/productsettings.ux";
        temp23.Children.Add(temp24);
        temp24.ColumnCount = 2;
        temp24.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp24.SourceLineNumber = 188;
        temp24.SourceFileName = "Pages/productsettings.ux";
        temp24.Children.Add(temp);
        temp.SourceLineNumber = 191;
        temp.SourceFileName = "Pages/productsettings.ux";
        temp.Templates.Add(temp25);
        temp.Bindings.Add(temp26);
        temp3.SourceLineNumber = 191;
        temp3.SourceFileName = "Pages/productsettings.ux";
        temp27.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp27.Margin = float4(0f, 10f, 0f, 0f);
        temp27.Padding = float4(5f, 5f, 5f, 5f);
        temp27.SourceLineNumber = 220;
        temp27.SourceFileName = "Pages/productsettings.ux";
        temp27.Children.Add(temp28);
        temp28.Text = "Back";
        temp28.Alignment = Fuse.Elements.Alignment.Center;
        temp28.SourceLineNumber = 221;
        temp28.SourceFileName = "Pages/productsettings.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp28, temp_eb59.OnEvent);
        temp28.Bindings.Add(temp_eb59);
        temp4.SourceLineNumber = 221;
        temp4.SourceFileName = "Pages/productsettings.ux";
        temp29.SourceLineNumber = 228;
        temp29.SourceFileName = "Pages/productsettings.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(buttonBg);
        __g_nametable.Objects.Add(temp_eb57);
        __g_nametable.Objects.Add(text);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb59);
        this.Background = temp30;
        this.Children.Add(temp5);
        this.Children.Add(temp6);
        this.Children.Add(temp7);
        this.Children.Add(temp8);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Items";
    static global::Uno.UX.Selector __selector2 = "text";
    static global::Uno.UX.Selector __selector3 = "ScrollbarBox";
}
