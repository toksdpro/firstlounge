[Uno.Compiler.UxGenerated]
public partial class Field: Fuse.Controls.TextInput
{
    string _field_FAcode;
    [global::Uno.UX.UXOriginSetter("SetFAcode")]
    public string FAcode
    {
        get { return _field_FAcode; }
        set { SetFAcode(value, null); }
    }
    public void SetFAcode(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_FAcode)
        {
            _field_FAcode = value;
            OnPropertyChanged("FAcode", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    static Field()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Field()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, FirstLoungeNewApp_accessor_Field_FAcode.Singleton);
        var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Read);
        var temp4 = new global::Fuse.Controls.Rectangle();
        var temp5 = new global::Fuse.Drawing.Stroke();
        this.PlaceholderColor = float4(0.2f, 0.2f, 0.2f, 1f);
        this.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        this.Height = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(0f, 5f, 0f, 5f);
        this.Padding = float4(10f, 10f, 10f, 10f);
        this.SourceLineNumber = 164;
        this.SourceFileName = "Pages/login.ux";
        temp.Alignment = Fuse.Elements.Alignment.CenterRight;
        temp.SourceLineNumber = 166;
        temp.SourceFileName = "Pages/login.ux";
        temp.Font = global::LoginPage.FontAwesome;
        temp.Bindings.Add(temp3);
        temp2.SourceLineNumber = 166;
        temp2.SourceFileName = "Pages/login.ux";
        temp1.SourceLineNumber = 166;
        temp1.SourceFileName = "Pages/login.ux";
        temp4.CornerRadius = float4(5f, 5f, 5f, 5f);
        temp4.Color = float4(1f, 1f, 1f, 1f);
        temp4.Layer = Fuse.Layer.Background;
        temp4.SourceLineNumber = 167;
        temp4.SourceFileName = "Pages/login.ux";
        temp4.Strokes.Add(temp5);
        temp5.Color = float4(0.4666667f, 0.4666667f, 0.4666667f, 1f);
        temp5.Width = 1f;
        this.Font = global::LoginPage.Poppins;
        this.Children.Add(temp);
        this.Children.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
