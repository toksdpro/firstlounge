[Uno.Compiler.UxGenerated]
public partial class InventoryView: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> __self_Url_inst;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Image();
            var temp = "http://restaurants.saddleng.com/storage/";
            var temp1 = new global::Fuse.Reactive.Constant(temp);
            var temp2 = new global::Fuse.Reactive.Data("product_image");
            __self_Url_inst = new FirstLoungeNewApp_FuseControlsImage_Url_Property(__self, __selector0);
            var temp3 = new global::Fuse.Reactive.Add(temp1, temp2);
            var temp4 = new global::Fuse.Reactive.DataBinding(__self_Url_inst, temp3, Fuse.Reactive.BindingMode.Default);
            __self.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Percent);
            __self.Alignment = Fuse.Elements.Alignment.TopCenter;
            __self.Margin = float4(0f, 5f, 0f, 5f);
            __self.SourceLineNumber = 84;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp3.SourceLineNumber = 84;
            temp3.SourceFileName = "Pages/inventoryview.ux";
            temp1.SourceLineNumber = 84;
            temp1.SourceFileName = "Pages/inventoryview.ux";
            temp2.SourceLineNumber = 84;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            __self.Bindings.Add(temp4);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Url";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template1: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template1(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        static Template1()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("name");
            var temp2 = new global::Fuse.Controls.Text();
            var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
            __self.Alignment = Fuse.Elements.Alignment.Center;
            __self.SourceLineNumber = 85;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp2.Value = "Name:";
            temp2.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp2.Margin = float4(5f, 0f, 5f, 0f);
            temp2.SourceLineNumber = 86;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Left);
            temp2.Font = global::LoginPage.Poppins;
            temp.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp.Margin = float4(5f, 0f, 5f, 0f);
            temp.SourceLineNumber = 87;
            temp.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
            temp.Font = global::LoginPage.Poppins;
            temp.Bindings.Add(temp3);
            temp1.SourceLineNumber = 87;
            temp1.SourceFileName = "Pages/inventoryview.ux";
            __self.Children.Add(temp2);
            __self.Children.Add(temp);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template2: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template2(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        static Template2()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp1 = "₦";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            var temp3 = new global::Fuse.Reactive.Data("price");
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp4 = new global::Fuse.Reactive.Add(temp2, temp3);
            var temp5 = new global::Fuse.Controls.Text();
            var temp6 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
            __self.Alignment = Fuse.Elements.Alignment.Center;
            __self.SourceLineNumber = 89;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp5.Value = "Unit Price:";
            temp5.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp5.Margin = float4(5f, 0f, 5f, 0f);
            temp5.SourceLineNumber = 90;
            temp5.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp5, Fuse.Layouts.Dock.Left);
            temp5.Font = global::LoginPage.Poppins;
            temp.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp.Margin = float4(5f, 0f, 5f, 0f);
            temp.SourceLineNumber = 91;
            temp.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
            temp.Font = global::LoginPage.Poppins;
            temp.Bindings.Add(temp6);
            temp4.SourceLineNumber = 91;
            temp4.SourceFileName = "Pages/inventoryview.ux";
            temp2.SourceLineNumber = 91;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            temp3.SourceLineNumber = 91;
            temp3.SourceFileName = "Pages/inventoryview.ux";
            __self.Children.Add(temp5);
            __self.Children.Add(temp);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template3: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template3(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        static Template3()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("quantity");
            var temp2 = new global::Fuse.Controls.Text();
            var temp3 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
            __self.Alignment = Fuse.Elements.Alignment.Center;
            __self.SourceLineNumber = 93;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp2.Value = "Qty:";
            temp2.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp2.Margin = float4(5f, 0f, 5f, 0f);
            temp2.SourceLineNumber = 94;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Left);
            temp2.Font = global::LoginPage.Poppins;
            temp.Alignment = Fuse.Elements.Alignment.TopCenter;
            temp.Margin = float4(5f, 0f, 5f, 0f);
            temp.SourceLineNumber = 95;
            temp.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
            temp.Font = global::LoginPage.Poppins;
            temp.Bindings.Add(temp3);
            temp1.SourceLineNumber = 95;
            temp1.SourceFileName = "Pages/inventoryview.ux";
            __self.Children.Add(temp2);
            __self.Children.Add(temp);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template4: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template4(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> qtext_Value_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb48;
        internal global::Fuse.Controls.Text qtext;
        internal global::Fuse.Reactive.EventBinding temp_eb49;
        static Template4()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::Fuse.Reactive.Data("removeItem");
            qtext = new global::Fuse.Controls.Text();
            qtext_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(qtext, __selector0);
            var temp1 = new global::Fuse.Reactive.Data("qty");
            var temp2 = new global::Fuse.Reactive.Data("addItem");
            var temp3 = new global::Fuse.Controls.Image();
            temp_eb48 = new global::Fuse.Reactive.EventBinding(temp);
            var temp4 = new global::Fuse.Controls.Rectangle();
            var temp5 = new global::Fuse.Reactive.DataBinding(qtext_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
            var temp6 = new global::Fuse.Drawing.Stroke();
            var temp7 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
            var temp8 = new global::Fuse.Controls.Image();
            temp_eb49 = new global::Fuse.Reactive.EventBinding(temp2);
            __self.Alignment = Fuse.Elements.Alignment.Center;
            __self.Margin = float4(0f, 10f, 0f, 0f);
            __self.SourceLineNumber = 98;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp3.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
            temp3.Alignment = Fuse.Elements.Alignment.CenterLeft;
            temp3.Margin = float4(0f, 0f, 5f, 0f);
            temp3.SourceLineNumber = 99;
            temp3.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp3, Fuse.Layouts.Dock.Left);
            global::Fuse.Gestures.Clicked.AddHandler(temp3, temp_eb48.OnEvent);
            temp3.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/remove.png"));
            temp3.Bindings.Add(temp_eb48);
            temp.SourceLineNumber = 99;
            temp.SourceFileName = "Pages/inventoryview.ux";
            temp4.Color = float4(1f, 1f, 1f, 1f);
            temp4.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
            temp4.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
            temp4.SourceLineNumber = 100;
            temp4.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp4, Fuse.Layouts.Dock.Left);
            temp4.Strokes.Add(temp6);
            temp4.Children.Add(qtext);
            qtext.TextAlignment = Fuse.Controls.TextAlignment.Center;
            qtext.Alignment = Fuse.Elements.Alignment.Center;
            qtext.Name = __selector1;
            qtext.SourceLineNumber = 101;
            qtext.SourceFileName = "Pages/inventoryview.ux";
            qtext.Bindings.Add(temp5);
            temp1.SourceLineNumber = 101;
            temp1.SourceFileName = "Pages/inventoryview.ux";
            temp6.Color = float4(0f, 1f, 0f, 1f);
            temp6.Width = 1f;
            temp6.Brush = temp7;
            temp8.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
            temp8.Alignment = Fuse.Elements.Alignment.CenterLeft;
            temp8.Margin = float4(5f, 0f, 0f, 0f);
            temp8.SourceLineNumber = 104;
            temp8.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Controls.DockPanel.SetDock(temp8, Fuse.Layouts.Dock.Left);
            global::Fuse.Gestures.Clicked.AddHandler(temp8, temp_eb49.OnEvent);
            temp8.File = new global::Uno.UX.BundleFileSource(import("../../../Assets/add.png"));
            temp8.Bindings.Add(temp_eb49);
            temp2.SourceLineNumber = 104;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            __self.Children.Add(temp3);
            __self.Children.Add(temp4);
            __self.Children.Add(temp8);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "qtext";
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template5: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryView __parent;
        [Uno.WeakReference] internal readonly InventoryView __parentInstance;
        public Template5(InventoryView parent, InventoryView parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        internal global::Fuse.Reactive.EventBinding temp_eb50;
        internal global::Fuse.Reactive.EventBinding temp_eb51;
        static Template5()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp = new global::Fuse.Reactive.Data("addToCart");
            var temp1 = new global::Fuse.Reactive.Data("resetOrder");
            var temp2 = new global::Fuse.Controls.StackPanel();
            var temp3 = new global::SubmitButton1();
            temp_eb50 = new global::Fuse.Reactive.EventBinding(temp);
            var temp4 = new global::SubmitButton1();
            temp_eb51 = new global::Fuse.Reactive.EventBinding(temp1);
            __self.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
            __self.Alignment = Fuse.Elements.Alignment.TopCenter;
            __self.Margin = float4(0f, 10f, 0f, 0f);
            __self.Padding = float4(5f, 5f, 5f, 5f);
            __self.SourceLineNumber = 108;
            __self.SourceFileName = "Pages/inventoryview.ux";
            temp2.SourceLineNumber = 109;
            temp2.SourceFileName = "Pages/inventoryview.ux";
            temp2.Children.Add(temp3);
            temp2.Children.Add(temp4);
            temp3.Text = "Add To Cart";
            temp3.Alignment = Fuse.Elements.Alignment.Center;
            temp3.Margin = float4(0f, 8f, 0f, 0f);
            temp3.SourceLineNumber = 110;
            temp3.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Gestures.Clicked.AddHandler(temp3, temp_eb50.OnEvent);
            temp3.Bindings.Add(temp_eb50);
            temp.SourceLineNumber = 110;
            temp.SourceFileName = "Pages/inventoryview.ux";
            temp4.BgColor = float4(0.1333333f, 0.2196078f, 0.4f, 1f);
            temp4.Text = "Back";
            temp4.Alignment = Fuse.Elements.Alignment.Center;
            temp4.Margin = float4(0f, 8f, 0f, 0f);
            temp4.SourceLineNumber = 111;
            temp4.SourceFileName = "Pages/inventoryview.ux";
            global::Fuse.Gestures.Clicked.AddHandler(temp4, temp_eb51.OnEvent);
            temp4.Bindings.Add(temp_eb51);
            temp1.SourceLineNumber = 111;
            temp1.SourceFileName = "Pages/inventoryview.ux";
            __self.Children.Add(temp2);
            return __self;
        }
    }
    global::Uno.UX.Property<string> checkOutDialogMsg_Value_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<float> checkOutDialogStatus_Opacity_inst;
    global::Uno.UX.Property<float> mainPanelBlur_Radius_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<object> temp2_Items_inst;
    internal global::Fuse.Controls.Panel checkOutDialogStatus;
    internal global::Fuse.Controls.Text checkOutDialogMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb46;
    internal global::Fuse.Reactive.EventBinding temp_eb47;
    internal global::Fuse.Effects.Blur mainPanelBlur;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "checkOutDialogStatus",
        "checkOutDialogMsg",
        "temp_eb46",
        "temp_eb47",
        "mainPanelBlur"
    };
    static InventoryView()
    {
    }
    [global::Uno.UX.UXConstructor]
    public InventoryView(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        checkOutDialogMsg = new global::Fuse.Controls.Text();
        checkOutDialogMsg_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(checkOutDialogMsg, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("dialogMsg");
        var temp4 = new global::Fuse.Reactive.Data("closeCheckOutMessageDialog");
        var temp5 = new global::Fuse.Reactive.Data("gocheckout");
        var temp = new global::Fuse.Triggers.WhileFalse();
        temp_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp, __selector0);
        var temp6 = new global::Fuse.Reactive.Data("isCheckOutDialogMessageShowing");
        checkOutDialogStatus = new global::Fuse.Controls.Panel();
        checkOutDialogStatus_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(checkOutDialogStatus, __selector1);
        mainPanelBlur = new global::Fuse.Effects.Blur();
        mainPanelBlur_Radius_inst = new FirstLoungeNewApp_FuseEffectsBlur_Radius_Property(mainPanelBlur, __selector2);
        var temp1 = new global::Fuse.Triggers.WhileTrue();
        temp1_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp1, __selector0);
        var temp7 = new global::Fuse.Reactive.Data("isCheckOutDialogMessageShowing");
        var temp2 = new global::Fuse.Reactive.Each();
        temp2_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp2, __selector3);
        var temp8 = new global::Fuse.Reactive.Data("productDetails");
        var temp9 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp10 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp11 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp12 = new global::Fuse.Controls.Rectangle();
        var temp13 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp14 = new global::Fuse.Controls.StackPanel();
        var temp15 = new global::Fuse.Reactive.DataBinding(checkOutDialogMsg_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp16 = new global::SubmitButton1();
        temp_eb46 = new global::Fuse.Reactive.EventBinding(temp4);
        var temp17 = new global::SubmitButton1();
        temp_eb47 = new global::Fuse.Reactive.EventBinding(temp5);
        var temp18 = new global::Fuse.Animations.Move();
        var temp19 = new global::Fuse.Animations.Scale();
        var temp20 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp21 = new global::Fuse.Effects.DropShadow();
        var temp22 = new global::Fuse.Animations.Change<float>(checkOutDialogStatus_Opacity_inst);
        var temp23 = new global::Fuse.Animations.Change<float>(mainPanelBlur_Radius_inst);
        var temp24 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp25 = new global::Fuse.Controls.DockPanel();
        var temp26 = new global::Header2();
        var temp27 = new global::Fuse.Controls.StackPanel();
        var temp28 = new global::OnlySearch();
        var temp29 = new global::Fuse.Controls.DockPanel();
        var temp30 = new global::Fuse.Controls.Text();
        var temp31 = new Template(this, this);
        var temp32 = new Template1(this, this);
        var temp33 = new Template2(this, this);
        var temp34 = new Template3(this, this);
        var temp35 = new Template4(this, this);
        var temp36 = new Template5(this, this);
        var temp37 = new global::Fuse.Reactive.DataBinding(temp2_Items_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp38 = new global::FooterNav();
        var temp39 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/inventoryview.ux";
        temp9.LineNumber = 2;
        temp9.FileName = "Pages/inventoryview.ux";
        temp9.SourceLineNumber = 2;
        temp9.SourceFileName = "Pages/inventoryview.ux";
        temp9.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp10.LineNumber = 5;
        temp10.FileName = "Pages/inventoryview.ux";
        temp10.SourceLineNumber = 5;
        temp10.SourceFileName = "Pages/inventoryview.ux";
        temp10.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/inventory.js"));
        temp11.Code = "\n\t\t// var inventory = require(\"inventory\");\n\t\t// var Observable = require(\"FuseJS/Observable\");\n\t\t\n\t\t// var error = Observable();\n\n\t\t// modal\n\t\t\n\n\t\t// module.exports={\n\t\t\t\n\t\t// };\n\t\t\n\t";
        temp11.LineNumber = 6;
        temp11.FileName = "Pages/inventoryview.ux";
        temp11.SourceLineNumber = 6;
        temp11.SourceFileName = "Pages/inventoryview.ux";
        checkOutDialogStatus.Width = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        checkOutDialogStatus.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        checkOutDialogStatus.Padding = float4(20f, 40f, 20f, 0f);
        checkOutDialogStatus.Opacity = 0f;
        checkOutDialogStatus.Name = __selector4;
        checkOutDialogStatus.SourceLineNumber = 44;
        checkOutDialogStatus.SourceFileName = "Pages/inventoryview.ux";
        checkOutDialogStatus.Children.Add(temp12);
        checkOutDialogStatus.Children.Add(temp14);
        checkOutDialogStatus.Children.Add(temp);
        checkOutDialogStatus.Children.Add(temp21);
        temp12.CornerRadius = float4(2f, 2f, 2f, 2f);
        temp12.Opacity = 0.8f;
        temp12.Layer = Fuse.Layer.Background;
        temp12.SourceLineNumber = 45;
        temp12.SourceFileName = "Pages/inventoryview.ux";
        temp12.Fill = temp13;
        temp14.SourceLineNumber = 46;
        temp14.SourceFileName = "Pages/inventoryview.ux";
        temp14.Children.Add(checkOutDialogMsg);
        temp14.Children.Add(temp16);
        temp14.Children.Add(temp17);
        checkOutDialogMsg.TextAlignment = Fuse.Controls.TextAlignment.Center;
        checkOutDialogMsg.Alignment = Fuse.Elements.Alignment.Center;
        checkOutDialogMsg.Name = __selector5;
        checkOutDialogMsg.SourceLineNumber = 48;
        checkOutDialogMsg.SourceFileName = "Pages/inventoryview.ux";
        checkOutDialogMsg.Font = global::LoginPage.Poppins;
        checkOutDialogMsg.Bindings.Add(temp15);
        temp3.SourceLineNumber = 48;
        temp3.SourceFileName = "Pages/inventoryview.ux";
        temp16.Text = "Add More";
        temp16.Alignment = Fuse.Elements.Alignment.Center;
        temp16.Margin = float4(0f, 10f, 0f, 0f);
        temp16.SourceLineNumber = 50;
        temp16.SourceFileName = "Pages/inventoryview.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp16, temp_eb46.OnEvent);
        temp16.Bindings.Add(temp_eb46);
        temp4.SourceLineNumber = 50;
        temp4.SourceFileName = "Pages/inventoryview.ux";
        temp17.BgColor = float4(0.1333333f, 0.2196078f, 0.4f, 1f);
        temp17.Text = "Checkout";
        temp17.Alignment = Fuse.Elements.Alignment.Center;
        temp17.Margin = float4(0f, 5f, 0f, 50f);
        temp17.SourceLineNumber = 51;
        temp17.SourceFileName = "Pages/inventoryview.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp17, temp_eb47.OnEvent);
        temp17.Bindings.Add(temp_eb47);
        temp5.SourceLineNumber = 51;
        temp5.SourceFileName = "Pages/inventoryview.ux";
        temp.SourceLineNumber = 55;
        temp.SourceFileName = "Pages/inventoryview.ux";
        temp.Animators.Add(temp18);
        temp.Animators.Add(temp19);
        temp.Bindings.Add(temp20);
        temp18.X = 3f;
        temp18.Duration = 0.6;
        temp18.RelativeTo = Fuse.TranslationModes.Size;
        temp18.Easing = Fuse.Animations.Easing.BackIn;
        temp19.Factor = 0.8f;
        temp19.Duration = 0.6;
        temp19.Easing = Fuse.Animations.Easing.BackIn;
        temp6.SourceLineNumber = 55;
        temp6.SourceFileName = "Pages/inventoryview.ux";
        temp21.SourceLineNumber = 60;
        temp21.SourceFileName = "Pages/inventoryview.ux";
        temp1.SourceLineNumber = 64;
        temp1.SourceFileName = "Pages/inventoryview.ux";
        temp1.Animators.Add(temp22);
        temp1.Animators.Add(temp23);
        temp1.Bindings.Add(temp24);
        temp22.Value = 1f;
        temp22.Duration = 0.3;
        temp22.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp23.Value = 10f;
        temp23.Duration = 0.3;
        temp23.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp7.SourceLineNumber = 64;
        temp7.SourceFileName = "Pages/inventoryview.ux";
        temp25.SourceLineNumber = 69;
        temp25.SourceFileName = "Pages/inventoryview.ux";
        temp25.Children.Add(temp26);
        temp25.Children.Add(temp27);
        temp25.Children.Add(temp38);
        temp25.Children.Add(mainPanelBlur);
        temp26.SourceLineNumber = 71;
        temp26.SourceFileName = "Pages/inventoryview.ux";
        temp27.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp27.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp27.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp27.SourceLineNumber = 74;
        temp27.SourceFileName = "Pages/inventoryview.ux";
        temp27.Children.Add(temp28);
        temp27.Children.Add(temp29);
        temp27.Children.Add(temp2);
        temp28.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp28.SourceLineNumber = 76;
        temp28.SourceFileName = "Pages/inventoryview.ux";
        temp29.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp29.Margin = float4(0f, 0f, 0f, 8f);
        temp29.SourceLineNumber = 78;
        temp29.SourceFileName = "Pages/inventoryview.ux";
        temp29.Children.Add(temp30);
        temp30.Value = "Product Details";
        temp30.SourceLineNumber = 79;
        temp30.SourceFileName = "Pages/inventoryview.ux";
        temp30.Font = global::LoginPage.Poppins;
        temp2.SourceLineNumber = 82;
        temp2.SourceFileName = "Pages/inventoryview.ux";
        temp2.Templates.Add(temp31);
        temp2.Templates.Add(temp32);
        temp2.Templates.Add(temp33);
        temp2.Templates.Add(temp34);
        temp2.Templates.Add(temp35);
        temp2.Templates.Add(temp36);
        temp2.Bindings.Add(temp37);
        temp8.SourceLineNumber = 82;
        temp8.SourceFileName = "Pages/inventoryview.ux";
        temp38.SourceLineNumber = 119;
        temp38.SourceFileName = "Pages/inventoryview.ux";
        mainPanelBlur.Radius = 0f;
        mainPanelBlur.Name = __selector6;
        mainPanelBlur.SourceLineNumber = 121;
        mainPanelBlur.SourceFileName = "Pages/inventoryview.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(checkOutDialogStatus);
        __g_nametable.Objects.Add(checkOutDialogMsg);
        __g_nametable.Objects.Add(temp_eb46);
        __g_nametable.Objects.Add(temp_eb47);
        __g_nametable.Objects.Add(mainPanelBlur);
        this.Background = temp39;
        this.Children.Add(temp9);
        this.Children.Add(temp10);
        this.Children.Add(temp11);
        this.Children.Add(checkOutDialogStatus);
        this.Children.Add(temp1);
        this.Children.Add(temp25);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Opacity";
    static global::Uno.UX.Selector __selector2 = "Radius";
    static global::Uno.UX.Selector __selector3 = "Items";
    static global::Uno.UX.Selector __selector4 = "checkOutDialogStatus";
    static global::Uno.UX.Selector __selector5 = "checkOutDialogMsg";
    static global::Uno.UX.Selector __selector6 = "mainPanelBlur";
}
