[Uno.Compiler.UxGenerated]
public partial class SubmitButton1: Fuse.Controls.Button
{
    float4 _field_BgColor;
    [global::Uno.UX.UXOriginSetter("SetBgColor")]
    public float4 BgColor
    {
        get { return _field_BgColor; }
        set { SetBgColor(value, null); }
    }
    public void SetBgColor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_BgColor)
        {
            _field_BgColor = value;
            OnPropertyChanged("BgColor", origin);
        }
    }
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly SubmitButton1 __parent;
        [Uno.WeakReference] internal readonly SubmitButton1 __parentInstance;
        public Template(SubmitButton1 parent, SubmitButton1 parentInstance): base("GraphicsAppearance", false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Value_inst;
        global::Uno.UX.Property<float4> temp1_Color_inst;
        global::Uno.UX.Property<float4> __self_Color_inst;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.Panel();
            var temp2 = new global::Fuse.Reactive.Constant(__parent);
            var temp = new global::Fuse.Controls.Text();
            temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
            var temp3 = new global::Fuse.Reactive.Property(temp2, FirstLoungeNewApp_accessor_Fuse_Controls_ButtonBase_Text.Singleton);
            var temp4 = new global::Fuse.Reactive.Constant(__parent);
            var temp1 = new global::Fuse.Controls.Rectangle();
            temp1_Color_inst = new FirstLoungeNewApp_FuseControlsShape_Color_Property(temp1, __selector1);
            var temp5 = new global::Fuse.Reactive.Property(temp4, FirstLoungeNewApp_accessor_SubmitButton1_BgColor.Singleton);
            __self_Color_inst = new FirstLoungeNewApp_FuseControlsPanel_Color_Property(__self, __selector1);
            var temp6 = new global::Fuse.Reactive.Constant(__parent);
            var temp7 = new global::Fuse.Reactive.Property(temp6, FirstLoungeNewApp_accessor_SubmitButton1_BgColor.Singleton);
            var temp8 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Read);
            var temp9 = new global::Fuse.Reactive.DataBinding(temp1_Color_inst, temp5, Fuse.Reactive.BindingMode.Default);
            var temp10 = new global::Fuse.Gestures.WhilePressed();
            var temp11 = new global::Fuse.Animations.Change<float4>(__self_Color_inst);
            var temp12 = new global::Fuse.Reactive.DataBinding(__self_Color_inst, temp7, Fuse.Reactive.BindingMode.Default);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
            __self.Name = __selector2;
            __self.SourceLineNumber = 168;
            __self.SourceFileName = "Pages/categoryProducts.ux";
            temp.FontSize = 16f;
            temp.TextAlignment = Fuse.Controls.TextAlignment.Center;
            temp.Color = float4(1f, 1f, 1f, 1f);
            temp.Alignment = Fuse.Elements.Alignment.Center;
            temp.Margin = float4(10f, 10f, 10f, 10f);
            temp.SourceLineNumber = 169;
            temp.SourceFileName = "Pages/categoryProducts.ux";
            temp.Font = global::LoginPage.Poppins;
            temp.Bindings.Add(temp8);
            temp3.SourceLineNumber = 169;
            temp3.SourceFileName = "Pages/categoryProducts.ux";
            temp2.SourceLineNumber = 169;
            temp2.SourceFileName = "Pages/categoryProducts.ux";
            temp1.CornerRadius = float4(1f, 1f, 1f, 1f);
            temp1.Layer = Fuse.Layer.Background;
            temp1.SourceLineNumber = 170;
            temp1.SourceFileName = "Pages/categoryProducts.ux";
            temp1.Bindings.Add(temp9);
            temp5.SourceLineNumber = 170;
            temp5.SourceFileName = "Pages/categoryProducts.ux";
            temp4.SourceLineNumber = 170;
            temp4.SourceFileName = "Pages/categoryProducts.ux";
            temp10.SourceLineNumber = 171;
            temp10.SourceFileName = "Pages/categoryProducts.ux";
            temp10.Animators.Add(temp11);
            temp11.Value = float4(0.7333333f, 0.7333333f, 0.7333333f, 1f);
            temp11.Duration = 0.05;
            temp11.DurationBack = 0.1;
            temp7.SourceLineNumber = 168;
            temp7.SourceFileName = "Pages/categoryProducts.ux";
            temp6.SourceLineNumber = 168;
            temp6.SourceFileName = "Pages/categoryProducts.ux";
            __self.Children.Add(temp);
            __self.Children.Add(temp1);
            __self.Children.Add(temp10);
            __self.Bindings.Add(temp12);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Value";
        static global::Uno.UX.Selector __selector1 = "Color";
        static global::Uno.UX.Selector __selector2 = "Buttonx";
    }
    static SubmitButton1()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SubmitButton1()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var Buttonx = new Template(this, this);
        this.BgColor = float4(0.7333333f, 0.5490196f, 0.007843138f, 1f);
        this.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        this.Margin = float4(10f, 0f, 10f, 0f);
        this.SourceLineNumber = 166;
        this.SourceFileName = "Pages/categoryProducts.ux";
        this.Templates.Add(Buttonx);
    }
}
