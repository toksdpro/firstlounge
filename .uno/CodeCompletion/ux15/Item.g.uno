[Uno.Compiler.UxGenerated]
public partial class Item: Fuse.Controls.DockPanel
{
    string _field_title;
    [global::Uno.UX.UXOriginSetter("Settitle")]
    public string title
    {
        get { return _field_title; }
        set { Settitle(value, null); }
    }
    public void Settitle(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_title)
        {
            _field_title = value;
            OnPropertyChanged("title", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    internal global::Fuse.Controls.Rectangle underlay;
    static Item()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Item()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, FirstLoungeNewApp_accessor_Item_title.Singleton);
        var temp3 = new global::Fuse.Controls.StackPanel();
        var temp4 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp5 = new global::Fuse.Controls.Panel();
        var temp6 = new global::Fuse.Controls.StackPanel();
        var temp7 = new global::Fuse.Controls.Rectangle();
        var temp8 = new global::Fuse.Rotation();
        var temp9 = new global::Fuse.Controls.Rectangle();
        var temp10 = new global::Fuse.Rotation();
        underlay = new global::Fuse.Controls.Rectangle();
        this.Height = new Uno.UX.Size(56f, Uno.UX.Unit.Unspecified);
        this.Margin = float4(0f, 5f, 0f, 0f);
        this.SourceLineNumber = 20;
        this.SourceFileName = "Pages/settings.ux";
        temp3.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp3.Padding = float4(10f, 0f, 0f, 0f);
        temp3.SourceLineNumber = 22;
        temp3.SourceFileName = "Pages/settings.ux";
        temp3.Children.Add(temp);
        temp.TextColor = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.Margin = float4(8f, 0f, 8f, 0f);
        temp.SourceLineNumber = 23;
        temp.SourceFileName = "Pages/settings.ux";
        temp.Bindings.Add(temp4);
        temp2.SourceLineNumber = 23;
        temp2.SourceFileName = "Pages/settings.ux";
        temp1.SourceLineNumber = 23;
        temp1.SourceFileName = "Pages/settings.ux";
        temp5.HitTestMode = Fuse.Elements.HitTestMode.LocalBounds;
        temp5.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp5.SourceLineNumber = 27;
        temp5.SourceFileName = "Pages/settings.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp5, Fuse.Layouts.Dock.Right);
        temp5.Children.Add(temp6);
        temp6.ItemSpacing = 3f;
        temp6.Width = new Uno.UX.Size(12f, Uno.UX.Unit.Unspecified);
        temp6.Alignment = Fuse.Elements.Alignment.Center;
        temp6.SourceLineNumber = 28;
        temp6.SourceFileName = "Pages/settings.ux";
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp9);
        temp7.CornerRadius = float4(1f, 1f, 1f, 1f);
        temp7.Color = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp7.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp7.SourceLineNumber = 29;
        temp7.SourceFileName = "Pages/settings.ux";
        temp7.Children.Add(temp8);
        temp8.DegreesZ = 30f;
        temp8.SourceLineNumber = 30;
        temp8.SourceFileName = "Pages/settings.ux";
        temp9.CornerRadius = float4(1f, 1f, 1f, 1f);
        temp9.Color = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp9.Height = new Uno.UX.Size(2f, Uno.UX.Unit.Unspecified);
        temp9.SourceLineNumber = 33;
        temp9.SourceFileName = "Pages/settings.ux";
        temp9.Children.Add(temp10);
        temp10.DegreesZ = -30f;
        temp10.SourceLineNumber = 34;
        temp10.SourceFileName = "Pages/settings.ux";
        underlay.CornerRadius = float4(2f, 2f, 2f, 2f);
        underlay.Color = float4(1f, 1f, 1f, 1f);
        underlay.Opacity = 1f;
        underlay.Layer = Fuse.Layer.Background;
        underlay.Name = __selector1;
        underlay.SourceLineNumber = 39;
        underlay.SourceFileName = "Pages/settings.ux";
        this.Children.Add(temp3);
        this.Children.Add(temp5);
        this.Children.Add(underlay);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "underlay";
}
