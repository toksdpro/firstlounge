[Uno.Compiler.UxGenerated]
public partial class ProductCreate: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly ProductCreate __parent;
        [Uno.WeakReference] internal readonly ProductCreate __parentInstance;
        public Template(ProductCreate parent, ProductCreate parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        global::Uno.UX.Property<string> temp_Image_inst;
        global::Uno.UX.Property<string> temp_name_inst;
        internal global::Fuse.Reactive.EventBinding temp_eb55;
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Fuse.Controls.DockPanel();
            var temp1 = "http://restaurants.saddleng.com/storage/";
            var temp2 = new global::Fuse.Reactive.Constant(temp1);
            var temp3 = new global::Fuse.Reactive.Data("image_url");
            var temp = new global::HomeIcons();
            temp_Image_inst = new FirstLoungeNewApp_HomeIcons_Image_Property(temp, __selector0);
            var temp4 = new global::Fuse.Reactive.Add(temp2, temp3);
            temp_name_inst = new FirstLoungeNewApp_HomeIcons_name_Property(temp, __selector1);
            var temp5 = new global::Fuse.Reactive.Data("name");
            var temp6 = new global::Fuse.Reactive.Data("categoryProductview");
            var temp7 = new global::Fuse.Reactive.DataBinding(temp_Image_inst, temp4, Fuse.Reactive.BindingMode.Default);
            var temp8 = new global::Fuse.Reactive.DataBinding(temp_name_inst, temp5, Fuse.Reactive.BindingMode.Default);
            var temp9 = new global::Fuse.Controls.Rectangle();
            var temp10 = new global::Fuse.Drawing.Stroke();
            var temp11 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
            temp_eb55 = new global::Fuse.Reactive.EventBinding(temp6);
            __self.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
            __self.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            __self.Alignment = Fuse.Elements.Alignment.TopCenter;
            __self.Margin = float4(5f, 5f, 5f, 5f);
            __self.Padding = float4(10f, 10f, 10f, 10f);
            __self.SourceLineNumber = 228;
            __self.SourceFileName = "Pages/productCreate.ux";
            global::Fuse.Gestures.Clicked.AddHandler(__self, temp_eb55.OnEvent);
            temp.SourceLineNumber = 230;
            temp.SourceFileName = "Pages/productCreate.ux";
            temp.Bindings.Add(temp7);
            temp.Bindings.Add(temp8);
            temp4.SourceLineNumber = 230;
            temp4.SourceFileName = "Pages/productCreate.ux";
            temp2.SourceLineNumber = 230;
            temp2.SourceFileName = "Pages/productCreate.ux";
            temp3.SourceLineNumber = 230;
            temp3.SourceFileName = "Pages/productCreate.ux";
            temp5.SourceLineNumber = 230;
            temp5.SourceFileName = "Pages/productCreate.ux";
            temp9.CornerRadius = float4(0f, 0f, 0f, 0f);
            temp9.Color = float4(1f, 1f, 1f, 1f);
            temp9.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
            temp9.Layer = Fuse.Layer.Background;
            temp9.SourceLineNumber = 233;
            temp9.SourceFileName = "Pages/productCreate.ux";
            temp9.Strokes.Add(temp10);
            temp10.Color = float4(0f, 1f, 0f, 1f);
            temp10.Width = 1f;
            temp10.Brush = temp11;
            temp6.SourceLineNumber = 228;
            temp6.SourceFileName = "Pages/productCreate.ux";
            __self.Children.Add(temp);
            __self.Children.Add(temp9);
            __self.Bindings.Add(temp_eb55);
            return __self;
        }
        static global::Uno.UX.Selector __selector0 = "Image";
        static global::Uno.UX.Selector __selector1 = "name";
    }
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Fieldvalue_inst;
    global::Uno.UX.Property<string> temp1_BorderColor_inst;
    global::Uno.UX.Property<string> temp2_Fieldvalue_inst;
    global::Uno.UX.Property<string> temp2_BorderColor_inst;
    global::Uno.UX.Property<object> temp3_Items_inst;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb54;
    internal global::Fuse.Reactive.EventBinding temp_eb56;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "ScrollbarBox",
        "temp_eb54",
        "temp_eb56"
    };
    static ProductCreate()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ProductCreate(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp4 = new global::Fuse.Reactive.Data("title");
        var temp5 = " Settings";
        var temp6 = new global::Fuse.Reactive.Constant(temp5);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp7 = new global::Fuse.Reactive.Add(temp4, temp6);
        var temp1 = new global::ProductField();
        temp1_Fieldvalue_inst = new FirstLoungeNewApp_ProductField_Fieldvalue_Property(temp1, __selector1);
        var temp8 = new global::Fuse.Reactive.Data("categoryName");
        temp1_BorderColor_inst = new FirstLoungeNewApp_ProductField_BorderColor_Property(temp1, __selector2);
        var temp9 = new global::Fuse.Reactive.Data("priceBorderColor");
        var temp2 = new global::ProductField();
        temp2_Fieldvalue_inst = new FirstLoungeNewApp_ProductField_Fieldvalue_Property(temp2, __selector1);
        var temp10 = new global::Fuse.Reactive.Data("categoryPrice");
        temp2_BorderColor_inst = new FirstLoungeNewApp_ProductField_BorderColor_Property(temp2, __selector2);
        var temp11 = new global::Fuse.Reactive.Data("priceBorderColor");
        var temp12 = new global::Fuse.Reactive.Data("addProduct");
        var temp3 = new global::Fuse.Reactive.Each();
        temp3_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp3, __selector3);
        var temp13 = new global::Fuse.Reactive.Data("productsByCategoryList");
        var temp14 = new global::Fuse.Reactive.Data("goBack");
        var temp15 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp16 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp17 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp18 = new global::Fuse.Controls.DockPanel();
        var temp19 = new global::Header();
        var temp20 = new global::Fuse.Controls.StackPanel();
        var temp21 = new global::Fuse.Controls.DockPanel();
        var temp22 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp23 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp24 = new global::Fuse.Controls.StackPanel();
        var temp25 = new global::Fuse.Controls.Text();
        var temp26 = new global::Fuse.Controls.DockPanel();
        var temp27 = new global::Fuse.Controls.Text();
        var temp28 = new global::Fuse.Reactive.DataBinding(temp1_Fieldvalue_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp29 = new global::Fuse.Reactive.DataBinding(temp1_BorderColor_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp30 = new global::Fuse.Controls.DockPanel();
        var temp31 = new global::Fuse.Controls.Text();
        var temp32 = new global::Fuse.Reactive.DataBinding(temp2_Fieldvalue_inst, temp10, Fuse.Reactive.BindingMode.Default);
        var temp33 = new global::Fuse.Reactive.DataBinding(temp2_BorderColor_inst, temp11, Fuse.Reactive.BindingMode.Default);
        var temp34 = new global::SubmitButton1();
        temp_eb54 = new global::Fuse.Reactive.EventBinding(temp12);
        var temp35 = new global::Fuse.Controls.Grid();
        var temp36 = new Template(this, this);
        var temp37 = new global::Fuse.Reactive.DataBinding(temp3_Items_inst, temp13, Fuse.Reactive.BindingMode.Default);
        var temp38 = new global::Fuse.Controls.Panel();
        var temp39 = new global::SubmitButton1();
        temp_eb56 = new global::Fuse.Reactive.EventBinding(temp14);
        var temp40 = new global::FooterNav();
        var temp41 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/productCreate.ux";
        temp15.LineNumber = 3;
        temp15.FileName = "Pages/productCreate.ux";
        temp15.SourceLineNumber = 3;
        temp15.SourceFileName = "Pages/productCreate.ux";
        temp15.File = new global::Uno.UX.BundleFileSource(import("../../../fdb-all.js"));
        temp16.LineNumber = 4;
        temp16.FileName = "Pages/productCreate.ux";
        temp16.SourceLineNumber = 4;
        temp16.SourceFileName = "Pages/productCreate.ux";
        temp16.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp17.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar Backend = require(\"Modules/Backend.js\");\n\t\tvar FileSystem = require(\"FuseJS/FileSystem\");\n\n\t\tvar login = require('Pages/login.js');\n\t\tvar category = Observable();\n\t\tvar api = require('Pages/api.js');\n\t\tvar Backend = require('Modules/Backend.js');\n\t\tvar categoryName = Observable();\n\t\tvar categoryPrice = Observable();\n\t\tvar error = Observable();\n\t\tvar categoryList = Observable();\n\t\tvar productsByCategoryList = Observable();\n\t\tvar visible = Observable(true);\n\n\t\tvar isDialogShowing = Observable(false);\n\n\t\tvar dialogMsg = Observable();\t\n\t\t\n\t\tfunction LoadProductsByCategory(category_id) {\n\n\t\t\t// var arr = [];\n\t\t\t// productsByCategoryList.refreshAll(arr);\n\n\t\t\tlogin.getToken()\n\t\t\t.then(function(contents) {\n\t\t\t    console.log('token: '+contents);\n\t\t\t\tif (contents != null) {\n\t\t\t\t\tGetCategoryProducts(contents);\n\t\t\t\t}else{\n\t\t\t\t\tconsole.log(\"Error not logged in\");\n\t\t\t\t}\n\t\t\t}, function(error) {\n\t\t\t\tconsole.log(error);\n\t\t\t});\n\n\t\t\tfunction GetCategoryProducts(token) {\n\t\t\t\tapi.ApiCategoryProducts(token, category_id)\n\t\t\t\t.then(function(response) {\n\t\t\t\t\tvar ok = response.ok;\n\t\t\t\t\tvar status = response.status;\n\t\t\t\t\tvar response_msg = JSON.parse(response._bodyText);\n\t\t\t\t\t// hide loader\n\t\t\t\t\tvisible.value = false;\n\t\t\t\t\tif (ok) {\n\t\t\t\t\t\tisDialogShowing.value = false;\n\t\t\t\t\t\tconsole.log(\"api: \"+JSON.stringify(JSON.parse(response._bodyText).products));\n\t\t\t\t\t\tproductsByCategoryList.replaceAll(JSON.parse(response._bodyText).products);\n\t\t\t\t\t}else{\n\t\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\t\tif (status == 502) {\n\t\t\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\t\t\tconsole.log(\"Network Error\");\n\t\t\t\t\t\t}else {\n\t\t\t\t\t\t\tdialogMsg.value = \"There was an error\";\n\t\t\t\t\t\t\tconsole.log(status);\t\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t}).catch(function(err) {\n\t\t\t\t\tvisible.value = false;\n\n\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t});\n\t\t\t}\n\t\t\t\n\t\t}\n\t\t\n\t\tvar stresObj = this.Parameter.map(function(x) { return x.data; });\t\t\t\n\t\t\t//var data = JSON.parse(stresObj.value);\n\t\t\t//console.log(\"Name\" + data.name);\n\t\t\tstresObj.onValueChanged(module, function(param) {\n\t\t\t// console.log(\"new \"+param);\n\t\t\tvar data = [];\n\t\t\tdata[0] = JSON.parse(param);\n\t\t\t\t// console.log('data: '+JSON.stringify(data));\n\n\t\t\tcategory.replaceAll(data);\n\t\t\t// console.log(param.toLowerCase());\n\t\t\t// if (data[0].toLowerCase() == \"ticket\") {\n\t\t\t// \tImage.value = \"Assets/packages.png\";\n\t\t\t// }else{\n\t\t\t// \tImage.value = \"Assets/\"+data[0].toLowerCase()+\".png\";\n\t\t\t// }\n\n\t\t\tconsole.log(\"categoryID: \"+JSON.stringify(category.value.id));\n\n\t\t\tLoadProductsByCategory(category.value.id);\n\n\t\t\t// console.log(JSON.stringify(Backend.lowerlize(category.value.name)));\n\t\t\t// console.log(JSON.stringify(category));\n\t\t\t// productsByCategoryList = Backend.LoadProductsByCategory(\"ticket\");\n\t\t\t\t\t\t\n\t\t});\n\nfunction addProduct(args){\n\t//console.log(JSON.stringify(args.data));\t\n\t//saveCategoryTitle(args.data.name);\n\t\n\t//var param = JSON.stringify(args.data);\n\t// console.log(\"param: \"+param);\n\tif (categoryName.length === 0 || categoryPrice.length === 0){\n\n\t}\n\t{\n\t\tconsole.log(\"Product\" + categoryName.value)\n\t\tconsole.log(\"Price\" + categoryPrice.value)\n\t\tlogin.getToken()\n\t\t\t.then(function(contents) {\n\t\t\t    console.log('token: '+contents);\n\t\t\t\tif (contents != null) {\n\t\t\t\t\t\n\t\t\t\t\tcreateProduct(contents, categoryName.value, categoryPrice.value, category.value.id);\n\t\t\t\t\tcategoryName.value = \"\";\n\t\t\t\t\tcategoryPrice.value = \"\";\n\t\t\t\t\trouter.push(\"productcreate\", {category: param});\n\t\t\t\t}else{\n\t\t\t\t\tconsole.log(\"Error not logged in\");\n\t\t\t\t\tLoadProductsByCategory(category.value.id);\n\t\t\t\t}\n\t\t\t}, function(error) {\n\t\t\t\tconsole.log(error);\n\t\t\t});\n\t\t\n\t}\n\t}\n\n\t\n\nfunction createProduct(token, product_name, product_price, product_category_id) {\n\t\n\t\t\t\tapi.ApiProductCreate(token, product_name, product_price, product_category_id)\n\t\t\t\t.then(function(response) {\n\t\t\t\t\t// console.log('tokenD: '+token);\n\t\t\t\t\tvar ok = response.ok;\n\t\t\t\t\tvar status = response.status;\n\t\t\t\t\tconsole.log('status: '+status);\n\t\t\t\t\tvar response_msg = JSON.parse(response._bodyText);\n\n\t\t\t\t\tif (ok) {\t\t\t\t\t\t\n\t\t\t\t\t\tconsole.log(\"it workds\");\n\t\t\t\t\t\t\n\t\t\t\t\t\tLoadProductsByCategory(product_category_id); \n\n\t\t\t\t\t}else{\n\n\t\t\t\t\t\tconsole.log(\"Error Ocured\");\n\t\t\t\t\t\tLoadProductsByCategory(product_category_id);\n\t\t\t\t\t\tif (status == 502) {\n\t\t\t\t\t\t\tconsole.log(\"Network Error\");\t\t\t\t\t\t\t\n\n\t\t\t\t\t\t}else {\n\t\t\t\t\t\t\tconsole.log(status);\t\n\t\t\t\t\t\t\t\n\t\t\t\t\t\t}\n\t\t\t\t\t\t\n\t\t\t\t\t}\n\t\t\t\t\t// apistatus.value = status;\n\n\t\t\t\t}).catch(function(err) {\t\t\t\t\t\n\n\t\t\t\t\tLoadProductsByCategory(product_category_id);\n\t\t\t\t\t\n\t\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t});\n}\n\n//LoadCategory();\n\n\t\t\n\t\tmodule.exports = {\n\t\t\tproductsByCategoryList: productsByCategoryList,\n\t\t\ttitle: category.value.name,\n\t\t\tdialogMsg: dialogMsg,\n\t\t\taddProduct: addProduct,\n\t\t\tisDialogShowing: isDialogShowing,\n\t\t\tcategoryName: categoryName,\n\t\t\tcategoryPrice: categoryPrice,\n\t\t\tvisible: visible\n\t\t};\n\t";
        temp17.LineNumber = 5;
        temp17.FileName = "Pages/productCreate.ux";
        temp17.SourceLineNumber = 5;
        temp17.SourceFileName = "Pages/productCreate.ux";
        temp18.SourceLineNumber = 190;
        temp18.SourceFileName = "Pages/productCreate.ux";
        temp18.Children.Add(temp19);
        temp18.Children.Add(temp20);
        temp18.Children.Add(temp23);
        temp18.Children.Add(temp40);
        temp19.SourceLineNumber = 191;
        temp19.SourceFileName = "Pages/productCreate.ux";
        temp20.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp20.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp20.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp20.SourceLineNumber = 194;
        temp20.SourceFileName = "Pages/productCreate.ux";
        temp20.Children.Add(temp21);
        temp21.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp21.Margin = float4(0f, 10f, 0f, 10f);
        temp21.SourceLineNumber = 198;
        temp21.SourceFileName = "Pages/productCreate.ux";
        temp21.Children.Add(temp);
        temp.SourceLineNumber = 199;
        temp.SourceFileName = "Pages/productCreate.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp22);
        temp7.SourceLineNumber = 199;
        temp7.SourceFileName = "Pages/productCreate.ux";
        temp4.SourceLineNumber = 199;
        temp4.SourceFileName = "Pages/productCreate.ux";
        temp6.SourceLineNumber = 199;
        temp6.SourceFileName = "Pages/productCreate.ux";
        temp23.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp23.Alignment = Fuse.Elements.Alignment.Center;
        temp23.SourceLineNumber = 204;
        temp23.SourceFileName = "Pages/productCreate.ux";
        temp23.Children.Add(ScrollbarBox);
        temp23.Children.Add(temp38);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(450f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector4;
        ScrollbarBox.SourceLineNumber = 205;
        ScrollbarBox.SourceFileName = "Pages/productCreate.ux";
        ScrollbarBox.Children.Add(temp24);
        temp24.Color = float4(1f, 1f, 1f, 1f);
        temp24.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp24.Margin = float4(0f, 20f, 0f, 0f);
        temp24.SourceLineNumber = 207;
        temp24.SourceFileName = "Pages/productCreate.ux";
        temp24.Children.Add(temp25);
        temp24.Children.Add(temp26);
        temp24.Children.Add(temp30);
        temp24.Children.Add(temp34);
        temp24.Children.Add(temp35);
        temp25.Value = "Add New Product";
        temp25.TextColor = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp25.Margin = float4(10f, 10f, 10f, 10f);
        temp25.SourceLineNumber = 208;
        temp25.SourceFileName = "Pages/productCreate.ux";
        temp26.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp26.Margin = float4(5f, 0f, 5f, 0f);
        temp26.SourceLineNumber = 209;
        temp26.SourceFileName = "Pages/productCreate.ux";
        temp26.Children.Add(temp27);
        temp26.Children.Add(temp1);
        temp27.Value = "Name: ";
        temp27.TextColor = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp27.Width = new Uno.UX.Size(45f, Uno.UX.Unit.Percent);
        temp27.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp27.SourceLineNumber = 210;
        temp27.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp27, Fuse.Layouts.Dock.Left);
        temp27.Font = global::LoginPage.Poppins;
        temp1.Width = new Uno.UX.Size(55f, Uno.UX.Unit.Percent);
        temp1.SourceLineNumber = 211;
        temp1.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
        temp1.Bindings.Add(temp28);
        temp1.Bindings.Add(temp29);
        temp8.SourceLineNumber = 211;
        temp8.SourceFileName = "Pages/productCreate.ux";
        temp9.SourceLineNumber = 211;
        temp9.SourceFileName = "Pages/productCreate.ux";
        temp30.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp30.Margin = float4(5f, 0f, 5f, 0f);
        temp30.SourceLineNumber = 214;
        temp30.SourceFileName = "Pages/productCreate.ux";
        temp30.Children.Add(temp31);
        temp30.Children.Add(temp2);
        temp31.Value = "Price: ";
        temp31.TextColor = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp31.Width = new Uno.UX.Size(45f, Uno.UX.Unit.Percent);
        temp31.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp31.SourceLineNumber = 216;
        temp31.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp31, Fuse.Layouts.Dock.Left);
        temp31.Font = global::LoginPage.Poppins;
        temp2.Width = new Uno.UX.Size(55f, Uno.UX.Unit.Percent);
        temp2.SourceLineNumber = 217;
        temp2.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Left);
        temp2.Bindings.Add(temp32);
        temp2.Bindings.Add(temp33);
        temp10.SourceLineNumber = 217;
        temp10.SourceFileName = "Pages/productCreate.ux";
        temp11.SourceLineNumber = 217;
        temp11.SourceFileName = "Pages/productCreate.ux";
        temp34.BgColor = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp34.Text = "Save";
        temp34.Alignment = Fuse.Elements.Alignment.Center;
        temp34.Margin = float4(10f, 20f, 10f, 20f);
        temp34.SourceLineNumber = 220;
        temp34.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp34, temp_eb54.OnEvent);
        temp34.Bindings.Add(temp_eb54);
        temp12.SourceLineNumber = 220;
        temp12.SourceFileName = "Pages/productCreate.ux";
        temp35.ColumnCount = 2;
        temp35.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp35.SourceLineNumber = 222;
        temp35.SourceFileName = "Pages/productCreate.ux";
        temp35.Children.Add(temp3);
        temp3.SourceLineNumber = 225;
        temp3.SourceFileName = "Pages/productCreate.ux";
        temp3.Templates.Add(temp36);
        temp3.Bindings.Add(temp37);
        temp13.SourceLineNumber = 225;
        temp13.SourceFileName = "Pages/productCreate.ux";
        temp38.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp38.Margin = float4(0f, 10f, 0f, 0f);
        temp38.Padding = float4(5f, 5f, 5f, 5f);
        temp38.SourceLineNumber = 253;
        temp38.SourceFileName = "Pages/productCreate.ux";
        temp38.Children.Add(temp39);
        temp39.Text = "Back";
        temp39.Alignment = Fuse.Elements.Alignment.Center;
        temp39.SourceLineNumber = 254;
        temp39.SourceFileName = "Pages/productCreate.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp39, temp_eb56.OnEvent);
        temp39.Bindings.Add(temp_eb56);
        temp14.SourceLineNumber = 254;
        temp14.SourceFileName = "Pages/productCreate.ux";
        temp40.SourceLineNumber = 261;
        temp40.SourceFileName = "Pages/productCreate.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb54);
        __g_nametable.Objects.Add(temp_eb56);
        this.Background = temp41;
        this.Children.Add(temp15);
        this.Children.Add(temp16);
        this.Children.Add(temp17);
        this.Children.Add(temp18);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Fieldvalue";
    static global::Uno.UX.Selector __selector2 = "BorderColor";
    static global::Uno.UX.Selector __selector3 = "Items";
    static global::Uno.UX.Selector __selector4 = "ScrollbarBox";
}
