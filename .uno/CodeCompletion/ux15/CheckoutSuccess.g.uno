[Uno.Compiler.UxGenerated]
public partial class CheckoutSuccess: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<Uno.UX.FileSource> temp1_File_inst;
    internal global::Fuse.Controls.Grid success;
    internal global::Fuse.Reactive.EventBinding temp_eb17;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "success",
        "temp_eb17"
    };
    static CheckoutSuccess()
    {
    }
    [global::Uno.UX.UXConstructor]
    public CheckoutSuccess(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Data("statusMsg");
        var temp1 = new global::Fuse.Controls.Image();
        temp1_File_inst = new FirstLoungeNewApp_FuseControlsImage_File_Property(temp1, __selector1);
        var temp3 = new global::Fuse.Reactive.Data("image");
        var temp4 = new global::Fuse.Reactive.Data("home");
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp7 = new global::Fuse.Controls.DockPanel();
        var temp8 = new global::Header1();
        var temp9 = new global::Fuse.Controls.StackPanel();
        success = new global::Fuse.Controls.Grid();
        var temp10 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp11 = new global::Fuse.Reactive.DataBinding(temp1_File_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp12 = new global::SubmitButton1();
        temp_eb17 = new global::Fuse.Reactive.EventBinding(temp4);
        var temp13 = new global::FooterNav();
        var temp14 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/checkoutsuccess.ux";
        temp5.LineNumber = 2;
        temp5.FileName = "Pages/checkoutsuccess.ux";
        temp5.SourceLineNumber = 2;
        temp5.SourceFileName = "Pages/checkoutsuccess.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp6.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar status = Observable();\n\t\tvar statusMsg = Observable();\n\t\tvar image = Observable();\n\t\tvar stresObj = this.Parameter.map(function(x) { return x.data; });\n\t\t\tstresObj.onValueChanged(module, function(param) {\n\t\t\t\n\t\t\tstatus.value = param;\n\t\t\tif (status.value == true) {\n\t\t\t\tstatusMsg.value = 'Successful!';\n\t\t\t\timage.value = '../Assets/success.png';\n\t\t\t}else{\n\t\t\t\tstatusMsg.value = 'Failed due to Network!';\n\t\t\t\timage.value = '../Assets/close.png';\n\t\t\t}\n\t\t\t\t\t\t\n\t\t});\n\n\t\tmodule.exports = {\n\t\t\tstatusMsg: statusMsg,\n\t\t\tstatus: status,\n\t\t\timage: image\n\t\t};\n\t";
        temp6.LineNumber = 5;
        temp6.FileName = "Pages/checkoutsuccess.ux";
        temp6.SourceLineNumber = 5;
        temp6.SourceFileName = "Pages/checkoutsuccess.ux";
        temp7.SourceLineNumber = 41;
        temp7.SourceFileName = "Pages/checkoutsuccess.ux";
        temp7.Children.Add(temp8);
        temp7.Children.Add(temp9);
        temp7.Children.Add(temp13);
        temp8.SourceLineNumber = 42;
        temp8.SourceFileName = "Pages/checkoutsuccess.ux";
        temp9.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp9.Alignment = Fuse.Elements.Alignment.Center;
        temp9.Margin = float4(10f, 10f, 10f, 10f);
        temp9.Padding = float4(0f, 20f, 0f, 20f);
        temp9.SourceLineNumber = 45;
        temp9.SourceFileName = "Pages/checkoutsuccess.ux";
        temp9.Children.Add(success);
        temp9.Children.Add(temp12);
        success.Opacity = 1f;
        success.Name = __selector2;
        success.SourceLineNumber = 46;
        success.SourceFileName = "Pages/checkoutsuccess.ux";
        success.Children.Add(temp);
        success.Children.Add(temp1);
        temp.TextAlignment = Fuse.Controls.TextAlignment.Center;
        temp.Margin = float4(0f, 20f, 0f, 20f);
        temp.SourceLineNumber = 47;
        temp.SourceFileName = "Pages/checkoutsuccess.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp10);
        temp2.SourceLineNumber = 47;
        temp2.SourceFileName = "Pages/checkoutsuccess.ux";
        temp1.Width = new Uno.UX.Size(120f, Uno.UX.Unit.Unspecified);
        temp1.SourceLineNumber = 49;
        temp1.SourceFileName = "Pages/checkoutsuccess.ux";
        temp1.Bindings.Add(temp11);
        temp3.SourceLineNumber = 49;
        temp3.SourceFileName = "Pages/checkoutsuccess.ux";
        temp12.Text = "Done";
        temp12.Alignment = Fuse.Elements.Alignment.Center;
        temp12.Margin = float4(0f, 20f, 0f, 20f);
        temp12.SourceLineNumber = 58;
        temp12.SourceFileName = "Pages/checkoutsuccess.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp12, temp_eb17.OnEvent);
        temp12.Bindings.Add(temp_eb17);
        temp4.SourceLineNumber = 58;
        temp4.SourceFileName = "Pages/checkoutsuccess.ux";
        temp13.SourceLineNumber = 73;
        temp13.SourceFileName = "Pages/checkoutsuccess.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(success);
        __g_nametable.Objects.Add(temp_eb17);
        this.Background = temp14;
        this.Children.Add(temp5);
        this.Children.Add(temp6);
        this.Children.Add(temp7);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "File";
    static global::Uno.UX.Selector __selector2 = "success";
}
