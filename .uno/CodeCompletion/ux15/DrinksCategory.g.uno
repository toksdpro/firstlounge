[Uno.Compiler.UxGenerated]
public partial class DrinksCategory: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly DrinksCategory __parent;
        [Uno.WeakReference] internal readonly DrinksCategory __parentInstance;
        public Template(DrinksCategory parent, DrinksCategory parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::SortedItem();
            __self.SourceLineNumber = 28;
            __self.SourceFileName = "Pages/drinkscategory.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<object> temp_Items_inst;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb21;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "ScrollbarBox",
        "temp_eb21"
    };
    static DrinksCategory()
    {
    }
    [global::Uno.UX.UXConstructor]
    public DrinksCategory(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("drinksList");
        var temp2 = new global::Fuse.Reactive.Data("goBack");
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Header();
        var temp8 = new global::Fuse.Controls.StackPanel();
        var temp9 = new global::OnlySearch();
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Controls.Text();
        var temp12 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp13 = new global::Fuse.Controls.StackPanel();
        var temp14 = new Template(this, this);
        var temp15 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp1, Fuse.Reactive.BindingMode.Default);
        var temp16 = new global::Fuse.Controls.Panel();
        var temp17 = new global::SubmitButton1();
        temp_eb21 = new global::Fuse.Reactive.EventBinding(temp2);
        var temp18 = new global::FooterNav();
        var temp19 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/drinkscategory.ux";
        temp3.LineNumber = 4;
        temp3.FileName = "Pages/drinkscategory.ux";
        temp3.SourceLineNumber = 4;
        temp3.SourceFileName = "Pages/drinkscategory.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../../fdb-all.js"));
        temp4.LineNumber = 5;
        temp4.FileName = "Pages/drinkscategory.ux";
        temp4.SourceLineNumber = 5;
        temp4.SourceFileName = "Pages/drinkscategory.ux";
        temp4.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/navigation.js"));
        temp5.LineNumber = 6;
        temp5.FileName = "Pages/drinkscategory.ux";
        temp5.SourceLineNumber = 6;
        temp5.SourceFileName = "Pages/drinkscategory.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../../Pages/drinks.js"));
        temp6.SourceLineNumber = 9;
        temp6.SourceFileName = "Pages/drinkscategory.ux";
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp6.Children.Add(temp12);
        temp6.Children.Add(temp18);
        temp7.SourceLineNumber = 10;
        temp7.SourceFileName = "Pages/drinkscategory.ux";
        temp8.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp8.SourceLineNumber = 13;
        temp8.SourceFileName = "Pages/drinkscategory.ux";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp9.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp9.SourceLineNumber = 15;
        temp9.SourceFileName = "Pages/drinkscategory.ux";
        temp10.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp10.Margin = float4(0f, 0f, 0f, 10f);
        temp10.SourceLineNumber = 17;
        temp10.SourceFileName = "Pages/drinkscategory.ux";
        temp10.Children.Add(temp11);
        temp11.Value = "Drinks And Cocktails";
        temp11.SourceLineNumber = 18;
        temp11.SourceFileName = "Pages/drinkscategory.ux";
        temp11.Font = global::LoginPage.Poppins;
        temp12.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp12.Alignment = Fuse.Elements.Alignment.Center;
        temp12.SourceLineNumber = 22;
        temp12.SourceFileName = "Pages/drinkscategory.ux";
        temp12.Children.Add(ScrollbarBox);
        temp12.Children.Add(temp16);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(350f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector1;
        ScrollbarBox.SourceLineNumber = 23;
        ScrollbarBox.SourceFileName = "Pages/drinkscategory.ux";
        ScrollbarBox.Children.Add(temp13);
        temp13.Margin = float4(0f, 60f, 0f, 0f);
        temp13.SourceLineNumber = 25;
        temp13.SourceFileName = "Pages/drinkscategory.ux";
        temp13.Children.Add(temp);
        temp.SourceLineNumber = 27;
        temp.SourceFileName = "Pages/drinkscategory.ux";
        temp.Templates.Add(temp14);
        temp.Bindings.Add(temp15);
        temp1.SourceLineNumber = 27;
        temp1.SourceFileName = "Pages/drinkscategory.ux";
        temp16.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp16.Margin = float4(0f, 10f, 0f, 0f);
        temp16.Padding = float4(5f, 5f, 5f, 5f);
        temp16.SourceLineNumber = 35;
        temp16.SourceFileName = "Pages/drinkscategory.ux";
        temp16.Children.Add(temp17);
        temp17.Text = "Back";
        temp17.Alignment = Fuse.Elements.Alignment.Center;
        temp17.SourceLineNumber = 36;
        temp17.SourceFileName = "Pages/drinkscategory.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp17, temp_eb21.OnEvent);
        temp17.Bindings.Add(temp_eb21);
        temp2.SourceLineNumber = 36;
        temp2.SourceFileName = "Pages/drinkscategory.ux";
        temp18.SourceLineNumber = 42;
        temp18.SourceFileName = "Pages/drinkscategory.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb21);
        this.Background = temp19;
        this.Children.Add(temp3);
        this.Children.Add(temp4);
        this.Children.Add(temp5);
        this.Children.Add(temp6);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
    static global::Uno.UX.Selector __selector1 = "ScrollbarBox";
}
