[Uno.Compiler.UxGenerated]
public partial class TopHeader: Fuse.Controls.Panel
{
    global::Uno.UX.Property<string> temp_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb0;
    static TopHeader()
    {
    }
    [global::Uno.UX.UXConstructor]
    public TopHeader()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp1 = new global::Fuse.Reactive.Data("quantityTotal");
        var temp2 = new global::Fuse.Reactive.Data("checkout");
        var temp3 = new global::Fuse.Controls.Image();
        var temp4 = new global::Fuse.Controls.Circle();
        var temp5 = new global::Fuse.Drawing.Stroke();
        var temp6 = new global::Fuse.Drawing.StaticSolidColor(float4(0.7333333f, 0.5490196f, 0.007843138f, 1f));
        var temp7 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp1, Fuse.Reactive.BindingMode.Default);
        temp_eb0 = new global::Fuse.Reactive.EventBinding(temp2);
        this.SourceLineNumber = 156;
        this.SourceFileName = "Pages/categoryProducts.ux";
        temp3.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        temp3.Width = new Uno.UX.Size(28f, Uno.UX.Unit.Unspecified);
        temp3.Alignment = Fuse.Elements.Alignment.Right;
        temp3.Margin = float4(20f, 0f, 20f, 0f);
        temp3.SourceLineNumber = 157;
        temp3.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp3, temp_eb0.OnEvent);
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../Assets/cart.png"));
        temp3.Children.Add(temp4);
        temp3.Bindings.Add(temp_eb0);
        temp4.Color = float4(1f, 1f, 1f, 1f);
        temp4.Width = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
        temp4.Height = new Uno.UX.Size(15f, Uno.UX.Unit.Unspecified);
        temp4.Offset = new Uno.UX.Size2(new Uno.UX.Size(10f, Uno.UX.Unit.Unspecified), new Uno.UX.Size(-8f, Uno.UX.Unit.Unspecified));
        temp4.SourceLineNumber = 158;
        temp4.SourceFileName = "Pages/categoryProducts.ux";
        temp4.Strokes.Add(temp5);
        temp4.Children.Add(temp);
        temp5.Color = float4(0f, 1f, 0f, 1f);
        temp5.Width = 1f;
        temp5.Brush = temp6;
        temp.FontSize = 11f;
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 160;
        temp.SourceFileName = "Pages/categoryProducts.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp7);
        temp1.SourceLineNumber = 160;
        temp1.SourceFileName = "Pages/categoryProducts.ux";
        temp2.SourceLineNumber = 157;
        temp2.SourceFileName = "Pages/categoryProducts.ux";
        this.Children.Add(temp3);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
