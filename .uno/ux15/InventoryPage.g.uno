[Uno.Compiler.UxGenerated]
public partial class InventoryPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly InventoryPage __parent;
        [Uno.WeakReference] internal readonly InventoryPage __parentInstance;
        public Template(InventoryPage parent, InventoryPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::Product();
            __self.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
            __self.SourceLineNumber = 232;
            __self.SourceFileName = "Pages/inventory.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<object> temp_Items_inst;
    global::Uno.UX.Property<float> inventorypage_Opacity_inst;
    global::Uno.UX.Property<float> iloading_Opacity_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    global::Uno.UX.Property<bool> temp2_Value_inst;
    global::Uno.UX.Property<float> idialog_Opacity_inst;
    global::Uno.UX.Property<float> imainPanelBlur_Radius_inst;
    global::Uno.UX.Property<bool> temp3_Value_inst;
    global::Uno.UX.Property<string> idialogMsg_Value_inst;
    global::Uno.UX.Property<bool> temp4_Value_inst;
    internal global::Fuse.Controls.Panel inventorypage;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Effects.Blur imainPanelBlur;
    internal global::Fuse.Controls.Panel iloading;
    internal global::Fuse.Controls.Panel idialog;
    internal global::Fuse.Controls.Text idialogMsg;
    internal global::Fuse.Reactive.EventBinding temp_eb42;
    internal global::Fuse.Reactive.EventBinding temp_eb43;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "inventorypage",
        "ScrollbarBox",
        "imainPanelBlur",
        "iloading",
        "idialog",
        "idialogMsg",
        "temp_eb42",
        "temp_eb43"
    };
    static InventoryPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public InventoryPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("products");
        inventorypage = new global::Fuse.Controls.Panel();
        inventorypage_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(inventorypage, __selector1);
        iloading = new global::Fuse.Controls.Panel();
        iloading_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(iloading, __selector1);
        var temp1 = new global::Fuse.Triggers.WhileTrue();
        temp1_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp1, __selector2);
        var temp6 = new global::Fuse.Reactive.Data("visible");
        var temp2 = new global::Fuse.Triggers.WhileFalse();
        temp2_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp2, __selector2);
        var temp7 = new global::Fuse.Reactive.Data("visible");
        idialog = new global::Fuse.Controls.Panel();
        idialog_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(idialog, __selector1);
        imainPanelBlur = new global::Fuse.Effects.Blur();
        imainPanelBlur_Radius_inst = new FirstLoungeNewApp_FuseEffectsBlur_Radius_Property(imainPanelBlur, __selector3);
        var temp3 = new global::Fuse.Triggers.WhileTrue();
        temp3_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp3, __selector2);
        var temp8 = new global::Fuse.Reactive.Data("isDialogShowing");
        idialogMsg = new global::Fuse.Controls.Text();
        idialogMsg_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(idialogMsg, __selector2);
        var temp9 = new global::Fuse.Reactive.Data("dialogMsg");
        var temp10 = new global::Fuse.Reactive.Data("retry");
        var temp11 = new global::Fuse.Reactive.Data("closeDialog");
        var temp4 = new global::Fuse.Triggers.WhileFalse();
        temp4_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp4, __selector2);
        var temp12 = new global::Fuse.Reactive.Data("isDialogShowing");
        var temp13 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp14 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp15 = new global::Fuse.Controls.DockPanel();
        var temp16 = new global::Header();
        var temp17 = new global::Fuse.Controls.StackPanel();
        var temp18 = new global::OnlySearch();
        var temp19 = new global::Fuse.Controls.DockPanel();
        var temp20 = new global::Fuse.Controls.Text();
        var temp21 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp22 = new global::Fuse.Controls.StackPanel();
        var temp23 = new Template(this, this);
        var temp24 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp25 = new global::Loader();
        var temp26 = new global::Fuse.Animations.Change<float>(inventorypage_Opacity_inst);
        var temp27 = new global::Fuse.Animations.Change<float>(iloading_Opacity_inst);
        var temp28 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        var temp29 = new global::Fuse.Animations.Change<float>(inventorypage_Opacity_inst);
        var temp30 = new global::Fuse.Animations.Change<float>(iloading_Opacity_inst);
        var temp31 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp32 = new global::Fuse.Animations.Change<float>(idialog_Opacity_inst);
        var temp33 = new global::Fuse.Animations.Change<float>(imainPanelBlur_Radius_inst);
        var temp34 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp35 = new global::Fuse.Controls.Rectangle();
        var temp36 = new global::Fuse.Drawing.StaticSolidColor(float4(1f, 1f, 1f, 1f));
        var temp37 = new global::Fuse.Controls.StackPanel();
        var temp38 = new global::Fuse.Reactive.DataBinding(idialogMsg_Value_inst, temp9, Fuse.Reactive.BindingMode.Default);
        var temp39 = new global::SubmitButton1();
        temp_eb42 = new global::Fuse.Reactive.EventBinding(temp10);
        var temp40 = new global::SubmitButton1();
        temp_eb43 = new global::Fuse.Reactive.EventBinding(temp11);
        var temp41 = new global::Fuse.Animations.Move();
        var temp42 = new global::Fuse.Animations.Scale();
        var temp43 = new global::Fuse.Reactive.DataBinding(temp4_Value_inst, temp12, Fuse.Reactive.BindingMode.Default);
        var temp44 = new global::Fuse.Effects.DropShadow();
        var temp45 = new global::FooterNav();
        var temp46 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/inventory.ux";
        temp13.LineNumber = 4;
        temp13.FileName = "Pages/inventory.ux";
        temp13.SourceLineNumber = 4;
        temp13.SourceFileName = "Pages/inventory.ux";
        temp13.File = new global::Uno.UX.BundleFileSource(import("../../Pages/navigation.js"));
        temp14.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tvar login = require('Pages/login.js');\n\t\tvar api = require('Pages/api.js');\n\n\t\tvar productsList = Observable();\n\n\t\tvar visible = Observable(true);\n\t\t\n\t\t// modal\n\t\tvar isDialogShowing = Observable(false);\n\t\tvar dialogMsg = Observable();\n\n\t\tfunction closeDialog() {\n\t\t\tisDialogShowing.value = false;\n\t\t}\n\n\t\tfunction retry() {\n\t\t\tLoadAllProducts();\n\t\t}\n\n\t\tfunction LoadAllProducts() {\n\n\t\t\t// console.log(\"jsdhsjd\");\n\t\t\tlogin.getToken()\n\t\t\t.then(function(contents) {\n\t\t\t    console.log('token: '+contents);\n\t\t\t\tif (contents != null) {\n\t\t\t\t\tGetProducts(contents);\n\t\t\t\t}else{\n\t\t\t\t\tconsole.log(\"Error not logged in\");\n\t\t\t\t}\n\t\t\t}, function(error) {\n\t\t\t\tconsole.log(error);\n\t\t\t});\n\n\t\t\tfunction GetProducts(token) {\n\t\t\t\tapi.ApiProducts(token)\n\t\t\t\t.then(function(response) {\n\t\t\t\t\tvar ok = response.ok;\n\t\t\t\t\tvar status = response.status;\n\t\t\t\t\tvar response_msg = JSON.parse(response._bodyText);\n\n\t\t\t\t\t// hide loader\n\t\t\t\t\tvisible.value = false;\n\n\t\t\t\t\tif (ok) {\n\t\t\t\t\t\tisDialogShowing.value = false;\n\t\t\t\t\t\tconsole.log(\"api: \"+JSON.stringify(JSON.parse(response._bodyText).products));\n\t\t\t\t\t\tproductsList.replaceAll(JSON.parse(response._bodyText).products);\n\t\t\t\t\t}else{\n\t\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\t\tif (status == 502) {\n\t\t\t\t\t\t\tconsole.log(\"Network Error\");\n\t\t\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\t\t}else {\n\t\t\t\t\t\t\tconsole.log(status);\n\t\t\t\t\t\t\tdialogMsg.value = \"There was an error\";\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t}).catch(function(err) {\n\t\t\t\t\tvisible.value = false;\n\n\t\t\t\t\tisDialogShowing.value = true;\n\t\t\t\t\tdialogMsg.value = \"Network Error\";\n\t\t\t\t\tconsole.log(\"There was an error\");\n\t\t\t\t});\n\t\t\t}\n\t\t\t// return productsList;\n\t\t}\n\n\t\t// LoadAllProducts();\n\n\t\t// Backend.productList\n\t\t\n\t\tfunction InventoryView(args){\n\t\t\t// console.log(JSON.stringify(args.data));\n\t\t\tvar param = JSON.stringify(args.data);\n\t\t\trouter.push(\"inventoryview\", {data: param});\n\t\t}\n\n\t\tLoadAllProducts()\n\n\t\t// setInterval(function(){ LoadAllProducts(); }, 3000);\n\t\t\n\t\tmodule.exports = {\n\t\t\tproducts: productsList,\n\t\t\tInventoryView: InventoryView,\n\t\t\tdialogMsg: dialogMsg,\n\n\t\t\tisDialogShowing: isDialogShowing,\n\n\t\t\tvisible: visible,\n\n\t\t\tcloseDialog: closeDialog,\n\n\t\t\tretry: retry\n\t\t};\n\t";
        temp14.LineNumber = 7;
        temp14.FileName = "Pages/inventory.ux";
        temp14.SourceLineNumber = 7;
        temp14.SourceFileName = "Pages/inventory.ux";
        temp15.SourceLineNumber = 210;
        temp15.SourceFileName = "Pages/inventory.ux";
        temp15.Children.Add(temp16);
        temp15.Children.Add(inventorypage);
        temp15.Children.Add(iloading);
        temp15.Children.Add(temp3);
        temp15.Children.Add(idialog);
        temp15.Children.Add(temp45);
        temp16.SourceLineNumber = 212;
        temp16.SourceFileName = "Pages/inventory.ux";
        inventorypage.Name = __selector4;
        inventorypage.SourceLineNumber = 215;
        inventorypage.SourceFileName = "Pages/inventory.ux";
        inventorypage.Children.Add(temp17);
        inventorypage.Children.Add(temp21);
        inventorypage.Children.Add(imainPanelBlur);
        temp17.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp17.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp17.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp17.SourceLineNumber = 216;
        temp17.SourceFileName = "Pages/inventory.ux";
        temp17.Children.Add(temp18);
        temp17.Children.Add(temp19);
        temp18.SourceLineNumber = 217;
        temp18.SourceFileName = "Pages/inventory.ux";
        temp19.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp19.Margin = float4(0f, 0f, 0f, 10f);
        temp19.SourceLineNumber = 220;
        temp19.SourceFileName = "Pages/inventory.ux";
        temp19.Children.Add(temp20);
        temp20.Value = "Inventory";
        temp20.SourceLineNumber = 221;
        temp20.SourceFileName = "Pages/inventory.ux";
        temp20.Font = global::LoginPage.Poppins;
        temp21.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp21.Alignment = Fuse.Elements.Alignment.Center;
        temp21.SourceLineNumber = 225;
        temp21.SourceFileName = "Pages/inventory.ux";
        temp21.Children.Add(ScrollbarBox);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(350f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector5;
        ScrollbarBox.SourceLineNumber = 226;
        ScrollbarBox.SourceFileName = "Pages/inventory.ux";
        ScrollbarBox.Children.Add(temp22);
        temp22.Margin = float4(0f, 60f, 0f, 0f);
        temp22.SourceLineNumber = 229;
        temp22.SourceFileName = "Pages/inventory.ux";
        temp22.Children.Add(temp);
        temp.SourceLineNumber = 231;
        temp.SourceFileName = "Pages/inventory.ux";
        temp.Templates.Add(temp23);
        temp.Bindings.Add(temp24);
        temp5.SourceLineNumber = 231;
        temp5.SourceFileName = "Pages/inventory.ux";
        imainPanelBlur.Radius = 0f;
        imainPanelBlur.Name = __selector6;
        imainPanelBlur.SourceLineNumber = 243;
        imainPanelBlur.SourceFileName = "Pages/inventory.ux";
        iloading.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        iloading.Height = new Uno.UX.Size(65f, Uno.UX.Unit.Percent);
        iloading.Opacity = 1f;
        iloading.Name = __selector7;
        iloading.SourceLineNumber = 247;
        iloading.SourceFileName = "Pages/inventory.ux";
        iloading.Children.Add(temp25);
        iloading.Children.Add(temp1);
        iloading.Children.Add(temp2);
        temp25.Lcolor = float4(0f, 0f, 0f, 1f);
        temp25.SourceLineNumber = 248;
        temp25.SourceFileName = "Pages/inventory.ux";
        temp1.SourceLineNumber = 250;
        temp1.SourceFileName = "Pages/inventory.ux";
        temp1.Animators.Add(temp26);
        temp1.Animators.Add(temp27);
        temp1.Bindings.Add(temp28);
        temp26.Value = 0f;
        temp27.Value = 1f;
        temp6.SourceLineNumber = 250;
        temp6.SourceFileName = "Pages/inventory.ux";
        temp2.SourceLineNumber = 255;
        temp2.SourceFileName = "Pages/inventory.ux";
        temp2.Animators.Add(temp29);
        temp2.Animators.Add(temp30);
        temp2.Bindings.Add(temp31);
        temp29.Value = 1f;
        temp30.Value = 0f;
        temp7.SourceLineNumber = 255;
        temp7.SourceFileName = "Pages/inventory.ux";
        temp3.SourceLineNumber = 261;
        temp3.SourceFileName = "Pages/inventory.ux";
        temp3.Animators.Add(temp32);
        temp3.Animators.Add(temp33);
        temp3.Bindings.Add(temp34);
        temp32.Value = 1f;
        temp32.Duration = 0.3;
        temp32.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp33.Value = 10f;
        temp33.Duration = 0.3;
        temp33.Easing = Fuse.Animations.Easing.QuadraticInOut;
        temp8.SourceLineNumber = 261;
        temp8.SourceFileName = "Pages/inventory.ux";
        idialog.Width = new Uno.UX.Size(300f, Uno.UX.Unit.Unspecified);
        idialog.Alignment = Fuse.Elements.Alignment.VerticalCenter;
        idialog.Padding = float4(20f, 40f, 20f, 0f);
        idialog.Opacity = 0f;
        idialog.ZOffset = 1f;
        idialog.Name = __selector8;
        idialog.SourceLineNumber = 266;
        idialog.SourceFileName = "Pages/inventory.ux";
        idialog.Children.Add(temp35);
        idialog.Children.Add(temp37);
        idialog.Children.Add(temp4);
        idialog.Children.Add(temp44);
        temp35.CornerRadius = float4(2f, 2f, 2f, 2f);
        temp35.Opacity = 0.8f;
        temp35.Layer = Fuse.Layer.Background;
        temp35.SourceLineNumber = 267;
        temp35.SourceFileName = "Pages/inventory.ux";
        temp35.Fill = temp36;
        temp37.SourceLineNumber = 268;
        temp37.SourceFileName = "Pages/inventory.ux";
        temp37.Children.Add(idialogMsg);
        temp37.Children.Add(temp39);
        temp37.Children.Add(temp40);
        idialogMsg.TextAlignment = Fuse.Controls.TextAlignment.Center;
        idialogMsg.Alignment = Fuse.Elements.Alignment.Center;
        idialogMsg.Name = __selector9;
        idialogMsg.SourceLineNumber = 270;
        idialogMsg.SourceFileName = "Pages/inventory.ux";
        idialogMsg.Font = global::LoginPage.Poppins;
        idialogMsg.Bindings.Add(temp38);
        temp9.SourceLineNumber = 270;
        temp9.SourceFileName = "Pages/inventory.ux";
        temp39.Text = "Retry";
        temp39.Alignment = Fuse.Elements.Alignment.Center;
        temp39.Margin = float4(0f, 10f, 0f, 0f);
        temp39.SourceLineNumber = 271;
        temp39.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp39, temp_eb42.OnEvent);
        temp39.Bindings.Add(temp_eb42);
        temp10.SourceLineNumber = 271;
        temp10.SourceFileName = "Pages/inventory.ux";
        temp40.Text = "close";
        temp40.Alignment = Fuse.Elements.Alignment.Center;
        temp40.Margin = float4(0f, 10f, 0f, 30f);
        temp40.SourceLineNumber = 272;
        temp40.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp40, temp_eb43.OnEvent);
        temp40.Bindings.Add(temp_eb43);
        temp11.SourceLineNumber = 272;
        temp11.SourceFileName = "Pages/inventory.ux";
        temp4.SourceLineNumber = 275;
        temp4.SourceFileName = "Pages/inventory.ux";
        temp4.Animators.Add(temp41);
        temp4.Animators.Add(temp42);
        temp4.Bindings.Add(temp43);
        temp41.X = 3f;
        temp41.Duration = 0.6;
        temp41.RelativeTo = Fuse.TranslationModes.Size;
        temp41.Easing = Fuse.Animations.Easing.BackIn;
        temp42.Factor = 0.8f;
        temp42.Duration = 0.6;
        temp42.Easing = Fuse.Animations.Easing.BackIn;
        temp12.SourceLineNumber = 275;
        temp12.SourceFileName = "Pages/inventory.ux";
        temp44.SourceLineNumber = 280;
        temp44.SourceFileName = "Pages/inventory.ux";
        temp45.SourceLineNumber = 284;
        temp45.SourceFileName = "Pages/inventory.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(inventorypage);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(imainPanelBlur);
        __g_nametable.Objects.Add(iloading);
        __g_nametable.Objects.Add(idialog);
        __g_nametable.Objects.Add(idialogMsg);
        __g_nametable.Objects.Add(temp_eb42);
        __g_nametable.Objects.Add(temp_eb43);
        this.Background = temp46;
        this.Children.Add(temp13);
        this.Children.Add(temp14);
        this.Children.Add(temp15);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
    static global::Uno.UX.Selector __selector1 = "Opacity";
    static global::Uno.UX.Selector __selector2 = "Value";
    static global::Uno.UX.Selector __selector3 = "Radius";
    static global::Uno.UX.Selector __selector4 = "inventorypage";
    static global::Uno.UX.Selector __selector5 = "ScrollbarBox";
    static global::Uno.UX.Selector __selector6 = "imainPanelBlur";
    static global::Uno.UX.Selector __selector7 = "iloading";
    static global::Uno.UX.Selector __selector8 = "idialog";
    static global::Uno.UX.Selector __selector9 = "idialogMsg";
}
