[Uno.Compiler.UxGenerated]
public partial class Settings: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb63;
    internal global::Fuse.Reactive.EventBinding temp_eb64;
    internal global::Fuse.Reactive.EventBinding temp_eb65;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "ScrollbarBox",
        "temp_eb63",
        "temp_eb64",
        "temp_eb65"
    };
    static Settings()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Settings(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp = new global::Fuse.Reactive.Data("productsettings");
        var temp1 = new global::Fuse.Reactive.Data("defaultcurrency");
        var temp2 = new global::Fuse.Reactive.Data("goBack");
        var temp3 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp4 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Controls.DockPanel();
        var temp7 = new global::Header();
        var temp8 = new global::Fuse.Controls.StackPanel();
        var temp9 = new global::OnlySearch();
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Controls.Text();
        var temp12 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp13 = new global::Fuse.Controls.StackPanel();
        var temp14 = new global::Item();
        temp_eb63 = new global::Fuse.Reactive.EventBinding(temp);
        var temp15 = new global::Item();
        temp_eb64 = new global::Fuse.Reactive.EventBinding(temp1);
        var temp16 = new global::Fuse.Controls.Panel();
        var temp17 = new global::SubmitButton1();
        temp_eb65 = new global::Fuse.Reactive.EventBinding(temp2);
        var temp18 = new global::FooterNav();
        var temp19 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/settings.ux";
        temp3.LineNumber = 2;
        temp3.FileName = "Pages/settings.ux";
        temp3.SourceLineNumber = 2;
        temp3.SourceFileName = "Pages/settings.ux";
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../fdb-all.js"));
        temp4.LineNumber = 3;
        temp4.FileName = "Pages/settings.ux";
        temp4.SourceLineNumber = 3;
        temp4.SourceFileName = "Pages/settings.ux";
        temp4.File = new global::Uno.UX.BundleFileSource(import("../../Pages/navigation.js"));
        temp5.Code = "\n\t\tvar Observable = require(\"FuseJS/Observable\");\n\t\tfunction productsettings() {\n\t\t\trouter.push(\"productsettings\");\n\t\t}\n\t\tfunction defaultcurrency() {\n\t\t\trouter.push(\"defaultcurrency\");\n\t\t}\n\t\tmodule.exports = {\n\t\t\tproductsettings: productsettings,\n\t\t\tdefaultcurrency: defaultcurrency\n\t\t};\n\t";
        temp5.LineNumber = 4;
        temp5.FileName = "Pages/settings.ux";
        temp5.SourceLineNumber = 4;
        temp5.SourceFileName = "Pages/settings.ux";
        temp6.SourceLineNumber = 42;
        temp6.SourceFileName = "Pages/settings.ux";
        temp6.Children.Add(temp7);
        temp6.Children.Add(temp8);
        temp6.Children.Add(temp12);
        temp6.Children.Add(temp18);
        temp7.SourceLineNumber = 43;
        temp7.SourceFileName = "Pages/settings.ux";
        temp8.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp8.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp8.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp8.SourceLineNumber = 46;
        temp8.SourceFileName = "Pages/settings.ux";
        temp8.Children.Add(temp9);
        temp8.Children.Add(temp10);
        temp9.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp9.SourceLineNumber = 48;
        temp9.SourceFileName = "Pages/settings.ux";
        temp10.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp10.Margin = float4(0f, 0f, 0f, 10f);
        temp10.SourceLineNumber = 50;
        temp10.SourceFileName = "Pages/settings.ux";
        temp10.Children.Add(temp11);
        temp11.Value = "Settings";
        temp11.SourceLineNumber = 51;
        temp11.SourceFileName = "Pages/settings.ux";
        temp11.Font = global::LoginPage.Poppins;
        temp12.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp12.Alignment = Fuse.Elements.Alignment.Center;
        temp12.SourceLineNumber = 56;
        temp12.SourceFileName = "Pages/settings.ux";
        temp12.Children.Add(ScrollbarBox);
        temp12.Children.Add(temp16);
        ScrollbarBox.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        ScrollbarBox.Height = new Uno.UX.Size(350f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector0;
        ScrollbarBox.SourceLineNumber = 57;
        ScrollbarBox.SourceFileName = "Pages/settings.ux";
        ScrollbarBox.Children.Add(temp13);
        temp13.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp13.Margin = float4(0f, 70f, 0f, 0f);
        temp13.SourceLineNumber = 59;
        temp13.SourceFileName = "Pages/settings.ux";
        temp13.Children.Add(temp14);
        temp13.Children.Add(temp15);
        temp14.title = "Product Settings";
        temp14.SourceLineNumber = 61;
        temp14.SourceFileName = "Pages/settings.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp14, temp_eb63.OnEvent);
        temp14.Bindings.Add(temp_eb63);
        temp.SourceLineNumber = 61;
        temp.SourceFileName = "Pages/settings.ux";
        temp15.title = "Default Currency";
        temp15.SourceLineNumber = 62;
        temp15.SourceFileName = "Pages/settings.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp15, temp_eb64.OnEvent);
        temp15.Bindings.Add(temp_eb64);
        temp1.SourceLineNumber = 62;
        temp1.SourceFileName = "Pages/settings.ux";
        temp16.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp16.Margin = float4(0f, 10f, 0f, 0f);
        temp16.Padding = float4(5f, 5f, 5f, 5f);
        temp16.SourceLineNumber = 68;
        temp16.SourceFileName = "Pages/settings.ux";
        temp16.Children.Add(temp17);
        temp17.Text = "Back";
        temp17.Alignment = Fuse.Elements.Alignment.Center;
        temp17.SourceLineNumber = 69;
        temp17.SourceFileName = "Pages/settings.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp17, temp_eb65.OnEvent);
        temp17.Bindings.Add(temp_eb65);
        temp2.SourceLineNumber = 69;
        temp2.SourceFileName = "Pages/settings.ux";
        temp18.SourceLineNumber = 76;
        temp18.SourceFileName = "Pages/settings.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb63);
        __g_nametable.Objects.Add(temp_eb64);
        __g_nametable.Objects.Add(temp_eb65);
        this.Background = temp19;
        this.Children.Add(temp3);
        this.Children.Add(temp4);
        this.Children.Add(temp5);
        this.Children.Add(temp6);
    }
    static global::Uno.UX.Selector __selector0 = "ScrollbarBox";
}
