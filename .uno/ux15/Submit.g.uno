[Uno.Compiler.UxGenerated]
public partial class Submit: Fuse.Controls.Button
{
    string _field_Texts;
    [global::Uno.UX.UXOriginSetter("SetTexts")]
    public string Texts
    {
        get { return _field_Texts; }
        set { SetTexts(value, null); }
    }
    public void SetTexts(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Texts)
        {
            _field_Texts = value;
            OnPropertyChanged("Texts", origin);
        }
    }
    global::Uno.UX.Property<string> sub_Value_inst;
    global::Uno.UX.Property<float> sub_Opacity_inst;
    global::Uno.UX.Property<float> loading_Opacity_inst;
    global::Uno.UX.Property<bool> temp_Value_inst;
    global::Uno.UX.Property<bool> temp1_Value_inst;
    internal global::Fuse.Controls.Text sub;
    internal global::Loader loading;
    static Submit()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Submit()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        sub = new global::Fuse.Controls.Text();
        sub_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(sub, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, FirstLoungeNewApp_accessor_Submit_Texts.Singleton);
        sub_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(sub, __selector1);
        loading = new global::Loader();
        loading_Opacity_inst = new FirstLoungeNewApp_FuseElementsElement_Opacity_Property(loading, __selector1);
        var temp = new global::Fuse.Triggers.WhileTrue();
        temp_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp, __selector0);
        var temp4 = new global::Fuse.Reactive.Data("visible");
        var temp1 = new global::Fuse.Triggers.WhileFalse();
        temp1_Value_inst = new FirstLoungeNewApp_FuseTriggersWhileBool_Value_Property(temp1, __selector0);
        var temp5 = new global::Fuse.Reactive.Data("visible");
        var temp6 = new global::Fuse.Reactive.DataBinding(sub_Value_inst, temp3, Fuse.Reactive.BindingMode.Read);
        var temp7 = new global::Fuse.Animations.Change<float>(sub_Opacity_inst);
        var temp8 = new global::Fuse.Animations.Change<float>(loading_Opacity_inst);
        var temp9 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp10 = new global::Fuse.Animations.Change<float>(sub_Opacity_inst);
        var temp11 = new global::Fuse.Animations.Change<float>(loading_Opacity_inst);
        var temp12 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        var temp13 = new global::Fuse.Controls.Rectangle();
        this.Width = new Uno.UX.Size(80f, Uno.UX.Unit.Percent);
        this.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        this.SourceLineNumber = 146;
        this.SourceFileName = "Pages/login.ux";
        sub.TextAlignment = Fuse.Controls.TextAlignment.Center;
        sub.Color = float4(1f, 1f, 1f, 1f);
        sub.Alignment = Fuse.Elements.Alignment.Center;
        sub.Opacity = 1f;
        sub.Name = __selector2;
        sub.SourceLineNumber = 148;
        sub.SourceFileName = "Pages/login.ux";
        sub.Font = global::LoginPage.Poppins;
        sub.Bindings.Add(temp6);
        temp3.SourceLineNumber = 148;
        temp3.SourceFileName = "Pages/login.ux";
        temp2.SourceLineNumber = 148;
        temp2.SourceFileName = "Pages/login.ux";
        loading.Lcolor = float4(0.8823529f, 0.7647059f, 0.1490196f, 1f);
        loading.Opacity = 0f;
        loading.Name = __selector3;
        loading.SourceLineNumber = 149;
        loading.SourceFileName = "Pages/login.ux";
        temp.SourceLineNumber = 151;
        temp.SourceFileName = "Pages/login.ux";
        temp.Animators.Add(temp7);
        temp.Animators.Add(temp8);
        temp.Bindings.Add(temp9);
        temp7.Value = 0f;
        temp8.Value = 1f;
        temp4.SourceLineNumber = 151;
        temp4.SourceFileName = "Pages/login.ux";
        temp1.SourceLineNumber = 156;
        temp1.SourceFileName = "Pages/login.ux";
        temp1.Animators.Add(temp10);
        temp1.Animators.Add(temp11);
        temp1.Bindings.Add(temp12);
        temp10.Value = 1f;
        temp11.Value = 0f;
        temp5.SourceLineNumber = 156;
        temp5.SourceFileName = "Pages/login.ux";
        temp13.CornerRadius = float4(3f, 3f, 3f, 3f);
        temp13.Color = float4(0.1647059f, 0.2313726f, 0.4117647f, 1f);
        temp13.Layer = Fuse.Layer.Background;
        temp13.SourceLineNumber = 161;
        temp13.SourceFileName = "Pages/login.ux";
        this.Children.Add(sub);
        this.Children.Add(loading);
        this.Children.Add(temp);
        this.Children.Add(temp1);
        this.Children.Add(temp13);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
    static global::Uno.UX.Selector __selector1 = "Opacity";
    static global::Uno.UX.Selector __selector2 = "sub";
    static global::Uno.UX.Selector __selector3 = "loading";
}
