[Uno.Compiler.UxGenerated]
public partial class Header1: Fuse.Controls.Panel
{
    internal global::Fuse.Reactive.EventBinding temp_eb16;
    static Header1()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Header1()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.Data("home");
        var temp1 = new global::Fuse.Controls.DockPanel();
        var temp2 = new global::BackIcon();
        temp_eb16 = new global::Fuse.Reactive.EventBinding(temp);
        var temp3 = new global::TopHeader();
        var temp4 = new global::Fuse.Controls.Text();
        var temp5 = new global::Fuse.Controls.Rectangle();
        var temp6 = new global::Fuse.Drawing.Stroke();
        var temp7 = new global::Fuse.Drawing.StaticSolidColor(float4(0.7333333f, 0.5490196f, 0.007843138f, 1f));
        this.Color = float4(1f, 1f, 1f, 1f);
        this.SourceLineNumber = 30;
        this.SourceFileName = "Pages/checkoutsuccess.ux";
        global::Fuse.Controls.DockPanel.SetDock(this, Fuse.Layouts.Dock.Top);
        temp1.Padding = float4(5f, 5f, 5f, 5f);
        temp1.SourceLineNumber = 31;
        temp1.SourceFileName = "Pages/checkoutsuccess.ux";
        temp1.Children.Add(temp2);
        temp1.Children.Add(temp3);
        temp1.Children.Add(temp4);
        temp2.Alignment = Fuse.Elements.Alignment.Left;
        temp2.Margin = float4(10f, 0f, 10f, 0f);
        temp2.SourceLineNumber = 32;
        temp2.SourceFileName = "Pages/checkoutsuccess.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp2, temp_eb16.OnEvent);
        temp2.Bindings.Add(temp_eb16);
        temp.SourceLineNumber = 32;
        temp.SourceFileName = "Pages/checkoutsuccess.ux";
        temp3.SourceLineNumber = 33;
        temp3.SourceFileName = "Pages/checkoutsuccess.ux";
        temp4.Value = "FIRST LOUNGE";
        temp4.FontSize = 14f;
        temp4.Color = float4(0.1333333f, 0.2196078f, 0.4f, 1f);
        temp4.Alignment = Fuse.Elements.Alignment.Center;
        temp4.SourceLineNumber = 34;
        temp4.SourceFileName = "Pages/checkoutsuccess.ux";
        temp4.Font = global::LoginPage.Poppins;
        temp5.MaxHeight = new Uno.UX.Size(1f, Uno.UX.Unit.Unspecified);
        temp5.Offset = new Uno.UX.Size2(new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified), new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified));
        temp5.Layer = Fuse.Layer.Background;
        temp5.SourceLineNumber = 37;
        temp5.SourceFileName = "Pages/checkoutsuccess.ux";
        temp5.Strokes.Add(temp6);
        temp6.Color = float4(0f, 1f, 0f, 1f);
        temp6.Width = 1f;
        temp6.Brush = temp7;
        this.Children.Add(temp1);
        this.Children.Add(temp5);
    }
}
