[Uno.Compiler.UxGenerated]
public partial class HomeIcons: Fuse.Controls.StackPanel
{
    string _field_Image;
    [global::Uno.UX.UXOriginSetter("SetImage")]
    public string Image
    {
        get { return _field_Image; }
        set { SetImage(value, null); }
    }
    public void SetImage(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Image)
        {
            _field_Image = value;
            OnPropertyChanged("Image", origin);
        }
    }
    string _field_name;
    [global::Uno.UX.UXOriginSetter("Setname")]
    public string name
    {
        get { return _field_name; }
        set { Setname(value, null); }
    }
    public void Setname(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_name)
        {
            _field_name = value;
            OnPropertyChanged("name", origin);
        }
    }
    global::Uno.UX.Property<string> temp_Url_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    static HomeIcons()
    {
    }
    [global::Uno.UX.UXConstructor]
    public HomeIcons()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp2 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Controls.Image();
        temp_Url_inst = new FirstLoungeNewApp_FuseControlsImage_Url_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Property(temp2, FirstLoungeNewApp_accessor_HomeIcons_Image.Singleton);
        var temp4 = new global::Fuse.Reactive.Constant(this);
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp1, __selector1);
        var temp5 = new global::Fuse.Reactive.Property(temp4, FirstLoungeNewApp_accessor_HomeIcons_name.Singleton);
        var temp6 = new global::Fuse.Reactive.DataBinding(temp_Url_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp7 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp5, Fuse.Reactive.BindingMode.Default);
        this.Alignment = Fuse.Elements.Alignment.Center;
        this.Margin = float4(20f, 0f, 20f, 0f);
        this.SourceLineNumber = 17;
        this.SourceFileName = "Pages/homepage.ux";
        temp.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp.Alignment = Fuse.Elements.Alignment.Center;
        temp.SourceLineNumber = 20;
        temp.SourceFileName = "Pages/homepage.ux";
        temp.Bindings.Add(temp6);
        temp3.SourceLineNumber = 20;
        temp3.SourceFileName = "Pages/homepage.ux";
        temp2.SourceLineNumber = 20;
        temp2.SourceFileName = "Pages/homepage.ux";
        temp1.TextColor = float4(0f, 0.1647059f, 0.2901961f, 1f);
        temp1.Alignment = Fuse.Elements.Alignment.Center;
        temp1.SourceLineNumber = 21;
        temp1.SourceFileName = "Pages/homepage.ux";
        temp1.Font = global::LoginPage.Poppins;
        temp1.Bindings.Add(temp7);
        temp5.SourceLineNumber = 21;
        temp5.SourceFileName = "Pages/homepage.ux";
        temp4.SourceLineNumber = 21;
        temp4.SourceFileName = "Pages/homepage.ux";
        this.Children.Add(temp);
        this.Children.Add(temp1);
    }
    static global::Uno.UX.Selector __selector0 = "Url";
    static global::Uno.UX.Selector __selector1 = "Value";
}
