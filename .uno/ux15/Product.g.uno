[Uno.Compiler.UxGenerated]
public partial class Product: Fuse.Controls.DockPanel
{
    global::Uno.UX.Property<string> temp_Value_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb41;
    static Product()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Product()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Text();
        temp_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("name");
        var temp4 = "₦";
        var temp5 = new global::Fuse.Reactive.Constant(temp4);
        var temp6 = new global::Fuse.Reactive.Data("price");
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp1, __selector0);
        var temp7 = new global::Fuse.Reactive.Add(temp5, temp6);
        var temp8 = "X ";
        var temp9 = new global::Fuse.Reactive.Constant(temp8);
        var temp10 = new global::Fuse.Reactive.Data("quantity");
        var temp2 = new global::Fuse.Controls.Text();
        temp2_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp2, __selector0);
        var temp11 = new global::Fuse.Reactive.Add(temp9, temp10);
        var temp12 = new global::Fuse.Reactive.Data("InventoryView");
        var temp13 = new global::Fuse.Controls.Image();
        var temp14 = new global::Fuse.Controls.StackPanel();
        var temp15 = new global::Fuse.Reactive.DataBinding(temp_Value_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp16 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp17 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp11, Fuse.Reactive.BindingMode.Default);
        var temp18 = new global::Fuse.Controls.DockPanel();
        var temp19 = new global::Fuse.Controls.Image();
        var temp20 = new global::Fuse.Controls.Rectangle();
        var temp21 = new global::Fuse.Drawing.Stroke();
        var temp22 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
        temp_eb41 = new global::Fuse.Reactive.EventBinding(temp12);
        this.HitTestMode = Fuse.Elements.HitTestMode.LocalBoundsAndChildren;
        this.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        this.Alignment = Fuse.Elements.Alignment.TopCenter;
        this.Margin = float4(5f, 5f, 5f, 5f);
        this.Padding = float4(10f, 10f, 10f, 10f);
        this.SourceLineNumber = 168;
        this.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Gestures.Clicked.AddHandler(this, temp_eb41.OnEvent);
        temp13.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp13.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp13.SourceLineNumber = 170;
        temp13.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp13, Fuse.Layouts.Dock.Left);
        temp13.File = new global::Uno.UX.BundleFileSource(import("../../Assets/packages.png"));
        temp14.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp14.Margin = float4(20f, 0f, 20f, 0f);
        temp14.SourceLineNumber = 171;
        temp14.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp14, Fuse.Layouts.Dock.Left);
        temp14.Children.Add(temp);
        temp14.Children.Add(temp1);
        temp14.Children.Add(temp2);
        temp.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp.Color = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        temp.Width = new Uno.UX.Size(150f, Uno.UX.Unit.Unspecified);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.SourceLineNumber = 172;
        temp.SourceFileName = "Pages/inventory.ux";
        temp.Font = global::LoginPage.Poppins;
        temp.Bindings.Add(temp15);
        temp3.SourceLineNumber = 172;
        temp3.SourceFileName = "Pages/inventory.ux";
        temp1.Width = new Uno.UX.Size(150f, Uno.UX.Unit.Unspecified);
        temp1.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp1.SourceLineNumber = 174;
        temp1.SourceFileName = "Pages/inventory.ux";
        temp1.Font = global::LoginPage.Poppins;
        temp1.Bindings.Add(temp16);
        temp7.SourceLineNumber = 174;
        temp7.SourceFileName = "Pages/inventory.ux";
        temp5.SourceLineNumber = 174;
        temp5.SourceFileName = "Pages/inventory.ux";
        temp6.SourceLineNumber = 174;
        temp6.SourceFileName = "Pages/inventory.ux";
        temp2.FontSize = 12f;
        temp2.TextColor = float4(0.7333333f, 0.5490196f, 0.007843138f, 1f);
        temp2.Width = new Uno.UX.Size(150f, Uno.UX.Unit.Unspecified);
        temp2.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp2.SourceLineNumber = 176;
        temp2.SourceFileName = "Pages/inventory.ux";
        temp2.Font = global::LoginPage.Poppins;
        temp2.Bindings.Add(temp17);
        temp11.SourceLineNumber = 176;
        temp11.SourceFileName = "Pages/inventory.ux";
        temp9.SourceLineNumber = 176;
        temp9.SourceFileName = "Pages/inventory.ux";
        temp10.SourceLineNumber = 176;
        temp10.SourceFileName = "Pages/inventory.ux";
        temp18.Alignment = Fuse.Elements.Alignment.CenterRight;
        temp18.SourceLineNumber = 180;
        temp18.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp18, Fuse.Layouts.Dock.Right);
        temp18.Children.Add(temp19);
        temp19.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp19.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp19.Margin = float4(0f, 0f, 5f, 0f);
        temp19.SourceLineNumber = 182;
        temp19.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp19, Fuse.Layouts.Dock.Left);
        temp19.File = new global::Uno.UX.BundleFileSource(import("../../Assets/next.png"));
        temp20.CornerRadius = float4(10f, 10f, 10f, 10f);
        temp20.Color = float4(1f, 1f, 1f, 1f);
        temp20.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp20.Layer = Fuse.Layer.Background;
        temp20.SourceLineNumber = 186;
        temp20.SourceFileName = "Pages/inventory.ux";
        temp20.Strokes.Add(temp21);
        temp21.Color = float4(0f, 1f, 0f, 1f);
        temp21.Width = 1f;
        temp21.Brush = temp22;
        temp12.SourceLineNumber = 168;
        temp12.SourceFileName = "Pages/inventory.ux";
        this.Children.Add(temp13);
        this.Children.Add(temp14);
        this.Children.Add(temp18);
        this.Children.Add(temp20);
        this.Bindings.Add(temp_eb41);
    }
    static global::Uno.UX.Selector __selector0 = "Value";
}
