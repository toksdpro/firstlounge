[Uno.Compiler.UxGenerated]
public partial class ProductField: Fuse.Controls.DockPanel
{
    string _field_Fieldvalue;
    [global::Uno.UX.UXOriginSetter("SetFieldvalue")]
    public string Fieldvalue
    {
        get { return _field_Fieldvalue; }
        set { SetFieldvalue(value, null); }
    }
    public void SetFieldvalue(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Fieldvalue)
        {
            _field_Fieldvalue = value;
            OnPropertyChanged("Fieldvalue", origin);
        }
    }
    string _field_Fieldname;
    [global::Uno.UX.UXOriginSetter("SetFieldname")]
    public string Fieldname
    {
        get { return _field_Fieldname; }
        set { SetFieldname(value, null); }
    }
    public void SetFieldname(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Fieldname)
        {
            _field_Fieldname = value;
            OnPropertyChanged("Fieldname", origin);
        }
    }
    string _field_BorderColor;
    [global::Uno.UX.UXOriginSetter("SetBorderColor")]
    public string BorderColor
    {
        get { return _field_BorderColor; }
        set { SetBorderColor(value, null); }
    }
    public void SetBorderColor(string value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_BorderColor)
        {
            _field_BorderColor = value;
            OnPropertyChanged("BorderColor", origin);
        }
    }
    global::Uno.UX.Property<Fuse.Drawing.Brush> temp_Brush_inst;
    global::Uno.UX.Property<string> itemName_PlaceholderText_inst;
    global::Uno.UX.Property<string> itemName_Value_inst;
    internal global::Fuse.Controls.TextInput itemName;
    static ProductField()
    {
    }
    [global::Uno.UX.UXConstructor]
    public ProductField()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp1 = new global::Fuse.Reactive.Constant(this);
        var temp = new global::Fuse.Drawing.Stroke();
        temp_Brush_inst = new FirstLoungeNewApp_FuseDrawingStroke_Brush_Property(temp, __selector0);
        var temp2 = new global::Fuse.Reactive.Property(temp1, FirstLoungeNewApp_accessor_ProductField_BorderColor.Singleton);
        var temp3 = new global::Fuse.Reactive.Constant(this);
        itemName = new global::Fuse.Controls.TextInput();
        itemName_PlaceholderText_inst = new FirstLoungeNewApp_FuseControlsTextInput_PlaceholderText_Property(itemName, __selector1);
        var temp4 = new global::Fuse.Reactive.Property(temp3, FirstLoungeNewApp_accessor_ProductField_Fieldname.Singleton);
        var temp5 = new global::Fuse.Reactive.Constant(this);
        itemName_Value_inst = new FirstLoungeNewApp_FuseControlsTextInputControl_Value_Property(itemName, __selector2);
        var temp6 = new global::Fuse.Reactive.Property(temp5, FirstLoungeNewApp_accessor_ProductField_Fieldvalue.Singleton);
        var temp7 = new global::Fuse.Controls.Rectangle();
        var temp8 = new global::Fuse.Reactive.DataBinding(temp_Brush_inst, temp2, Fuse.Reactive.BindingMode.Default);
        var temp9 = new global::Fuse.Reactive.DataBinding(itemName_PlaceholderText_inst, temp4, Fuse.Reactive.BindingMode.Default);
        var temp10 = new global::Fuse.Reactive.DataBinding(itemName_Value_inst, temp6, Fuse.Reactive.BindingMode.Default);
        this.BorderColor = "#777";
        this.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        this.Alignment = Fuse.Elements.Alignment.TopCenter;
        this.SourceLineNumber = 192;
        this.SourceFileName = "Pages/inventory.ux";
        itemName.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        itemName.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        itemName.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        itemName.Padding = float4(15f, 15f, 15f, 15f);
        itemName.Name = __selector3;
        itemName.SourceLineNumber = 200;
        itemName.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(itemName, Fuse.Layouts.Dock.Left);
        itemName.Children.Add(temp7);
        itemName.Bindings.Add(temp9);
        itemName.Bindings.Add(temp10);
        temp7.CornerRadius = float4(1f, 1f, 1f, 1f);
        temp7.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp7.Layer = Fuse.Layer.Background;
        temp7.SourceLineNumber = 202;
        temp7.SourceFileName = "Pages/inventory.ux";
        temp7.Strokes.Add(temp);
        temp7.Bindings.Add(temp8);
        temp.Color = float4(0f, 1f, 0f, 1f);
        temp.Width = 1.4f;
        temp2.SourceLineNumber = 203;
        temp2.SourceFileName = "Pages/inventory.ux";
        temp1.SourceLineNumber = 203;
        temp1.SourceFileName = "Pages/inventory.ux";
        temp4.SourceLineNumber = 200;
        temp4.SourceFileName = "Pages/inventory.ux";
        temp3.SourceLineNumber = 200;
        temp3.SourceFileName = "Pages/inventory.ux";
        temp6.SourceLineNumber = 200;
        temp6.SourceFileName = "Pages/inventory.ux";
        temp5.SourceLineNumber = 200;
        temp5.SourceFileName = "Pages/inventory.ux";
        this.Children.Add(itemName);
    }
    static global::Uno.UX.Selector __selector0 = "Brush";
    static global::Uno.UX.Selector __selector1 = "PlaceholderText";
    static global::Uno.UX.Selector __selector2 = "Value";
    static global::Uno.UX.Selector __selector3 = "itemName";
}
