[Uno.Compiler.UxGenerated]
public partial class SearchPage: Fuse.Controls.Page
{
    readonly Fuse.Navigation.Router router;
    [Uno.Compiler.UxGenerated]
    public partial class Template: Uno.UX.Template
    {
        [Uno.WeakReference] internal readonly SearchPage __parent;
        [Uno.WeakReference] internal readonly SearchPage __parentInstance;
        public Template(SearchPage parent, SearchPage parentInstance): base(null, false)
        {
            __parent = parent;
            __parentInstance = parentInstance;
        }
        static Template()
        {
        }
        public override object New()
        {
            var __self = new global::SortedItem();
            __self.SourceLineNumber = 52;
            __self.SourceFileName = "Pages/searchpage.ux";
            return __self;
        }
    }
    global::Uno.UX.Property<object> temp_Items_inst;
    internal global::Fuse.Drawing.ImageFill buttonBg;
    internal global::Fuse.Reactive.EventBinding temp_eb60;
    internal global::Fuse.Controls.TextInput text;
    internal global::Fuse.Reactive.EventBinding temp_eb61;
    internal global::Fuse.Controls.ScrollView ScrollbarBox;
    internal global::Fuse.Reactive.EventBinding temp_eb62;
    global::Uno.UX.NameTable __g_nametable;
    static string[] __g_static_nametable = new string[] {
        "router",
        "buttonBg",
        "temp_eb60",
        "text",
        "temp_eb61",
        "ScrollbarBox",
        "temp_eb62"
    };
    static SearchPage()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SearchPage(
		[global::Uno.UX.UXParameter("router")] Fuse.Navigation.Router router)
    {
        this.router = router;
        InitializeUX();
    }
    void InitializeUX()
    {
        __g_nametable = new global::Uno.UX.NameTable(null, __g_static_nametable);
        var temp1 = new global::Fuse.Reactive.Data("Searchfunction");
        var temp2 = new global::Fuse.Reactive.Data("checkout");
        var temp = new global::Fuse.Reactive.Each();
        temp_Items_inst = new FirstLoungeNewApp_FuseReactiveEach_Items_Property(temp, __selector0);
        var temp3 = new global::Fuse.Reactive.Data("searchList");
        var temp4 = new global::Fuse.Reactive.Data("goBack");
        var temp5 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp6 = new global::Fuse.Reactive.JavaScript(__g_nametable);
        var temp7 = new global::Fuse.Controls.DockPanel();
        var temp8 = new global::Header();
        var temp9 = new global::Fuse.Controls.StackPanel();
        var temp10 = new global::Fuse.Controls.DockPanel();
        var temp11 = new global::Fuse.Controls.Panel();
        var temp12 = new global::Fuse.Controls.Button();
        buttonBg = new global::Fuse.Drawing.ImageFill();
        var temp13 = new global::Fuse.Gestures.Tapped();
        var temp14 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb60 = new global::Fuse.Reactive.EventBinding(temp1);
        text = new global::Fuse.Controls.TextInput();
        var temp15 = new global::Fuse.Controls.Rectangle();
        var temp16 = new global::Fuse.Drawing.Stroke();
        var temp17 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
        var temp18 = new global::Fuse.Controls.Panel();
        var temp19 = new global::Fuse.Controls.Text();
        var temp20 = new global::Fuse.Controls.Rectangle();
        var temp21 = new global::Fuse.Drawing.Stroke();
        var temp22 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
        temp_eb61 = new global::Fuse.Reactive.EventBinding(temp2);
        var temp23 = new global::Fuse.Controls.DockPanel();
        var temp24 = new global::Fuse.Controls.Text();
        var temp25 = new global::Fuse.Controls.StackPanel();
        ScrollbarBox = new global::Fuse.Controls.ScrollView();
        var temp26 = new global::Fuse.Controls.StackPanel();
        var temp27 = new Template(this, this);
        var temp28 = new global::Fuse.Reactive.DataBinding(temp_Items_inst, temp3, Fuse.Reactive.BindingMode.Default);
        var temp29 = new global::Fuse.Controls.Panel();
        var temp30 = new global::SubmitButton1();
        temp_eb62 = new global::Fuse.Reactive.EventBinding(temp4);
        var temp31 = new global::FooterNav();
        var temp32 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9490196f, 0.9490196f, 0.9490196f, 1f));
        this.SourceLineNumber = 1;
        this.SourceFileName = "Pages/searchpage.ux";
        temp5.LineNumber = 4;
        temp5.FileName = "Pages/searchpage.ux";
        temp5.SourceLineNumber = 4;
        temp5.SourceFileName = "Pages/searchpage.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../Pages/navigation.js"));
        temp6.LineNumber = 5;
        temp6.FileName = "Pages/searchpage.ux";
        temp6.SourceLineNumber = 5;
        temp6.SourceFileName = "Pages/searchpage.ux";
        temp6.File = new global::Uno.UX.BundleFileSource(import("../../Pages/search.js"));
        temp7.SourceLineNumber = 8;
        temp7.SourceFileName = "Pages/searchpage.ux";
        temp7.Children.Add(temp8);
        temp7.Children.Add(temp9);
        temp7.Children.Add(temp25);
        temp7.Children.Add(temp31);
        temp8.SourceLineNumber = 9;
        temp8.SourceFileName = "Pages/searchpage.ux";
        temp9.Color = float4(0.9490196f, 0.9490196f, 0.9490196f, 1f);
        temp9.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp9.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp9.SourceLineNumber = 12;
        temp9.SourceFileName = "Pages/searchpage.ux";
        temp9.Children.Add(temp10);
        temp9.Children.Add(temp23);
        temp10.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        temp10.Margin = float4(0f, 10f, 0f, 0f);
        temp10.SourceLineNumber = 15;
        temp10.SourceFileName = "Pages/searchpage.ux";
        temp10.Children.Add(temp11);
        temp10.Children.Add(temp18);
        temp11.Width = new Uno.UX.Size(70f, Uno.UX.Unit.Percent);
        temp11.Margin = float4(5f, 5f, 5f, 5f);
        temp11.SourceLineNumber = 16;
        temp11.SourceFileName = "Pages/searchpage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp11, Fuse.Layouts.Dock.Left);
        temp11.Children.Add(temp12);
        temp11.Children.Add(text);
        temp11.Children.Add(temp15);
        temp12.Color = float4(0f, 0f, 0f, 1f);
        temp12.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp12.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp12.Alignment = Fuse.Elements.Alignment.Right;
        temp12.Padding = float4(15f, 15f, 15f, 15f);
        temp12.SourceLineNumber = 17;
        temp12.SourceFileName = "Pages/searchpage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp12, Fuse.Layouts.Dock.Right);
        temp12.Background = buttonBg;
        temp12.Children.Add(temp13);
        buttonBg.File = new global::Uno.UX.BundleFileSource(import("../../Assets/search-icon-2.png"));
        temp13.SourceLineNumber = 19;
        temp13.SourceFileName = "Pages/searchpage.ux";
        temp13.Actions.Add(temp14);
        temp13.Bindings.Add(temp_eb60);
        temp14.SourceLineNumber = 20;
        temp14.SourceFileName = "Pages/searchpage.ux";
        temp14.Handler += temp_eb60.OnEvent;
        temp1.SourceLineNumber = 20;
        temp1.SourceFileName = "Pages/searchpage.ux";
        text.PlaceholderText = "Search by Name";
        text.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        text.Value = "";
        text.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        text.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        text.Alignment = Fuse.Elements.Alignment.Left;
        text.Padding = float4(15f, 15f, 15f, 15f);
        text.Name = __selector1;
        text.SourceLineNumber = 24;
        text.SourceFileName = "Pages/searchpage.ux";
        global::Fuse.Controls.DockPanel.SetDock(text, Fuse.Layouts.Dock.Left);
        temp15.CornerRadius = float4(15f, 15f, 15f, 15f);
        temp15.Color = float4(1f, 1f, 1f, 1f);
        temp15.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp15.Layer = Fuse.Layer.Background;
        temp15.SourceLineNumber = 26;
        temp15.SourceFileName = "Pages/searchpage.ux";
        temp15.Strokes.Add(temp16);
        temp16.Color = float4(0f, 1f, 0f, 1f);
        temp16.Width = 1f;
        temp16.Brush = temp17;
        temp18.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Percent);
        temp18.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp18.Padding = float4(8f, 8f, 8f, 8f);
        temp18.SourceLineNumber = 32;
        temp18.SourceFileName = "Pages/searchpage.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp18, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp18, temp_eb61.OnEvent);
        temp18.Children.Add(temp19);
        temp18.Children.Add(temp20);
        temp18.Bindings.Add(temp_eb61);
        temp19.Value = "CHECKOUT";
        temp19.FontSize = 12f;
        temp19.Color = float4(1f, 1f, 1f, 1f);
        temp19.Alignment = Fuse.Elements.Alignment.Center;
        temp19.SourceLineNumber = 33;
        temp19.SourceFileName = "Pages/searchpage.ux";
        temp19.Font = global::LoginPage.Poppins;
        temp20.CornerRadius = float4(2f, 2f, 2f, 2f);
        temp20.Color = float4(0.5058824f, 0.6039216f, 0.1019608f, 1f);
        temp20.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp20.Layer = Fuse.Layer.Background;
        temp20.SourceLineNumber = 34;
        temp20.SourceFileName = "Pages/searchpage.ux";
        temp20.Strokes.Add(temp21);
        temp21.Color = float4(0f, 1f, 0f, 1f);
        temp21.Width = 1f;
        temp21.Brush = temp22;
        temp2.SourceLineNumber = 32;
        temp2.SourceFileName = "Pages/searchpage.ux";
        temp23.Alignment = Fuse.Elements.Alignment.TopCenter;
        temp23.Margin = float4(0f, 0f, 0f, 10f);
        temp23.SourceLineNumber = 41;
        temp23.SourceFileName = "Pages/searchpage.ux";
        temp23.Children.Add(temp24);
        temp24.Value = "Search Results";
        temp24.SourceLineNumber = 42;
        temp24.SourceFileName = "Pages/searchpage.ux";
        temp24.Font = global::LoginPage.Poppins;
        temp25.Alignment = Fuse.Elements.Alignment.Center;
        temp25.SourceLineNumber = 46;
        temp25.SourceFileName = "Pages/searchpage.ux";
        temp25.Children.Add(ScrollbarBox);
        temp25.Children.Add(temp29);
        ScrollbarBox.Height = new Uno.UX.Size(350f, Uno.UX.Unit.Unspecified);
        ScrollbarBox.Alignment = Fuse.Elements.Alignment.Center;
        ScrollbarBox.Name = __selector2;
        ScrollbarBox.SourceLineNumber = 47;
        ScrollbarBox.SourceFileName = "Pages/searchpage.ux";
        ScrollbarBox.Children.Add(temp26);
        temp26.Margin = float4(0f, 20f, 0f, 0f);
        temp26.SourceLineNumber = 49;
        temp26.SourceFileName = "Pages/searchpage.ux";
        temp26.Children.Add(temp);
        temp.SourceLineNumber = 51;
        temp.SourceFileName = "Pages/searchpage.ux";
        temp.Templates.Add(temp27);
        temp.Bindings.Add(temp28);
        temp3.SourceLineNumber = 51;
        temp3.SourceFileName = "Pages/searchpage.ux";
        temp29.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        temp29.Margin = float4(0f, 10f, 0f, 0f);
        temp29.Padding = float4(5f, 5f, 5f, 5f);
        temp29.SourceLineNumber = 59;
        temp29.SourceFileName = "Pages/searchpage.ux";
        temp29.Children.Add(temp30);
        temp30.Text = "Back";
        temp30.Alignment = Fuse.Elements.Alignment.Center;
        temp30.SourceLineNumber = 60;
        temp30.SourceFileName = "Pages/searchpage.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp30, temp_eb62.OnEvent);
        temp30.Bindings.Add(temp_eb62);
        temp4.SourceLineNumber = 60;
        temp4.SourceFileName = "Pages/searchpage.ux";
        temp31.SourceLineNumber = 66;
        temp31.SourceFileName = "Pages/searchpage.ux";
        __g_nametable.This = this;
        __g_nametable.Objects.Add(router);
        __g_nametable.Objects.Add(buttonBg);
        __g_nametable.Objects.Add(temp_eb60);
        __g_nametable.Objects.Add(text);
        __g_nametable.Objects.Add(temp_eb61);
        __g_nametable.Objects.Add(ScrollbarBox);
        __g_nametable.Objects.Add(temp_eb62);
        this.Background = temp32;
        this.Children.Add(temp5);
        this.Children.Add(temp6);
        this.Children.Add(temp7);
    }
    static global::Uno.UX.Selector __selector0 = "Items";
    static global::Uno.UX.Selector __selector1 = "text";
    static global::Uno.UX.Selector __selector2 = "ScrollbarBox";
}
