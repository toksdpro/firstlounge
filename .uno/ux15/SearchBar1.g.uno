[Uno.Compiler.UxGenerated]
public partial class SearchBar1: Fuse.Controls.DockPanel
{
    internal global::Fuse.Drawing.ImageFill buttonBg;
    internal global::Fuse.Controls.TextInput text;
    static SearchBar1()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SearchBar1()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Controls.Panel();
        var temp1 = new global::Fuse.Controls.Button();
        buttonBg = new global::Fuse.Drawing.ImageFill();
        text = new global::Fuse.Controls.TextInput();
        var temp2 = new global::Fuse.Controls.Rectangle();
        var temp3 = new global::Fuse.Drawing.Stroke();
        var temp4 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
        this.Margin = float4(0f, 10f, 0f, 0f);
        this.SourceLineNumber = 7;
        this.SourceFileName = "Pages/history.ux";
        temp.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp.Margin = float4(5f, 5f, 5f, 5f);
        temp.SourceLineNumber = 8;
        temp.SourceFileName = "Pages/history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Children.Add(temp1);
        temp.Children.Add(text);
        temp.Children.Add(temp2);
        temp1.Color = float4(0f, 0f, 0f, 1f);
        temp1.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp1.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp1.Alignment = Fuse.Elements.Alignment.Right;
        temp1.Padding = float4(15f, 15f, 15f, 15f);
        temp1.SourceLineNumber = 9;
        temp1.SourceFileName = "Pages/history.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Right);
        temp1.Background = buttonBg;
        buttonBg.File = new global::Uno.UX.BundleFileSource(import("../../Assets/search-icon-2.png"));
        text.PlaceholderText = "Search by Name";
        text.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        text.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        text.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        text.Alignment = Fuse.Elements.Alignment.Left;
        text.Padding = float4(15f, 15f, 15f, 15f);
        text.Name = __selector0;
        text.SourceLineNumber = 13;
        text.SourceFileName = "Pages/history.ux";
        global::Fuse.Controls.DockPanel.SetDock(text, Fuse.Layouts.Dock.Left);
        temp2.CornerRadius = float4(15f, 15f, 15f, 15f);
        temp2.Color = float4(1f, 1f, 1f, 1f);
        temp2.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp2.Layer = Fuse.Layer.Background;
        temp2.SourceLineNumber = 15;
        temp2.SourceFileName = "Pages/history.ux";
        temp2.Strokes.Add(temp3);
        temp3.Color = float4(0f, 1f, 0f, 1f);
        temp3.Width = 1f;
        temp3.Brush = temp4;
        this.Children.Add(temp);
    }
    static global::Uno.UX.Selector __selector0 = "text";
}
