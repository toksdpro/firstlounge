[Uno.Compiler.UxGenerated]
public partial class SortedItem: Fuse.Controls.DockPanel
{
    global::Uno.UX.Property<string> temp_Url_inst;
    global::Uno.UX.Property<string> temp1_Value_inst;
    global::Uno.UX.Property<string> temp2_Value_inst;
    global::Uno.UX.Property<string> temp3_Value_inst;
    internal global::Fuse.Reactive.EventBinding temp_eb4;
    static SortedItem()
    {
    }
    [global::Uno.UX.UXConstructor]
    public SortedItem()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp4 = "http://restaurants.saddleng.com/storage/";
        var temp5 = new global::Fuse.Reactive.Constant(temp4);
        var temp6 = new global::Fuse.Reactive.Data("product_image");
        var temp = new global::Fuse.Controls.Image();
        temp_Url_inst = new FirstLoungeNewApp_FuseControlsImage_Url_Property(temp, __selector0);
        var temp7 = new global::Fuse.Reactive.Add(temp5, temp6);
        var temp1 = new global::Fuse.Controls.Text();
        temp1_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp1, __selector1);
        var temp8 = new global::Fuse.Reactive.Data("name");
        var temp9 = "N";
        var temp10 = new global::Fuse.Reactive.Constant(temp9);
        var temp11 = new global::Fuse.Reactive.Data("price");
        var temp2 = new global::Fuse.Controls.Text();
        temp2_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp2, __selector1);
        var temp12 = new global::Fuse.Reactive.Add(temp10, temp11);
        var temp13 = "X ";
        var temp14 = new global::Fuse.Reactive.Constant(temp13);
        var temp15 = new global::Fuse.Reactive.Data("quantity");
        var temp3 = new global::Fuse.Controls.Text();
        temp3_Value_inst = new FirstLoungeNewApp_FuseControlsTextControl_Value_Property(temp3, __selector1);
        var temp16 = new global::Fuse.Reactive.Add(temp14, temp15);
        var temp17 = new global::Fuse.Reactive.Data("InventoryView");
        var temp18 = new global::Fuse.Reactive.DataBinding(temp_Url_inst, temp7, Fuse.Reactive.BindingMode.Default);
        var temp19 = new global::Fuse.Controls.StackPanel();
        var temp20 = new global::Fuse.Reactive.DataBinding(temp1_Value_inst, temp8, Fuse.Reactive.BindingMode.Default);
        var temp21 = new global::Fuse.Reactive.DataBinding(temp2_Value_inst, temp12, Fuse.Reactive.BindingMode.Default);
        var temp22 = new global::Fuse.Reactive.DataBinding(temp3_Value_inst, temp16, Fuse.Reactive.BindingMode.Default);
        var temp23 = new global::Fuse.Controls.DockPanel();
        var temp24 = new global::Fuse.Controls.Image();
        temp_eb4 = new global::Fuse.Reactive.EventBinding(temp17);
        var temp25 = new global::Fuse.Controls.Rectangle();
        var temp26 = new global::Fuse.Drawing.Stroke();
        var temp27 = new global::Fuse.Drawing.StaticSolidColor(float4(0.9254902f, 0.9254902f, 0.9254902f, 1f));
        this.Width = new Uno.UX.Size(95f, Uno.UX.Unit.Percent);
        this.Alignment = Fuse.Elements.Alignment.TopCenter;
        this.Margin = float4(5f, 5f, 5f, 5f);
        this.Padding = float4(10f, 10f, 10f, 10f);
        this.SourceLineNumber = 189;
        this.SourceFileName = "Pages/categoryProducts.ux";
        temp.Width = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        temp.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp.SourceLineNumber = 190;
        temp.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp, Fuse.Layouts.Dock.Left);
        temp.Bindings.Add(temp18);
        temp7.SourceLineNumber = 190;
        temp7.SourceFileName = "Pages/categoryProducts.ux";
        temp5.SourceLineNumber = 190;
        temp5.SourceFileName = "Pages/categoryProducts.ux";
        temp6.SourceLineNumber = 190;
        temp6.SourceFileName = "Pages/categoryProducts.ux";
        temp19.Width = new Uno.UX.Size(55f, Uno.UX.Unit.Percent);
        temp19.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp19.Margin = float4(20f, 0f, 20f, 0f);
        temp19.SourceLineNumber = 191;
        temp19.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp19, Fuse.Layouts.Dock.Left);
        temp19.Children.Add(temp1);
        temp19.Children.Add(temp2);
        temp19.Children.Add(temp3);
        temp1.TextWrapping = Fuse.Controls.TextWrapping.Wrap;
        temp1.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp1.SourceLineNumber = 192;
        temp1.SourceFileName = "Pages/categoryProducts.ux";
        temp1.Font = global::LoginPage.Poppins;
        temp1.Bindings.Add(temp20);
        temp8.SourceLineNumber = 192;
        temp8.SourceFileName = "Pages/categoryProducts.ux";
        temp2.Width = new Uno.UX.Size(150f, Uno.UX.Unit.Unspecified);
        temp2.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp2.SourceLineNumber = 196;
        temp2.SourceFileName = "Pages/categoryProducts.ux";
        temp2.Font = global::LoginPage.Poppins;
        temp2.Bindings.Add(temp21);
        temp12.SourceLineNumber = 196;
        temp12.SourceFileName = "Pages/categoryProducts.ux";
        temp10.SourceLineNumber = 196;
        temp10.SourceFileName = "Pages/categoryProducts.ux";
        temp11.SourceLineNumber = 196;
        temp11.SourceFileName = "Pages/categoryProducts.ux";
        temp3.FontSize = 12f;
        temp3.TextColor = float4(0.7333333f, 0.5490196f, 0.007843138f, 1f);
        temp3.Width = new Uno.UX.Size(150f, Uno.UX.Unit.Unspecified);
        temp3.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp3.SourceLineNumber = 198;
        temp3.SourceFileName = "Pages/categoryProducts.ux";
        temp3.Font = global::LoginPage.Poppins;
        temp3.Bindings.Add(temp22);
        temp16.SourceLineNumber = 198;
        temp16.SourceFileName = "Pages/categoryProducts.ux";
        temp14.SourceLineNumber = 198;
        temp14.SourceFileName = "Pages/categoryProducts.ux";
        temp15.SourceLineNumber = 198;
        temp15.SourceFileName = "Pages/categoryProducts.ux";
        temp23.Alignment = Fuse.Elements.Alignment.CenterRight;
        temp23.SourceLineNumber = 201;
        temp23.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp23, Fuse.Layouts.Dock.Right);
        temp23.Children.Add(temp24);
        temp24.Width = new Uno.UX.Size(20f, Uno.UX.Unit.Unspecified);
        temp24.Alignment = Fuse.Elements.Alignment.CenterLeft;
        temp24.Margin = float4(0f, 0f, 5f, 0f);
        temp24.SourceLineNumber = 203;
        temp24.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp24, Fuse.Layouts.Dock.Left);
        global::Fuse.Gestures.Clicked.AddHandler(temp24, temp_eb4.OnEvent);
        temp24.File = new global::Uno.UX.BundleFileSource(import("../../Assets/next.png"));
        temp24.Bindings.Add(temp_eb4);
        temp17.SourceLineNumber = 203;
        temp17.SourceFileName = "Pages/categoryProducts.ux";
        temp25.CornerRadius = float4(10f, 10f, 10f, 10f);
        temp25.Color = float4(1f, 1f, 1f, 1f);
        temp25.Height = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp25.Layer = Fuse.Layer.Background;
        temp25.SourceLineNumber = 207;
        temp25.SourceFileName = "Pages/categoryProducts.ux";
        temp25.Strokes.Add(temp26);
        temp26.Color = float4(0f, 1f, 0f, 1f);
        temp26.Width = 1f;
        temp26.Brush = temp27;
        this.Children.Add(temp);
        this.Children.Add(temp19);
        this.Children.Add(temp23);
        this.Children.Add(temp25);
    }
    static global::Uno.UX.Selector __selector0 = "Url";
    static global::Uno.UX.Selector __selector1 = "Value";
}
