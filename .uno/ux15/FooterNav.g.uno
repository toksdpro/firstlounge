[Uno.Compiler.UxGenerated]
public partial class FooterNav: Fuse.Controls.Grid
{
    internal global::Fuse.Reactive.EventBinding temp_eb1;
    internal global::Fuse.Reactive.EventBinding temp_eb2;
    internal global::Fuse.Reactive.EventBinding temp_eb3;
    static FooterNav()
    {
    }
    [global::Uno.UX.UXConstructor]
    public FooterNav()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.Data("inventory");
        var temp1 = new global::Fuse.Reactive.Data("home");
        var temp2 = new global::Fuse.Reactive.Data("history");
        var temp3 = new global::Fuse.Controls.Image();
        temp_eb1 = new global::Fuse.Reactive.EventBinding(temp);
        var temp4 = new global::Fuse.Controls.Panel();
        var temp5 = new global::Fuse.Controls.Image();
        var temp6 = new global::Fuse.Controls.Circle();
        temp_eb2 = new global::Fuse.Reactive.EventBinding(temp1);
        var temp7 = new global::Fuse.Controls.Image();
        temp_eb3 = new global::Fuse.Reactive.EventBinding(temp2);
        this.Columns = "1*,1*,1*";
        this.Color = float4(0.07843138f, 0.1294118f, 0.2313726f, 1f);
        this.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        this.SourceLineNumber = 178;
        this.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Controls.DockPanel.SetDock(this, Fuse.Layouts.Dock.Bottom);
        temp3.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp3.SourceLineNumber = 179;
        temp3.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp3, temp_eb1.OnEvent);
        temp3.File = new global::Uno.UX.BundleFileSource(import("../../Assets/inventory2.png"));
        temp3.Bindings.Add(temp_eb1);
        temp.SourceLineNumber = 179;
        temp.SourceFileName = "Pages/categoryProducts.ux";
        temp4.Width = new Uno.UX.Size(200f, Uno.UX.Unit.Unspecified);
        temp4.Height = new Uno.UX.Size(80f, Uno.UX.Unit.Unspecified);
        temp4.Offset = new Uno.UX.Size2(new Uno.UX.Size(0f, Uno.UX.Unit.Unspecified), new Uno.UX.Size(-14f, Uno.UX.Unit.Unspecified));
        temp4.SourceLineNumber = 180;
        temp4.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp4, temp_eb2.OnEvent);
        temp4.Children.Add(temp5);
        temp4.Children.Add(temp6);
        temp4.Bindings.Add(temp_eb2);
        temp5.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Unspecified);
        temp5.SourceLineNumber = 181;
        temp5.SourceFileName = "Pages/categoryProducts.ux";
        temp5.File = new global::Uno.UX.BundleFileSource(import("../../Assets/home.png"));
        temp6.Color = float4(0.07450981f, 0.1137255f, 0.1960784f, 1f);
        temp6.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp6.Layer = Fuse.Layer.Background;
        temp6.SourceLineNumber = 182;
        temp6.SourceFileName = "Pages/categoryProducts.ux";
        temp1.SourceLineNumber = 180;
        temp1.SourceFileName = "Pages/categoryProducts.ux";
        temp7.Width = new Uno.UX.Size(45f, Uno.UX.Unit.Unspecified);
        temp7.SourceLineNumber = 184;
        temp7.SourceFileName = "Pages/categoryProducts.ux";
        global::Fuse.Gestures.Clicked.AddHandler(temp7, temp_eb3.OnEvent);
        temp7.File = new global::Uno.UX.BundleFileSource(import("../../Assets/history2.png"));
        temp7.Bindings.Add(temp_eb3);
        temp2.SourceLineNumber = 184;
        temp2.SourceFileName = "Pages/categoryProducts.ux";
        this.Children.Add(temp3);
        this.Children.Add(temp4);
        this.Children.Add(temp7);
    }
}
