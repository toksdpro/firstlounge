[Uno.Compiler.UxGenerated]
public partial class OnlySearch: Fuse.Controls.DockPanel
{
    internal global::Fuse.Drawing.ImageFill buttonBg;
    internal global::Fuse.Reactive.EventBinding temp_eb40;
    internal global::Fuse.Controls.TextInput text;
    static OnlySearch()
    {
    }
    [global::Uno.UX.UXConstructor]
    public OnlySearch()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.Data("SearchPage");
        var temp1 = new global::Fuse.Controls.Panel();
        var temp2 = new global::Fuse.Controls.Button();
        buttonBg = new global::Fuse.Drawing.ImageFill();
        var temp3 = new global::Fuse.Gestures.Tapped();
        var temp4 = new global::Fuse.Triggers.Actions.Callback();
        temp_eb40 = new global::Fuse.Reactive.EventBinding(temp);
        text = new global::Fuse.Controls.TextInput();
        var temp5 = new global::Fuse.Controls.Rectangle();
        var temp6 = new global::Fuse.Drawing.Stroke();
        var temp7 = new global::Fuse.Drawing.StaticSolidColor(float4(0.5568628f, 0.5882353f, 0.6470588f, 1f));
        this.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        this.Alignment = Fuse.Elements.Alignment.TopCenter;
        this.Margin = float4(0f, 5f, 0f, 5f);
        this.SourceLineNumber = 149;
        this.SourceFileName = "Pages/inventory.ux";
        temp1.Width = new Uno.UX.Size(100f, Uno.UX.Unit.Percent);
        temp1.Margin = float4(5f, 5f, 5f, 5f);
        temp1.SourceLineNumber = 150;
        temp1.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp1, Fuse.Layouts.Dock.Left);
        temp1.Children.Add(temp2);
        temp1.Children.Add(text);
        temp1.Children.Add(temp5);
        temp2.Color = float4(0f, 0f, 0f, 1f);
        temp2.Width = new Uno.UX.Size(40f, Uno.UX.Unit.Unspecified);
        temp2.Height = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp2.Alignment = Fuse.Elements.Alignment.Right;
        temp2.Padding = float4(15f, 15f, 15f, 15f);
        temp2.SourceLineNumber = 151;
        temp2.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(temp2, Fuse.Layouts.Dock.Right);
        temp2.Background = buttonBg;
        temp2.Children.Add(temp3);
        buttonBg.File = new global::Uno.UX.BundleFileSource(import("../../Assets/search-icon-2.png"));
        temp3.SourceLineNumber = 153;
        temp3.SourceFileName = "Pages/inventory.ux";
        temp3.Actions.Add(temp4);
        temp3.Bindings.Add(temp_eb40);
        temp4.SourceLineNumber = 154;
        temp4.SourceFileName = "Pages/inventory.ux";
        temp4.Handler += temp_eb40.OnEvent;
        temp.SourceLineNumber = 154;
        temp.SourceFileName = "Pages/inventory.ux";
        text.PlaceholderText = "Search by Name";
        text.PlaceholderColor = float4(0.8f, 0.8f, 0.8f, 1f);
        text.Value = "";
        text.Width = new Uno.UX.Size(90f, Uno.UX.Unit.Percent);
        text.Height = new Uno.UX.Size(50f, Uno.UX.Unit.Unspecified);
        text.Alignment = Fuse.Elements.Alignment.Left;
        text.Padding = float4(15f, 15f, 15f, 15f);
        text.Name = __selector0;
        text.SourceLineNumber = 158;
        text.SourceFileName = "Pages/inventory.ux";
        global::Fuse.Controls.DockPanel.SetDock(text, Fuse.Layouts.Dock.Left);
        temp5.CornerRadius = float4(15f, 15f, 15f, 15f);
        temp5.Color = float4(1f, 1f, 1f, 1f);
        temp5.MaxHeight = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        temp5.Layer = Fuse.Layer.Background;
        temp5.SourceLineNumber = 160;
        temp5.SourceFileName = "Pages/inventory.ux";
        temp5.Strokes.Add(temp6);
        temp6.Color = float4(0f, 1f, 0f, 1f);
        temp6.Width = 1f;
        temp6.Brush = temp7;
        this.Children.Add(temp1);
    }
    static global::Uno.UX.Selector __selector0 = "text";
}
