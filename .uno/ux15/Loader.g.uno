[Uno.Compiler.UxGenerated]
public partial class Loader: Fuse.Controls.Text
{
    float4 _field_Lcolor;
    [global::Uno.UX.UXOriginSetter("SetLcolor")]
    public float4 Lcolor
    {
        get { return _field_Lcolor; }
        set { SetLcolor(value, null); }
    }
    public void SetLcolor(float4 value, global::Uno.UX.IPropertyListener origin)
    {
        if (value != _field_Lcolor)
        {
            _field_Lcolor = value;
            OnPropertyChanged("Lcolor", origin);
        }
    }
    global::Uno.UX.Property<float4> this_Color_inst;
    static Loader()
    {
    }
    [global::Uno.UX.UXConstructor]
    public Loader()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        var temp = new global::Fuse.Reactive.Constant(this);
        this_Color_inst = new FirstLoungeNewApp_FuseControlsTextControl_Color_Property(this, __selector0);
        var temp1 = new global::Fuse.Reactive.Property(temp, FirstLoungeNewApp_accessor_Loader_Lcolor.Singleton);
        var temp2 = new global::Fuse.Triggers.WhileTrue();
        var temp3 = new global::Fuse.Animations.Spin();
        var temp4 = new global::Fuse.Reactive.DataBinding(this_Color_inst, temp1, Fuse.Reactive.BindingMode.Read);
        this.Value = "\uF110";
        this.FontSize = 30f;
        this.Alignment = Fuse.Elements.Alignment.Center;
        this.SourceLineNumber = 172;
        this.SourceFileName = "Pages/login.ux";
        temp2.Value = true;
        temp2.SourceLineNumber = 174;
        temp2.SourceFileName = "Pages/login.ux";
        temp2.Animators.Add(temp3);
        temp3.Frequency = 1;
        temp1.SourceLineNumber = 172;
        temp1.SourceFileName = "Pages/login.ux";
        temp.SourceLineNumber = 172;
        temp.SourceFileName = "Pages/login.ux";
        this.Font = global::LoginPage.FontAwesome;
        this.Children.Add(temp2);
        this.Bindings.Add(temp4);
    }
    static global::Uno.UX.Selector __selector0 = "Color";
}
