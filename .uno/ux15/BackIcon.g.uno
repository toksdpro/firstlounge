[Uno.Compiler.UxGenerated]
public partial class BackIcon: Fuse.Controls.Circle
{
    internal global::Fuse.Controls.Image logout;
    static BackIcon()
    {
    }
    [global::Uno.UX.UXConstructor]
    public BackIcon()
    {
        InitializeUX();
    }
    void InitializeUX()
    {
        logout = new global::Fuse.Controls.Image();
        this.Color = float4(1f, 1f, 1f, 1f);
        this.Width = new Uno.UX.Size(30f, Uno.UX.Unit.Unspecified);
        this.Height = new Uno.UX.Size(25f, Uno.UX.Unit.Unspecified);
        this.Layer = Fuse.Layer.Background;
        this.SourceLineNumber = 151;
        this.SourceFileName = "Pages/categoryProducts.ux";
        logout.Opacity = 1f;
        logout.Name = __selector0;
        logout.SourceLineNumber = 152;
        logout.SourceFileName = "Pages/categoryProducts.ux";
        logout.File = new global::Uno.UX.BundleFileSource(import("../../Assets/goback.png"));
        this.Children.Add(logout);
    }
    static global::Uno.UX.Selector __selector0 = "logout";
}
