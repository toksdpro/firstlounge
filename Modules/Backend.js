var Observable = require("FuseJS/Observable");

var historyList = Observable();
var staffDetails = Observable();

var api = require('../Pages/api.js');

var login = require('../Pages/login.js');


var cart = [];
// priceTotal = 100;
var quantityTotal = Observable(0);


function closeDialog() {
	isDialogShowing.value = false;
}

function closeMessageDialog() {
	isDialogMessageShowing.value = false;
}


function isEmpty(str) {
	if(!str || str.length === 0){
		return true;
	}else{
		return false;
	}
}

function commit(coll) {
	coll.save(function (err) {
		if (err) {
			console.log("ERROR: " + err);
		}
	});
}

function setCartEmpty() {
	cart = [];
}


function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function lowerlize(string) {
    return string.charAt(0).toLowerCase() + string.slice(1);
}


// function checkIfItemExistsInDB(productObservable, key) {
// 	var found = false;
// 	productObservable.forEach(function(item) {
// 		if (item.productName.toLowerCase() == key.toLowerCase()) {
// 			found = true;
// 		}
// 				// console.log("itemName: "+item.productName);
// 	});
// 	return found;
// }

function isEmpty(str) {
	if(!str || str.length === 0){
		return true;
	}else{
		return false;
	}
}

// var categoryArray = [];

function getStaffDetails() {

	login.getToken()
	.then(function(contents) {
	    // console.log('token: '+contents);
		if (contents != null) {
			// token.value = contents;
			GetStaff(contents);
		}else{
			console.log("Error not logged in");
		}
	}, function(error) {
		console.log(error);
	});

	function GetStaff(token) {
		// body...
	
		api.ApiStaffDetails(token)
		.then(function(response) {
			var ok = response.ok;
			var status = response.status;
			var response_msg = JSON.parse(response._bodyText);
			if (ok) {
				console.log("staffDetails: "+JSON.stringify(JSON.parse(response._bodyText).data));
				// var arr = [];
				// arr.push(JSON.parse(response._bodyText).data);
				var data = JSON.parse(response._bodyText).data.name.split(" ");
				staffDetails.replaceAll(data);
			}else{
				if (status == 502) {
					console.log("Network Error");
				}else {
					console.log(status);	
				}
			}
		}).catch(function(err) {
			console.log(err+": There was an error");
		});
	}

	return staffDetails;
}

// function LoadCategory() {
// 	// reset error status
// 	apistatus.clear();

// 	login.getToken()
// 	.then(function(contents) {
// 	    console.log('token: '+contents);
// 		if (contents != null) {
// 			// token.value = contents;
// 			GetCategories(contents);
// 		}else{
// 			console.log("Error not logged in");
// 		}
// 	}, function(error) {
// 		console.log(error);
// 	});

// 	function GetCategories(token) {
// 		api.ApiCategory(token)
// 		.then(function(response) {
// 			// console.log('tokenD: '+token);
// 			var ok = response.ok;
// 			var status = response.status;
// 			var response_msg = JSON.parse(response._bodyText);
// 			if (ok) {
// 				// console.log("error");
// 				console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).categories));
// 				// var arr = [];
// 				// arr.push(JSON.parse(response._bodyText).categories);
// 				categoryList.replaceAll(JSON.parse(response._bodyText).categories);
// 				// apistatus.value = status;
// 			}else{
// 				if (status == 502) {
// 					console.log("Network Error");
// 					// error.value = "Network error";
// 				}else {
// 					console.log(status);	
// 					// error.value = "There was an error";
// 				}
				
// 			}

// 			apistatus.value = status;

// 		}).catch(function(err) {
// 			// console.log('here');
// 			apistatus.value = 401;
// 			console.log('here: '+error.value);

// 			console.log("There was an error");
// 		});
// 	}

	
// 	return categoryList;
// }


// function LoadProductsByCategory(category_id) {
// 	// reset error status
// 	apistatus.clear();

// 	var arr = [];
// 	productsByCategoryList.refreshAll(arr);

// 	login.getToken()
// 	.then(function(contents) {
// 	    console.log('token: '+contents);
// 		if (contents != null) {
// 			GetCategoryProducts(contents);
// 		}else{
// 			console.log("Error not logged in");
// 		}
// 	}, function(error) {
// 		console.log(error);
// 	});

// 	function GetCategoryProducts(token) {
// 		api.ApiCategoryProducts(token, category_id)
// 		.then(function(response) {
// 			var ok = response.ok;
// 			var status = response.status;
// 			var response_msg = JSON.parse(response._bodyText);
// 			if (ok) {
// 				console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).products));
// 				// var arr = [];
// 				// arr.push(JSON.parse(response._bodyText).categories);
// 				productsByCategoryList.replaceAll(JSON.parse(response._bodyText).products);
// 			}else{
// 				if (status == 502) {
// 					console.log("Network Error");
// 				}else {
// 					console.log(status);	
// 				}
// 			}
// 		}).catch(function(err) {
// 			console.log("There was an error");
// 		});
// 	}
// 	// itemCollection.load(function (err, tableStats, metaStats) {
// 	// 	if (!err) {
// 	// 		// Load was successful
// 	// 		result = itemCollection.find({
// 	// 			category:{
// 	// 				$eq: category
// 	// 			}
// 	// 		});
// 	// 		// console.log(JSON.stringify(result));
// 	// 		if (tableStats.foundData && tableStats.rowCount > 0) {
// 	// 			// console.log(JSON.stringify(result));
// 	// 			for (var item in result) {
// 	// 				if (result[item].productName) {
// 	// 					product = new Item(result[item].productName, result[item].category, result[item].price, result[item].quantity)
// 	// 					product._id = result[item]._id;
// 	// 					arr.push(product);
// 	// 				}
// 	// 			}
// 	// 			productsByCategoryList.refreshAll(arr);
// 	// 			// console.log("product list: "+JSON.stringify(productsByCategoryList.value));
// 	// 			// productsByCategoryList.forEach(function(item) {
// 	// 		 //   		// console.log(JSON.stringify(item) + " is a nice number!");
// 	// 		 //   		// console.log(item._id + " is a nice number!");

// 	// 			// });
// 	// 		} else {
// 	// 			itemCollection.save();
// 	// 		};
// 	// 		// console.log("Load was successful");
// 	// 		// console.log(JSON.stringify(result));
// 	// 	}
// 	// });
// 	// return productsByCategoryList;
// }

// function LoadAllProducts() {
// 	// reset error status
// 	apistatus.clear();

// 	// console.log("jsdhsjd");
// 	login.getToken()
// 	.then(function(contents) {
// 	    console.log('token: '+contents);
// 		if (contents != null) {
// 			GetProducts(contents);
// 		}else{
// 			console.log("Error not logged in");
// 		}
// 	}, function(error) {
// 		console.log(error);
// 	});

// 	function GetProducts(token) {
// 		api.ApiProducts(token)
// 		.then(function(response) {
// 			var ok = response.ok;
// 			var status = response.status;
// 			var response_msg = JSON.parse(response._bodyText);
// 			if (ok) {
// 				console.log("api: "+JSON.stringify(JSON.parse(response._bodyText).products));
// 				// var arr = [];
// 				// arr.push(JSON.parse(response._bodyText).categories);
// 				productsList.replaceAll(JSON.parse(response._bodyText).products);
// 			}else{
// 				if (status == 502) {
// 					console.log("Network Error");
// 				}else {
// 					console.log(status);	
// 				}
// 			}
// 		}).catch(function(err) {
// 			console.log("There was an error");
// 		});
// 	}
// 	// itemCollection.load(function (err, tableStats, metaStats) {
// 	// 	if (!err) {
// 	// 		// Load was successful
// 	// 		result = itemCollection.find();
// 	// 		// console.log(JSON.stringify(result));
// 	// 		if (tableStats.foundData && tableStats.rowCount > 0) {
// 	// 			// console.log(JSON.stringify(result));
// 	// 			for (var item in result) {
// 	// 				if (result[item].productName) {
// 	// 					product = new Item(result[item].productName, result[item].category, result[item].price, result[item].quantity)
// 	// 					product._id = result[item]._id	
// 	// 					productsList.add(product);
// 	// 				}
// 	// 			}
// 	// 			// console.log(JSON.stringify(productsList));
// 	// 		} else {
// 	// 			itemCollection.save();
// 	// 		};
// 	// 		// console.log("Load was successful");
// 	// 		// console.log(JSON.stringify(result));
// 	// 	}
// 	// });
	
// 	// return productsList;
// }

function MoveToHistory(new_arr) {

	// console.log("jsdhsjd");
	// for (var item in new_arr) {
	// 	if (new_arr[item].productName) {
	// 			arr = new ItemHistory(new_arr[item].productName, new_arr[item].category, new_arr[item].priceBought, new_arr[item].quantityBought, new_arr[item].date, new_arr[item].time, new_arr[item].status);

	// 		historyList.add(arr);
	// 	}
	// }
	
	// history.insert(new_arr, 
	// 		function (result) {
	// 		if (result.inserted.length != 0) {
	// 			console.log(result.inserted.length+" items was inserted");
	// 			history.save(function (err) {
	// 				if (!err) {
	// 					// Save was successful
	// 					console.log("commited");
	// 					// set the cart everywhere to zero
	// 					quantityTotal.value = 0;
	// 					cart = [];
	// 					// stresObj = null;
	// 					console.log("new_arr: "+JSON.stringify(new_arr));
	// 				}
	// 			});
											
	// 		}else{
	// 			console.log(result.failed.length+" items failed");
	// 		}
	// });
}



module.exports = {
	lowerlize: lowerlize,

	quantityTotal: quantityTotal,
	cart: cart,
	setCartEmpty: setCartEmpty,

	isEmpty: isEmpty,
	commit: commit,
	
	MoveToHistory: MoveToHistory,
	historyList: historyList,

	staffDetails: staffDetails,
	getStaffDetails: getStaffDetails,

	// apistatus: apistatus
};

